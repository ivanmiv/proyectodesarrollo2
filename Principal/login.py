from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox

from ModuloEmpleado.modelo import Empleado
from Principal.Fabrica_Panel_Empleado import FabricaPanelEmpleados


class UiLoginSingleton:
    __singleton = None

    class UiLogin(QtWidgets.QMainWindow):
        def __init__(self):
            super(UiLoginSingleton.UiLogin, self).__init__()
            uic.loadUi('Principal/login.ui', self)
            self.bIniciarSesion.clicked.connect(self.on_click_bIniciarSesion)

            self.panel_empleado = None
            self.show()

        def on_click_bIniciarSesion(self):
            login = self.lineUser.text()
            password = self.linePassword.text()

            if password == '' or login == '':
                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setText("Debe ingresar número de documento de identificación y contraseña")
                ventana_confirmacion.exec_()
                return

            try:
                empleado = Empleado.get(Empleado.numero_documento_identificacion == login,Empleado.estado == 'ACTIVO')
            except Exception as e:
                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText("No existe el empleado con el número de identificación ingresado")
                ventana_confirmacion.exec_()
                return
            try:

                if empleado.estado == 'INACTIVO':
                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText("El empleado no se encuentra activo en el sistema")
                    ventana_confirmacion.exec_()
                else:
                    if empleado.intentos_inicio_sesion < 3:
                        if (empleado.verificar_contrasena(password)):
                            # En caso de autenticarse el login desplegamos la ventana correspondiente segun el cargo
                            fabrica_panel = FabricaPanelEmpleados()
                            self.panel_empleado = fabrica_panel.mostrar_panel_empleado(empleado, empleado.cargo,
                                                                                       self)
                            self.hide()
                            self.lineUser.setText("")
                            self.linePassword.setText("")
                        else:
                            empleado.intentos_inicio_sesion += 1
                            empleado.save()
                            ventana_confirmacion = QMessageBox()
                            ventana_confirmacion.setWindowTitle("Mensaje")
                            ventana_confirmacion.setText("Contraseña incorrecta")
                            ventana_confirmacion.exec_()
                    else:
                        ventana_confirmacion = QMessageBox()
                        ventana_confirmacion.setWindowTitle("Mensaje")
                        ventana_confirmacion.setText("Ha superado el maximo número de intentos de sesíon fallidos\n"
                                                     "Contacte con un gerente para desbloquear su usuario")
                        ventana_confirmacion.exec_()
            except Exception as e:
                print(e)

    def __new__(cls, *args, **kwargs):
        if not cls.__singleton:
            cls.__singleton = UiLoginSingleton.UiLogin()
        return cls.__singleton
