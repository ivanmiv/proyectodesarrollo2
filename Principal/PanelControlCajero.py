from PyQt5 import uic
from PyQt5.QtWidgets import QMessageBox

from ModuloEmpleado.ModuloEmpleado import ModuloEmpleados
from ModuloFactura.ModuloFacturacion import ModuloFacturacion
from ModuloReportes.Fabrica_Panel_Reportes_Empleado import FabricaPanelEmpleadosReportes
from  Principal.PanelControl import UiPanelControl


class PanelControlCajero(UiPanelControl):
    def __init__(self, empleado, ui_padre):
        super(PanelControlCajero, self).__init__()
        uic.loadUi('Principal/PanelControl.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.label_nombre.setText("Bienvenido - " + str(empleado.nombres))

        self.b_facturacion_pagos.clicked.connect(self.on_click_b_facturacion_pagos)
        self.b_informacion_personal_cajero.clicked.connect(self.on_click_b_informacion_personal_cajero)
        self.b_reportes_cajero.clicked.connect(self.on_click_b_reportes_cajero)
        self.wCajero.show()
        self.wGerente.hide()
        self.wMesero.hide()
        self.show()

        ########################################################################

    def on_click_b_facturacion_pagos(self):
        self.facturas = ModuloFacturacion(self.usuario, self)

    def on_click_b_informacion_personal_cajero(self):
        self.empleados = ModuloEmpleados(self.usuario, self)

    def on_click_b_reportes_cajero(self):
        self.reporte = FabricaPanelEmpleadosReportes()
        self.reporte.mostrar_panel_empleado(self,"cajero")

    def closeEvent(self, e):
        ventana_confirmacion = QMessageBox()
        ventana_confirmacion.setText("Cerrar sesión?")
        ventana_confirmacion.setWindowTitle("Mensaje")
        ventana_confirmacion.setStandardButtons(QMessageBox.Ok | QMessageBox.Discard | QMessageBox.Cancel)
        ventana_confirmacion.setDefaultButton(QMessageBox.Cancel)
        ventana_confirmacion.button(QMessageBox.Ok).setText("Si")
        ventana_confirmacion.button(QMessageBox.Discard).setText("No")
        ventana_confirmacion.button(QMessageBox.Cancel).setText("Cancelar")
        respuesta = ventana_confirmacion.exec_()
        if respuesta == QMessageBox.Ok:
            self.ui_padre.show()
            e.accept()
        elif (respuesta == QMessageBox.Discard or respuesta == QMessageBox.Cancel):
            e.ignore()
