from PyQt5 import uic
from PyQt5.QtWidgets import QMessageBox

from ModuloCategoria.ModuloCategoria import ModuloCategoria
from ModuloEmpleado.ModuloEmpleado import ModuloEmpleados
from ModuloItemMenu.ModuloItemMenu import ModuloItemMenu
from ModuloReportes.Fabrica_Panel_Reportes_Empleado import FabricaPanelEmpleadosReportes
from ModuloSucursal.ModuloSucursales import ModuloSucursales
from  Principal.PanelControl import UiPanelControl


class PanelControlGerente(UiPanelControl):
    def __init__(self, empleado, ui_padre):
        super(PanelControlGerente, self).__init__()
        uic.loadUi('Principal/PanelControl.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.label_nombre.setText("Bienvenido - " + str(empleado.nombres))

        self.b_gestion_categorias.clicked.connect(self.on_click_b_gestion_categorias)
        self.b_gestion_items.clicked.connect(self.on_click_b_gestion_items)
        self.b_gestion_personal.clicked.connect(self.on_click_b_gestion_personal)
        self.b_gestion_sucursales.clicked.connect(self.on_click_b_gestion_sucursales)
        self.b_reportes_gerente.clicked.connect(self.on_click_b_reportes_gerente)
        self.wGerente.show()
        self.wCajero.hide()
        self.wMesero.hide()
        self.show()
        ########################################################################

    def on_click_b_gestion_personal(self):
        self.empleados = ModuloEmpleados(self.usuario, self)

    def on_click_b_gestion_sucursales(self):
        self.sucursales = ModuloSucursales(self.usuario, self)

    def on_click_b_gestion_categorias(self):
        self.categorias = ModuloCategoria(self.usuario, self)

    def on_click_b_gestion_items(self):
        self.items = ModuloItemMenu(self.usuario, self)

    def on_click_b_reportes_gerente(self):
        self.reporte = FabricaPanelEmpleadosReportes()
        self.reporte.mostrar_panel_empleado(self,"gerente")

    def closeEvent(self, e):
        ventana_confirmacion = QMessageBox()
        ventana_confirmacion.setText("Cerrar sesión?")
        ventana_confirmacion.setWindowTitle("Mensaje")
        ventana_confirmacion.setStandardButtons(QMessageBox.Ok | QMessageBox.Discard | QMessageBox.Cancel)
        ventana_confirmacion.setDefaultButton(QMessageBox.Cancel)
        ventana_confirmacion.button(QMessageBox.Ok).setText("Si")
        ventana_confirmacion.button(QMessageBox.Discard).setText("No")
        ventana_confirmacion.button(QMessageBox.Cancel).setText("Cancelar")
        respuesta = ventana_confirmacion.exec_()
        if respuesta == QMessageBox.Ok:
            self.ui_padre.show()
            e.accept()
        elif (respuesta == QMessageBox.Discard or respuesta == QMessageBox.Cancel):
            e.ignore()
