from PyQt5 import uic
from PyQt5.QtWidgets import QMessageBox

from ModuloEmpleado.ModuloEmpleado import ModuloEmpleados
from ModuloItemMenu.ModuloItemMenu import ModuloItemMenu
from ModuloOrden.ModuloPedidos import ModuloPedidos
from ModuloReportes.Fabrica_Panel_Reportes_Empleado import FabricaPanelEmpleadosReportes
from Principal.PanelControl import UiPanelControl


class PanelControlMesero(UiPanelControl):
    def __init__(self, empleado, ui_padre):
        super(PanelControlMesero, self).__init__()
        uic.loadUi('Principal/PanelControl.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.label_nombre.setText("Bienvenido - " + str(empleado.nombres))

        self.b_pedidos.clicked.connect(self.on_click_b_pedidos)
        self.b_informacion_personal_mesero.clicked.connect(self.on_click_b_informacion_personal_mesero)
        self.b_gestion_items_mesero.clicked.connect(self.on_click_b_gestion_items)
        self.b_reportes_mesero.clicked.connect(self.on_click_b_reportes_mesero)
        self.wMesero.show()
        self.wGerente.hide()
        self.wCajero.hide()
        self.show()
        ########################################################################

    def on_click_b_pedidos(self):
        self.pedidos = ModuloPedidos(self.usuario, self)

    def on_click_b_informacion_personal_mesero(self):
        self.empleados = ModuloEmpleados(self.usuario, self)

    def on_click_b_gestion_items(self):
        self.items = ModuloItemMenu(self.usuario, self)

    def on_click_b_reportes_mesero(self):
        self.reporte = FabricaPanelEmpleadosReportes()
        self.reporte.mostrar_panel_empleado(self,"mesero")

    def closeEvent(self, e):
        ventana_confirmacion = QMessageBox()
        ventana_confirmacion.setText("Cerrar sesión?")
        ventana_confirmacion.setWindowTitle("Mensaje")
        ventana_confirmacion.setStandardButtons(QMessageBox.Ok | QMessageBox.Discard | QMessageBox.Cancel)
        ventana_confirmacion.setDefaultButton(QMessageBox.Cancel)
        ventana_confirmacion.button(QMessageBox.Ok).setText("Si")
        ventana_confirmacion.button(QMessageBox.Discard).setText("No")
        ventana_confirmacion.button(QMessageBox.Cancel).setText("Cancelar")
        respuesta = ventana_confirmacion.exec_()
        if respuesta == QMessageBox.Ok:
            self.ui_padre.show()
            e.accept()
        elif (respuesta == QMessageBox.Discard or respuesta == QMessageBox.Cancel):
            e.ignore()
