from abc import ABCMeta, abstractmethod

from Principal.PanelControlCajero import PanelControlCajero
from Principal.PanelControlGerente import PanelControlGerente
from Principal.PanelControlMesero import PanelControlMesero


# --------------------Adapter--------------------------

# Interfaz a implementar
class FabricaPanelEmpleadosIF:
    __metaclass__ = ABCMeta

    @abstractmethod
    def mostrar_panel_empleado(self, tipo):
        raise Exception("Metodo no implementado")


class FabricaPanelEmpleados(FabricaPanelEmpleadosIF):
    # codigo para la fabrica
    tiposDeEmpleados = {"MESERO": PanelControlMesero,
                        "CAJERO": PanelControlCajero,
                        "GERENTE": PanelControlGerente}

    def mostrar_panel_empleado(self, empleado, tipo, ui_padre):
        panel_empleado = self.tiposDeEmpleados[tipo](empleado, ui_padre)

        return panel_empleado
