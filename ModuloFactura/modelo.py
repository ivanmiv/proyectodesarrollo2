from peewee import CharField, ForeignKeyField, IntegerField, FloatField, Check

from ConfiguracionORM.BaseModel import BaseModel
from ModuloOrden.modelo import Orden


class Factura(BaseModel):
    propina = IntegerField()
    descuento = IntegerField(constraints=[Check('descuento >= 0 and descuento <99')])
    cedula_cliente = CharField(max_length=20)
    impuesto = IntegerField()
    orden = ForeignKeyField(Orden, unique=True)
    nombre_sucursal = CharField()
    telefono_sucursal = CharField()
    direccion_sucursal = CharField()

    def __str__(self):
        return str(self.id)


class Pago(BaseModel):
    TIPO_PAGO_CHOICES = (('EFECTIVO', 'En efectivo'), ('TARJETA', 'Tarjeta'))
    factura = ForeignKeyField(Factura)
    valor = IntegerField()
    tipo_pago = CharField(max_length=20,
                          constraints=[Check('tipo_pago = \'EFECTIVO\' or tipo_pago = \'TARJETA CREDITO\''
                                             ' or tipo_pago = \'TARJETA DEBITO\'')])


class ItemFactura(BaseModel):
    factura = ForeignKeyField(Factura)
    nombre = CharField()
    precio = FloatField()
    cantidad = IntegerField()
