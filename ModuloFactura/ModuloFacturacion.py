#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback

from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QTableWidgetItem
from peewee import DoesNotExist, IntegrityError

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter
from ModuloFactura.GeneradorFactura import Generador
from ModuloFactura.modelo import Factura, Pago, ItemFactura
from ModuloItemMenu.modelo import ItemMenu
from ModuloOrden.modelo import Orden, ItemOrden


class ModuloFacturacion(QtWidgets.QMainWindow):
    def __init__(self, empleado, ui_padre):
        super().__init__()
        uic.loadUi('ModuloFactura/ModuloFacturacion.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.b_buscar_orden_pago.clicked.connect(self.on_click_b_buscar_orden_pago)
        self.b_registrar_pago.clicked.connect(self.on_click_b_registrar_pago)
        self.b_buscar_factura_consulta.clicked.connect(self.on_click_b_buscar_factura_consulta)
        self.b_agregar_pago.clicked.connect(self.on_click_b_agregar_pago)
        self.b_eliminar_pago.clicked.connect(self.on_click_b_eliminar_pago)
        self.sb_descuento_pago.valueChanged.connect(self.on_value_change_descuento)
        self.le_propina_pago.textChanged.connect(self.on_text_changed_propina)
        self.orden = None
        self.items_orden = None

        self.iniciar_tablas()

        self.ventana_confirmacion = QMessageBox()

        self.show()

    def actualizar_texto_ventana_confirmacion(self, texto):
        self.ventana_confirmacion.setText(texto)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        # respuesta = ventana_confirmacion.exec_()

    def iniciar_tablas(self):

        self.t_pagos_pago.setColumnCount(2)
        self.t_pagos_pago.setRowCount(0)
        self.t_pagos_pago.setHorizontalHeaderLabels(['Valor','Tipo'])
        self.t_pagos_consulta.setColumnCount(2)
        self.t_pagos_consulta.setRowCount(0)
        self.t_pagos_consulta.setHorizontalHeaderLabels(['Valor','Tipo'])
        self.t_items_comprados_pago.setColumnCount(4)
        self.t_items_comprados_pago.setHorizontalHeaderLabels(
            ['Nombre plato', 'Cantidad', 'Valor unidad', 'Valor Total'])
        self.t_items_comprados_consulta.setColumnCount(4)
        self.t_items_comprados_consulta.setHorizontalHeaderLabels(
            ['Nombre plato', 'Cantidad', 'Valor unidad', 'Valor Total'])

    def on_click_b_buscar_orden_pago(self):
        with BaseDatosAdapter.base_datos.atomic():
            numero_orden = self.le_numero_orden_pago.text().strip()
            try:
                try:
                    self.orden = Orden.get(Orden.id == numero_orden, Orden.estado == 'ACTIVO')
                    self.items_orden = ItemOrden.select().where(ItemOrden.orden == self.orden)
                except DoesNotExist as e:
                    print(e)
                    texto = "No existe la orden %s" % numero_orden
                    self.actualizar_texto_ventana_confirmacion(texto)
                    self.ventana_confirmacion.exec_()
                else:
                    self.le_numero_orden_pago.setText(numero_orden)
                    valor_bruto = 0

                    self.t_items_comprados_pago.setRowCount(len(self.items_orden))

                    i = 0
                    for item in self.items_orden:
                        item_actual = ItemMenu.get(ItemMenu.id == item.plato)
                        cantidad = item.cantidad_plato
                        precio = item_actual.precio
                        precio_total_items = cantidad * precio
                        valor_bruto = precio_total_items + valor_bruto

                        self.t_items_comprados_pago.setItem(i, 0, QTableWidgetItem(item_actual.nombre))
                        self.t_items_comprados_pago.setItem(i, 1, QTableWidgetItem(str(cantidad)))
                        self.t_items_comprados_pago.setItem(i, 2, QTableWidgetItem(str(precio)))
                        self.t_items_comprados_pago.setItem(i, 3, QTableWidgetItem(str(precio_total_items)))
                        i += 1

                    self.le_valor_bruto_pago.setText(str(valor_bruto))

                    impuesto = valor_bruto * 0.08
                    self.le_iva_pago.setText(str(impuesto))

                    descuento = self.sb_descuento_pago.value() / 100.0

                    propina = float(self.le_propina_pago.text())

                    self.total = (valor_bruto + impuesto) * (1 - descuento) + propina
                    self.le_valor_total_pago.setText(str(self.total))


            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error buscando orden: "+str(e))
                self.ventana_confirmacion.exec_()

    def on_click_b_registrar_pago(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                factura = Factura()
                factura.orden = self.orden
                factura.impuesto = float(self.le_iva_pago.text())
                factura.cedula_cliente = self.le_documento_cliente_pago.text()
                factura.propina = float(self.le_propina_pago.text())
                factura.descuento = self.sb_descuento_pago.value()
                factura.nombre_sucursal = self.usuario.sucursal.nombre
                factura.telefono_sucursal = self.usuario.sucursal.telefono
                factura.direccion_sucursal = self.usuario.sucursal.direccion
                factura.save()

                devolver = 0
                lista_pagos = []
                for i in range(self.t_pagos_pago.rowCount()):
                    tipo_pago = self.t_pagos_pago.item(i, 1).text()
                    valor = self.t_pagos_pago.item(i, 0).text()
                    pago_factura = Pago()
                    pago_factura.factura = factura
                    pago_factura.tipo_pago = tipo_pago
                    pago_factura.valor = float(valor)
                    lista_pagos.append({'nombre_pago': pago_factura.tipo_pago, 'valor_pagado': pago_factura.valor})
                    pago_factura.save()

                    devolver += float(valor)

                devolver -= self.total

                if (devolver < 0):
                    raise IntegrityError(
                        "Pago incompleto, el valor pagado debe ser mayor o igual al total de la factura")

                self.le_devuelta_pago.setText(str(devolver))

                lista_productos = []
                valor_bruto = 0
                for i in range(self.t_items_comprados_pago.rowCount()):
                    item_factura = ItemFactura()
                    item_factura.factura = factura
                    item_factura.nombre = self.t_items_comprados_pago.item(i, 0).text()
                    item_factura.precio = float(self.t_items_comprados_pago.item(i, 2).text())
                    item_factura.cantidad = int(self.t_items_comprados_pago.item(i, 1).text())
                    lista_productos.append({'descripcion': item_factura.nombre, 'valor': item_factura.precio,
                                            'cantidad': item_factura.cantidad})
                    valor_bruto = valor_bruto + float(self.t_items_comprados_pago.item(i, 2).text()) * int(
                        self.t_items_comprados_pago.item(i, 1).text())
                    item_factura.save()


                pdf_factura = Generador()
                pdf_factura.generar_pdf(
                    factura.telefono_sucursal, factura.id,
                    factura.nombre_sucursal,
                    factura.orden.atentido_por.numero_documento_identificacion,
                    lista_productos, valor_bruto,
                    factura.descuento,
                    lista_pagos
                )
                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText("Factura registrada correctamente")
                ventana_confirmacion.exec_()




            except IntegrityError as e:
                BaseDatosAdapter.base_datos.rollback()
                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText(str(e))
                ventana_confirmacion.exec_()
                print(e)

            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error durante la facturacion: "+str(e))
                self.ventana_confirmacion.exec_()

    def on_click_b_buscar_factura_consulta(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                cedula = self.le_documento_cliente_consulta.text()
                numero_orden = self.le_numero_orden_consulta.text()
                if (numero_orden == ''):
                    raise DoesNotExist("no existe la orden")

                factura_consulta = Factura.get(Factura.cedula_cliente == cedula, Factura.orden == numero_orden)


                try:
                    try:
                        self.items_factura = ItemFactura.select().where(ItemFactura.factura == factura_consulta)
                        self.items_orden = ItemOrden.select().where(ItemOrden.orden == factura_consulta.orden)
                    except DoesNotExist as e:
                        print(e)
                        texto = "No existe"
                        ventana_confirmacion = QMessageBox()
                        ventana_confirmacion.setWindowTitle("Mensaje")
                        ventana_confirmacion.setText(texto)
                        ventana_confirmacion.exec_()
                    else:

                        valor_bruto = 0

                        descuento = factura_consulta.descuento / 100.0

                        pagos = Pago.select().where(Pago.factura == factura_consulta)

                        self.t_pagos_consulta.clearContents()
                        self.t_pagos_consulta.setRowCount(0)
                        devolver = 0
                        for pago in pagos:
                            devolver += float(pago.valor)
                            rowPosition = self.t_pagos_consulta.rowCount()
                            self.t_pagos_consulta.insertRow(rowPosition)
                            self.t_pagos_consulta.setItem(rowPosition, 0, QTableWidgetItem(str(pago.valor)))
                            self.t_pagos_consulta.setItem(rowPosition, 1, QTableWidgetItem(pago.tipo_pago))

                        self.t_items_comprados_consulta.clearContents()
                        self.t_items_comprados_consulta.setRowCount(0)
                        for item in self.items_factura:
                            valor_bruto = (item.cantidad * item.precio) + valor_bruto
                            rowPosition = self.t_items_comprados_consulta.rowCount()
                            self.t_items_comprados_consulta.insertRow(rowPosition)
                            self.t_items_comprados_consulta.setItem(rowPosition, 0, QTableWidgetItem(item.nombre))
                            self.t_items_comprados_consulta.setItem(rowPosition, 1, QTableWidgetItem(str(item.cantidad)))
                            self.t_items_comprados_consulta.setItem(rowPosition, 2, QTableWidgetItem(str(item.precio)))
                            self.t_items_comprados_consulta.setItem(rowPosition, 3,
                                                                    QTableWidgetItem(str(item.cantidad * item.precio)))

                        self.le_numero_orden_consulta.setText(str(factura_consulta.orden.id))
                        self.le_iva_consulta.setText(str(factura_consulta.impuesto))
                        self.le_propina_consulta.setText(str(factura_consulta.propina))
                        self.sb_descuento_consulta.setValue(int(factura_consulta.descuento))
                        self.le_valor_bruto_consulta.setText(str(valor_bruto))
                        total = (valor_bruto + factura_consulta.impuesto) * (1 - descuento) + factura_consulta.propina
                        self.le_valor_total_consulta.setText(str(total))
                        self.le_total_pagado_consulta.setText(str(devolver))
                        self.le_numero_factura_consulta.setText(str(factura_consulta.id))
                        devolver -= total
                        self.le_devuelta_consulta.setText(str(devolver))

                except Exception as e:
                    print(e)
                    traceback.print_exc()

            except DoesNotExist as e:
                texto = "No existe la factura para la orden %s del cliente %s \n"%(numero_orden,cedula)

                try:
                    facturas_cliente = Factura.select().where(Factura.cedula_cliente == cedula)
                    texto+="Las facturas del cliente son: \n"

                    for factura in facturas_cliente:
                        texto += " Factura "+str(factura.id)+" numero de orden "+str(factura.orden.id)+", "



                except Exception as e:
                    print(e)

                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText(texto)
                ventana_confirmacion.exec_()

            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error consultando factura: "+str(e))
                self.ventana_confirmacion.exec_()

    def on_click_b_agregar_pago(self):
        try:
            valor = self.le_valor_pago.text()
            tipo = self.cb_tipo_pago.currentText()
            rowPosition = self.t_pagos_pago.rowCount()
            self.t_pagos_pago.insertRow(rowPosition)
            self.t_pagos_pago.setItem(rowPosition, 0, QTableWidgetItem(valor))
            self.t_pagos_pago.setItem(rowPosition, 1, QTableWidgetItem(tipo))
        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()

    def on_click_b_eliminar_pago(self):
        try:
            self.t_pagos_pago.removeRow(self.t_pagos_pago.currentRow())
        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()

    def on_value_change_descuento(self, i):
        self.recalcular_total()

    def on_text_changed_propina(self, i):
        self.recalcular_total()

    def recalcular_total(self):

        if not self.orden == None:

            valor_bruto = 0

            for item in self.items_orden:
                item_actual = ItemMenu.get(ItemMenu.id == item.plato)
                cantidad = item.cantidad_plato
                precio = item_actual.precio
                precio_total_items = cantidad * precio
                valor_bruto = precio_total_items + valor_bruto

            impuesto = valor_bruto * 0.08

            descuento = self.sb_descuento_pago.value() / 100.0

            propina = float(self.le_propina_pago.text())

            self.total = (valor_bruto + impuesto) * (1 - descuento) + propina
            self.le_valor_total_pago.setText(str(self.total))

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()
