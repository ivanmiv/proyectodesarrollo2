# import Imagenes
from datetime import datetime

from PyQt5.QtWidgets import QMessageBox
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas

import subprocess

class Generador:
    def __init__(self):
        self.imagen = 'Imagenes/logoBN.png'
    def generar_pdf(self, telefono, id_factura, nombre_sucursal, id_empleado, lista_items, valor_bruto, descuento,
                    lista_formas_pago):
        # print(telefono, id_factura, lista_productos)
        '''
        Formato Factura

        Logo
        Nombre empresa
        Nit: Inventado
        Posible: telefono
        ------------------  
        ID Factura
        Fecha - hora
        Posible: sucursal
        Empleado caja
        ------------------

        Codigo - Nombre producto - Valor unit - impuesto - cantidad

        Lista productos

        ------------------

        Venta bruta
        Descuento
        Subtotal
        Impuesto: suma de valores
        Total
        Recibido---------
        Lista: Formas de pago
        '''

        nombre_factura_pdf = 'factura' + str(id_factura) + '_' + str(datetime.now().year) + '-' + str(
            datetime.now().month) + '-' + str(datetime.now().day) + '.pdf'
        c = canvas.Canvas(nombre_factura_pdf, pagesize=letter)

        c.drawImage(self.imagen, 50, 700, width = None, height = None)
        c.drawCentredString(300, 660, 'Restaurante Il Forno S.A.S')
        c.drawCentredString(300, 640, 'NIT 982748749-1')
        c.drawCentredString(300, 620, telefono)
        c.drawCentredString(300, 595,
                            '-----------------------------------------------------------------------------------------------------------------------------------')

        c.drawString(50, 570, 'No. Factura')
        c.drawString(200, 570, ':')
        c.drawString(300, 570, str(id_factura))

        c.drawString(50, 550, 'Fecha - Hora')
        c.drawString(200, 550, ':')
        c.drawString(300, 550, str(datetime.now()))

        c.drawString(50, 530, 'Sucursal')
        c.drawString(200, 530, ':')
        c.drawString(300, 530, nombre_sucursal)

        c.drawString(50, 510, 'Cod. Empleado')
        c.drawString(200, 510, ':')
        c.drawString(300, 510, id_empleado)

        c.drawCentredString(300, 485,
                            '-----------------------------------------------------------------------------------------------------------------------------------')
        c.drawString(50, 460, 'Descripción')
        c.drawString(250, 460, 'Valor Unit.')
        c.drawString(350, 460, 'Impuesto')
        c.drawString(450, 460, 'Cantidad')
        pos_item = 440
        for item in lista_items:
            c.drawString(50, pos_item, item['descripcion'])
            c.drawString(250, pos_item, str(item['valor']))
            c.drawString(350, pos_item, '8.0%')
            c.drawString(450, pos_item, str(item['cantidad']))
            pos_item = pos_item - 20
            if (pos_item < 100):
                c.showPage()
                pos_item = 660

        pos_item = pos_item - 20
        c.drawCentredString(300, pos_item,
                            '-----------------------------------------------------------------------------------------------------------------------------------')
        pos_item = pos_item - 20
        c.drawString(50, pos_item, 'Venta Bruta')
        c.drawString(200, pos_item, ':')
        c.drawString(300, pos_item, str(valor_bruto))
        pos_item = pos_item - 20

        c.drawString(50, pos_item, 'Descuento')
        c.drawString(200, pos_item, ':')
        c.drawString(300, pos_item, str(descuento))
        pos_item = pos_item - 20

        c.drawString(50, pos_item, 'Impuesto')
        c.drawString(200, pos_item, ':')
        c.drawString(300, pos_item, str(valor_bruto * 0.19))
        pos_item = pos_item - 20

        valor_total = valor_bruto + valor_bruto * 0.19 - descuento
        c.drawString(50, pos_item, 'Total')
        c.drawString(200, pos_item, ':')
        c.drawString(300, pos_item, str(valor_total))
        pos_item = pos_item - 20

        c.drawString(50, pos_item,
                     'RECIBIDO---------------------------------------------------------------------------------------------------------------------------')
        pos_item = pos_item - 20
        for pago in lista_formas_pago:
            c.drawString(50, pos_item, pago['nombre_pago'])
            c.drawString(200, pos_item, ':')
            c.drawString(300, pos_item, str(pago['valor_pagado']))
            pos_item = pos_item - 20
            if (pos_item < 100):
                c.showPage()
                pos_item = 660

        c.showPage()

        c.save()
        print('GUARDANDO')
        import os
        path = os.getcwd()
        import webbrowser
        webbrowser.open_new(r'file://'+path+'/'+nombre_factura_pdf)
        self.ventana_confirmacion = QMessageBox()
        self.ventana_confirmacion.setText("Factura generada en: "+nombre_factura_pdf)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        respuesta = self.ventana_confirmacion.exec_()

    def generar_reporte(self, datos, labels, mes, tipo):

        try:
            tipos = {'1': 'mesero_mes', '2': 'ingresos_diarios'}
            fecha_actual = datetime.now()
            nombre_reporte_pdf = 'reporte_' + tipos[str(tipo)] + '_' + str(fecha_actual.year) + '-' + str(
                fecha_actual.month) + '-' + str(fecha_actual.day) + '_' + str(fecha_actual.hour) + '-' + str(fecha_actual.minute) + '.pdf'
            c = canvas.Canvas(nombre_reporte_pdf, pagesize=letter)
            c.drawImage(self.imagen, 50, 700, width = None, height = None)
            c.drawCentredString(300, 660, 'Restaurante Il Forno S.A.S')
            c.drawCentredString(300, 640, 'NIT 982748749-1')
            if (tipo == 1):
                c.drawCentredString(300, 620, 'Reporte: Meseros del mes')
                c.drawString(50, 580, 'Mes')
                c.drawString(250, 580, 'Mesero')
            elif (tipo == 2):
                c.drawCentredString(300, 620, 'Reporte: Ingresos diarios durante: ' + mes)
                c.drawString(50, 580, 'Día')
                c.drawString(250, 580, 'Ingresos (Pesos)')

            pos_item = 560
            for i in range(0, len(labels)):
                c.drawString(50, pos_item, labels[i])
                c.drawString(250, pos_item, str(datos[i]))
                pos_item = pos_item - 20
                if (pos_item < 100):
                    c.showPage()
                    pos_item = 660
            c.showPage()
            c.save()
            import os
            path = os.getcwd()
            import webbrowser
            webbrowser.open_new(r'file://'+path+'/'+nombre_reporte_pdf)
            self.ventana_confirmacion = QMessageBox()
            self.ventana_confirmacion.setText("Reporte generado en: " + nombre_reporte_pdf)
            self.ventana_confirmacion.setWindowTitle("Mensaje")
            self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
            self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
            self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
            respuesta = self.ventana_confirmacion.exec_()
        except Exception as e:
            print(e)
