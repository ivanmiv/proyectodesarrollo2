from peewee import CharField

from ConfiguracionORM.BaseModel import BaseModel


# from ModuloMesa.modelo import Mesa


class Sucursal(BaseModel):
    nombre = CharField(max_length=200, unique=True)
    direccion = CharField(max_length=200)
    telefono = CharField(max_length=20)

    def crear_sucursal(self):
        self.save()

    def modificar_sucursal(self):
        self.save()

    def eliminar_sucursal(self):
        query = self.update(estado='INACTIVO').where(Sucursal.id == self.id)
        query.execute()

    def consultar_sucursal(self, nombre_sucursal):
        return Sucursal.get(Sucursal.nombre==nombre_sucursal,Sucursal.estado == 'ACTIVO')
