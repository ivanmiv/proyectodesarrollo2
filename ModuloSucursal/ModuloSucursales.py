#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt5 import uic, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMessageBox
from peewee import DoesNotExist

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter
from ModuloMesa.modelo import Mesa
from ModuloSucursal.modelo import Sucursal


class ModuloSucursales(QtWidgets.QMainWindow):
    sucursal = None

    def __init__(self, empleado, ui_padre):
        super(ModuloSucursales, self).__init__()
        uic.loadUi('ModuloSucursal/ModuloSucursales.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.b_registrar_sucursal.clicked.connect(self.registrar_sucursales)
        self.b_registrar_cambios.clicked.connect(self.modificar_sucursales)
        self.b_eliminar_sucursal.clicked.connect(self.eliminar_sucursales)
        self.b_obtener_informacion_consulta.clicked.connect(self.consultar_sucursales)
        self.b_obtener_informacion_modificacion.clicked.connect(self.consultar_sucursal_modificar)
        self.b_agregar_mesa.clicked.connect(self.agregar_mesa)
        self.b_eliminar_mesa.clicked.connect(self.eliminar_mesa)
        self.llenar_combo_boxes()
        self.set_table_model_and_fill(sucursal=None, tabla=self.t_mesas_modificar)
        self.set_table_model_and_fill(sucursal=None, tabla=self.t_mesas_consultar)

        self.ventana_confirmacion = QMessageBox()

        self.show()

    def actualizar_texto_ventana_confirmacion(self, texto):
        self.ventana_confirmacion.setText(texto)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        # respuesta = ventana_confirmacion.exec_()

    def registrar_sucursales(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                sucursal = Sucursal()
                estado = 'ACTIVO'
                nombre = self.le_nombre_sucursal.text().strip()
                telefono = self.le_telefono_sucursal.text().strip()
                direccion = self.le_direccion_sucursal.text().strip()

                cantidad_mesas = self.sb_cantidad_mesas.value()

                try:
                    cantidad_mesas = int(cantidad_mesas)
                except Exception as e:
                    raise Exception("La cantidad de mesas debe ser un valor numerico")

                if cantidad_mesas <= 0:
                    raise Exception("La cantidad de mesas debe ser mayor que 0")

                if len(str(cantidad_mesas)) == 0:
                    raise Exception("El número de mesas no puede ser vacío")

                if len(str(direccion)) == 0:
                    raise Exception("La dirección de la sucursal no debe ser vacía")

                if len(str(nombre)) == 0:
                    raise Exception("El nombre de la sucursal no debe ser vacio")

                try:
                    telefono = int(telefono)
                except Exception as e:
                    raise Exception("El teléfono debe ser un némero válido")
                try:
                    sucursal = Sucursal().consultar_sucursal(nombre)
                    if not (sucursal == None):
                        raise  Exception("El nombre de la sucursal ingresado ya existe")
                except DoesNotExist as e:
                    pass

                if len(str(telefono)) == 0:
                    raise Exception("El teléfono no puede ser vacio")
                if len(str(telefono)) != 7 or len(str(telefono)) != 10:
                    raise Exception("El número de teléfono no es válido")

                sucursal.estado = estado
                sucursal.nombre = nombre
                sucursal.telefono = telefono
                sucursal.direccion = direccion

                try:
                    sucursal.crear_sucursal()
                    for i in range(1, cantidad_mesas + 1):
                        mesa = Mesa()
                        mesa.estado = 'ACTIVO'
                        mesa.ocupado = False
                        mesa.numero_interno = i
                        mesa.sucursal = sucursal
                        mesa.crear_mesa()
                except Exception as e:
                    BaseDatosAdapter.base_datos.rollback()
                    self.actualizar_texto_ventana_confirmacion(str(e))
                    self.ventana_confirmacion.exec_()
                finally:
                    self.llenar_combo_boxes()

                self.actualizar_texto_ventana_confirmacion("Sucursal creada exitosamente")
                self.ventana_confirmacion.exec_()

            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def consultar_sucursal_modificar(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                self.sucursal = Sucursal().consultar_sucursal(
                    nombre_sucursal=self.jcb_seleccionar_sucursal_modificar.currentText())
                self.le_nombre_sucursal_2.setText(self.sucursal.nombre)
                self.le_direccion_sucursal_2.setText(self.sucursal.direccion)
                self.le_telefono_sucursal_2.setText(self.sucursal.telefono)
                cantidad_mesas = Mesa().obtener_cantidad_mesas(sucursal_buscada=self.sucursal)
                self.l_cantidad_mesas_modificar.setText(str(cantidad_mesas))
                self.set_table_model_and_fill(sucursal=self.sucursal, tabla=self.t_mesas_modificar)
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def modificar_sucursales(self):
        with BaseDatosAdapter.base_datos.atomic():

            self.sucursal.nombre = self.le_nombre_sucursal_2.text()
            self.sucursal.telefono = self.le_telefono_sucursal_2.text()
            self.sucursal.direccion = self.le_direccion_sucursal_2.text()
            try:
                self.sucursal.modificar_sucursal()
                self.le_nombre_sucursal_2.setText('')
                self.le_telefono_sucursal_2.setText('')
                self.le_direccion_sucursal_2.setText('')
                self.l_cantidad_mesas_modificar.setText('')
                self.set_table_model_and_fill(sucursal=None, tabla=self.t_mesas_modificar)
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()
            finally:
                self.sucursal = None
                self.llenar_combo_boxes()

    def eliminar_sucursales(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                sucursal = Sucursal().consultar_sucursal(nombre_sucursal=self.jcb_seleccionar_sucursal_eliminar.currentText())
                try:
                    sucursal.eliminar_sucursal()
                except Exception as e:
                    self.actualizar_texto_ventana_confirmacion("Error al eliminar la sucursal. \n")
                    self.ventana_confirmacion.exec_()
                finally:
                    self.llenar_combo_boxes()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def consultar_sucursales(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                try:
                    sucursal = Sucursal().consultar_sucursal(
                        nombre_sucursal=self.jcb_seleccionar_sucursal_consultar.currentText())
                    self.le_nombre_sucursal_4.setText(sucursal.nombre)
                    self.le_direccion_sucursal_4.setText(sucursal.direccion)
                    self.le_telefono_sucursal_4.setText(sucursal.telefono)
                    cantidad_mesas = Mesa().obtener_cantidad_mesas(sucursal_buscada=sucursal)
                    self.l_cantidad_mesas_consultar.setText(str(cantidad_mesas))

                    self.set_table_model_and_fill(sucursal=sucursal, tabla=self.t_mesas_consultar)
                except Exception as e:
                    self.actualizar_texto_ventana_confirmacion("Error al consultar la sucursal. \n")
                    self.ventana_confirmacion.exec_()
                finally:
                    self.llenar_combo_boxes()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def llenar_combo_boxes(self):
        self.jcb_seleccionar_sucursal_modificar.clear()
        self.jcb_seleccionar_sucursal_consultar.clear()
        self.jcb_seleccionar_sucursal_eliminar.clear()
        try:
            for sucursal in Sucursal.select().where(Sucursal.estado == 'ACTIVO'):
                self.jcb_seleccionar_sucursal_modificar.addItem(sucursal.nombre)
                self.jcb_seleccionar_sucursal_consultar.addItem(sucursal.nombre)
                self.jcb_seleccionar_sucursal_eliminar.addItem(sucursal.nombre)
        except Exception as e:
            print(e)

    def set_table_model_and_fill(self, sucursal, tabla):
        header = ['Numero', 'Estado']
        data = [(None, None)] * 7
        try:
            if not(sucursal is None): 
                data = []
                for mesa in Mesa.select().where(Mesa.sucursal_id == sucursal.id,Mesa.estado == 'ACTIVO').order_by(Mesa.numero_interno):
                    data.append((mesa.numero_interno, mesa.estado))

            modelo = MyTableModel(data, header, self)
            tabla.setModel(modelo)
            tabla.setColumnWidth(0, 125)
            tabla.setColumnWidth(1, 125)
        except Exception as e:
            print(e)

    def agregar_mesa(self):
        try:
            numero_mesas = Mesa().obtener_cantidad_mesas(self.sucursal)
            numero_mesas = numero_mesas + 1;
            mesa = Mesa()
            mesa.estado = 'ACTIVO'
            mesa.ocupado = False
            mesa.numero_interno = mesa.obtener_ultimo_numero_interno(self.sucursal) + 1
            mesa.sucursal = self.sucursal
            mesa.crear_mesa()
            cantidad_mesas = Mesa().obtener_cantidad_mesas(sucursal_buscada=self.sucursal)
            self.l_cantidad_mesas_modificar.setText(str(cantidad_mesas))
        except Exception as e:
            self.actualizar_texto_ventana_confirmacion("Error al crear las mesas. \n")
            self.actualizar_texto_ventana_confirmacion(e)
            self.ventana_confirmacion.exec_()
        finally:
            self.set_table_model_and_fill(sucursal=self.sucursal, tabla=self.t_mesas_modificar)

    def eliminar_mesa(self):
        numero_interno = self.t_mesas_modificar.selectedIndexes()[0].data()
        try:
            mesa = Mesa().obtener_mesa(numero_interno, self.sucursal)
            print(mesa)
            mesa.estado = 'INACTIVO'
            mesa.modificar_mesa()
            numero_mesas = Mesa().obtener_cantidad_mesas(self.sucursal)
            self.l_cantidad_mesas_modificar.setText(str(numero_mesas))
        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()
        else:
            pass
        finally:
            self.set_table_model_and_fill(sucursal=self.sucursal, tabla=self.t_mesas_modificar)

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()


class MyTableModel(QAbstractTableModel):
    def __init__(self, datain, headerdata, parent=None, *args):
        """ datain: a list of lists
            headerdata: a list of strings
        """

        QAbstractTableModel.__init__(self, parent, *args)
        self.arraydata = datain
        self.headerdata = headerdata

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        return len(self.arraydata[0])

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role != Qt.DisplayRole:

            return QVariant()
        return QVariant(self.arraydata[index.row()][index.column()])

    def headerData(self, col, orientation, role):

        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])

        return QVariant()
