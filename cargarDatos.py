import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from ConfiguracionORM.FachadaBD import FachadaBD
from ModuloCategoria.modelo import Categoria
from ModuloEmpleado.modelo import Empleado, Turno
from ModuloItemMenu.modelo import ItemMenu
from ModuloMesa.modelo import Mesa
from ModuloSucursal.modelo import Sucursal

app = QtWidgets.QApplication(sys.argv)
fachada_bd = FachadaBD()
fachada_bd.crear_tablas_bd()


def crear_categorias():
    categorias = (
        'Postres', 'Cafes', 'Gratinados', 'Canelones', 'Lasañas', 'Armalo a tu Gusto', 'Combinados', 'Risottos',
        'Raviolis', 'Pastas', 'Sanduches', 'Calzonis', 'Arma tu Pizza', 'Pizzas', 'Ensaladas', 'Entradas', 'Sopas',
        'Bruschettas', 'Otras Bebidas', 'Jugos Naturales', 'Sodas Saborizadas', 'Cervezas', 'Licores', 'Cocteles',
        'Sangrias',
        'Vinos Blancos', 'Vinos Rosados', 'Vinos Tintos')

    for nombre in categorias:
        cat1 = Categoria()
        cat1.nombre = nombre
        cat1.crear_categoria()


def crear_lista_items():
    # nombre foto precio descripcion categoria
    postres = Categoria.get(nombre='Postres')

    nombres_platos_postres = (
        {'nombre': 'Gelato Italiano', 'precio': 3500, 'descripcion': 'Con manos artesanas, creamos un helado suave, '
                                                                     'natural y de sabores intensos para tu deleit. SABORES: Limon - Kiwi/Banano - Yogurt - Frutos del Bosque - Veteado de Caramelo - Vainilla '
                                                                     '- Chocolate - Pistacho.', 'categoria': postres},

        {'nombre': 'Tres Leches', 'precio': 8200,
         'descripcion': 'Bizcochuelo tradicional, bañado en una salsa a base de leche condensada, cubierto con'
                        'merengue y virtua de chocolate.', 'categoria': postres},
        {
            'nombre': 'Brownie con Helado', 'precio': 8200,
            'descripcion': 'Brownie elaborado con nuestra receta tradicional, acompañado de helado artesanal'
                           'de vainilla y salsa de chocolate. Tambien dispobible con brownie bajo en calorias.',
            'categoria': postres},

        {'nombre': 'Tiramisu', 'precio': 8200,
         'descripcion': 'Tradicional postre italiano a base de bizcochuelo remojado en licor de cafe,'
                        'cubierto con un toque de cocoa y acompañado con dos barquillos.', 'categoria': postres},

        {'nombre': 'Pecan Pie', 'precio': 8200,
         'descripcion': 'Postre a base de nueces pecanas endulzadas, acompañado de helado artesanala de vainilla.',
         'categoria': postres},

        {'nombre': 'Flas de Leche', 'precio': 8200,
         'descripcion': 'Suave flan a base de leche condensada, cubierto con salsa de caramelo.', 'categoria': postres},

        {'nombre': 'Key Lime Pie', 'precio': 8200,
         'descripcion': 'Postre elaborado a base de galleta triturada, con centro de limon y leche condensada, '
                        'cubierto con una mezcla de crema y azucar.', 'categoria': postres},

        {'nombre': 'Cheese Cake', 'precio': 8200,
         'descripcion': 'Postre elaborado a base de queso crema, sobre una base de galleta triturada y cubierto '
                        'con salsa de moras frescas.', 'categoria': postres},

        {'nombre': 'Galleta', 'precio': 8200,
         'descripcion': 'Galleta con chips de chocolate y nueces, recien salida del horno, acompañada '
                        'de helado artesanal de vainilla.', 'categoria': postres},

        {'nombre': 'Copa Rigoletto', 'precio': 8200,
         'descripcion': 'Helado artesanal de vainilla y chocolate, cubierto con salsa de chocolate y mora; '
                        'acompañado de crema chantilli y un barquillo.', 'categoria': postres},

        {'nombre': 'Copa Baileys', 'precio': 8200,
         'descripcion': 'Combinacion de helado artesanal de vainilla, Baileys y crema chantilli.',
         'categoria': postres},

        {'nombre': 'Affogatto', 'precio': 5700,
         'descripcion': 'Cafe expreso acompañado de helado artesanal de vainilla', 'categoria': postres},

        {'nombre': 'Copa Brownie', 'precio': 8200,
         'descripcion': 'Brownie con helado artesanal de vainilla y chocolate, fresas frescas y crema chantilli; acompañado con un barquillo.',
         'categoria': postres},

        {'nombre': 'Malteada', 'precio': 8200,
         'descripcion': 'Puedes preparlas con cualquiera de nuestros sabores de helado artesanal. '
                        'SABORES: Limon - Kiwi/Banano - Yogurt - Frutos del Bosque - Veteado de Caramelo - Vainilla - Chocolate - Pistacho.',
         'categoria': postres})

    cafes = Categoria.get(nombre='Cafes')
    nombres_platos_cafes = (
        {'nombre': 'Americano', 'precio': 2800, 'descripcion': 'Cafe Americano.', 'categoria': cafes},
        {'nombre': 'Espresso', 'precio': 2800, 'descripcion': 'Cafe fuerte Espresso.', 'categoria': cafes},
        {'nombre': 'Espresso Doble', 'precio': 5600, 'descripcion': 'Cafe extra fuerte Espresso.', 'categoria': cafes},
        {'nombre': 'Cafe Affogatto', 'precio': 5700, 'descripcion': 'Expreso + Helado de Vainilla.',
         'categoria': cafes},
        {'nombre': 'Cappuccino', 'precio': 3500, 'descripcion': 'Espresso + Leche espumada caliente.',
         'categoria': cafes},
        {'nombre': 'Cappuccino Saborizado', 'precio': 5500,
         'descripcion': 'Cappuccino + Sirope de Macadamia o Chocolate Blanco.', 'categoria': cafes},
        {'nombre': 'Cappuccino con Licor', 'precio': 7900, 'descripcion': 'Cappuccino + Amaretto, Baileys o Brandy.',
         'categoria': cafes},
        {'nombre': 'Espresso Macchiato', 'precio': 3300, 'descripcion': 'Espresso + Pequeña porcion de leche espumada.',
         'categoria': cafes},
        {'nombre': 'Latte Macchiato', 'precio': 4800, 'descripcion': 'Leche caliente + Espresso + Leche espumada.',
         'categoria': cafes},
        {'nombre': 'Granizado de cafe', 'precio': 4800, 'descripcion': '', 'categoria': cafes},
        {'nombre': 'Opera', 'precio': 6900, 'descripcion': '', 'categoria': cafes},
        {'nombre': 'Opera con Licor', 'precio': 9500, 'descripcion': '', 'categoria': cafes}
    )

    gratinados = Categoria.get(nombre='Gratinados')

    nombres_platos_gratinados = (
        {'nombre': 'Pasta al Horno con Solomito y Queso Azul', 'precio': 19900,
         'descripcion': 'Penne con julianas de solomito, champiñones salteados y tocineta, con salsa de queso azul y crema de leche'
                        '; gratinado con queso mozzarella, holandes y parmesano', 'categoria': gratinados},
        {'nombre': 'Pasta al Horno con Boloñesa', 'precio': 17500,
         'descripcion': 'Penne con salsa boloñesa, pimentones asados, champiñones y tocinetaa; gratinado con queso mozzarella, holandes y parmesano',
         'categoria': gratinados},
        {'nombre': 'Pasta al Horno con Pollo', 'precio': 17500,
         'descripcion': 'Penne con jamon ahumado, pollo desmechado, maiz tierno, champiñones salteados y tocineta, en salsa de champiñones; gratinaod con queso mozzarella, honaldes y parmesano',
         'categoria': gratinados},
        {'nombre': 'Gratinado Otoño', 'precio': 17500,
         'descripcion': 'Cazuela con jamon ahumado, pollo desmechado, champiñones, maiz tierno, queso mozzarella, queso parmesano y crema de leche',
         'categoria': gratinados}
    )

    lasanas = Categoria.get(nombre='Lasañas')

    nombres_platos_lasanas = (
        {'nombre': 'Boloñesa', 'precio': 17900, 'descripcion': 'Boloñesa, queso holandes y mozzarella.',
         'categoria': lasanas},
        {'nombre': 'Boloñesa y Pollo', 'precio': 17900,
         'descripcion': 'Boloñesa, pollo desmechado y quezzo mozzarella.', 'categoria': lasanas},
        {'nombre': 'Boloñesa, Pollo y Champiñones', 'precio': 18900,
         'descripcion': 'Boloñesa, pollo desmechado, champiñones salteados y queso mozzarella.', 'categoria': lasanas},
        {'nombre': 'Pollo con Champiñones', 'precio': 17900,
         'descripcion': 'Pollo desmechado, champiñones salteados y queso mozzarella.', 'categoria': lasanas},
        {'nombre': 'Pollo', 'precio': 17900, 'descripcion': 'Pollo desmechado y queso mozzarella.',
         'categoria': lasanas}
    )

    canelones = Categoria.get(nombre='Canelones')

    nombres_platos_canelones = (
        {'nombre': 'Carnes', 'precio': 19900,
         'descripcion': 'Jamon serrano, esparragos, champiñones salteados y queso mozzarella.', 'categoria': canelones},
        {'nombre': 'Vegetariano', 'precio': 19900,
         'descripcion': 'Espinaca, queso ricotta, queso mozzarella y tomastes secos.', 'categoria': canelones},
        {'nombre': 'Hawaiano', 'precio': 17900,
         'descripcion': 'Jamon ahumado, tocineta, trozos de piña y queso mozzarella, con un toque de mostaza.',
         'categoria': canelones},
        {'nombre': 'Boloñesa', 'precio': 17900, 'descripcion': 'Boloñesa, jamon ahumado, tocineta y queso mozzarella.',
         'categoria': canelones},
        {'nombre': 'Pollo', 'precio': 17900,
         'descripcion': 'Pollo desmechado, jamon ahumado, champiñones salteados y queso mozzarella.',
         'categoria': canelones}
    )

    combinados = Categoria.get(nombre='Combinados')

    nombres_platos_combinados = (
        {'nombre': 'San Lorenzo', 'precio': 21900,
         'descripcion': 'Tocino cocido lentamente en reduccion de salsa teriyaki + Risotto Alfredo + Tomates cherry confitados.',
         'categoria': combinados},
        {'nombre': 'Mar y Tierra', 'precio': 25900,
         'descripcion': 'Corte de cerdo apanado, cubierto con camarones al ajillo en salsa rosa + Spaghetti al Burro.',
         'categoria': combinados},
        {'nombre': 'Pollo Fiorentino', 'precio': 22900,
         'descripcion': 'Filete de pollo a la plancha con esponaca, queso Ricotta y salsa napolitanal gratinado con queso mozzarella y parmesano + Spaghetti Alfredo + Ensalada de la casa.',
         'categoria': combinados},
        {'nombre': 'Donatelo', 'precio': 20900,
         'descripcion': 'Chuleta de cerdo + Spaghetti Siciliano + Ensalada de la casa.', 'categoria': combinados},
        {'nombre': 'Filippo', 'precio': 20900,
         'descripcion': 'Chuleta de cerdo + Spaghetti Carbonara + Ensalada de la casa.', 'categoria': combinados},
        {'nombre': 'Corleone', 'precio': 24900,
         'descripcion': 'Chuleta de cerdo + Solomito + Papa al horno gratinada + Ensalada de la casa.',
         'categoria': combinados},
        {'nombre': 'Manarola', 'precio': 17200,
         'descripcion': 'Chuleta de cerdo + Oaoa al horno gratinada + Ensalada Cesar.', 'categoria': combinados},
        {'nombre': 'Cotoletta Alla Parmigiana', 'precio': 21900,
         'descripcion': 'Corte de cerdo apanado con salsa napolitana, queso mozzarella y parmesado + Spaghetti Alfredo + Ensalada de la casa.',
         'categoria': combinados},
        {'nombre': 'Involtini Valdostana', 'precio': 21900,
         'descripcion': 'Rollitos de cerdo rellenos con tocineta y mozzarella + Fetuccini en salsa de champiñones.',
         'categoria': combinados},
        {'nombre': 'Ossobuco', 'precio': 22900,
         'descripcion': 'Corte de la pierna del cerdo con jueso, cocido lentamente en salsa de vegetales + Risotto Alfredo + Ensalada de la casa.',
         'categoria': combinados},
        {'nombre': 'Treviso', 'precio': 17900, 'descripcion': 'Filete de pollo + Spaghetti Alfredo + Ensalada Cesar.',
         'categoria': combinados},
        {'nombre': 'Luciano', 'precio': 13100, 'descripcion': 'Canelon de pollo, jamon y champiñones + Ensalda Cesar.',
         'categoria': combinados},
        {'nombre': 'Alberobello', 'precio': 25900,
         'descripcion': 'Atun fresco encontrado en ajonjoli + Risotto de vegetales.', 'categoria': combinados},
        {'nombre': 'Pesacara', 'precio': 21900,
         'descripcion': 'Filete de trucha arcoiris + Fetuccini en crema de Pesto + Esparragos a la parrilla.',
         'categoria': combinados},
        {'nombre': 'Verona', 'precio': 21900,
         'descripcion': 'Filete de salmon + Spaghetti Alfredo + Ensalada de la casa.', 'categoria': combinados},
        {'nombre': 'Turin', 'precio': 23900,
         'descripcion': 'Filete de samon en finas hierbas + Fetuccini al Pesto + Ensalada de la casa.',
         'categoria': combinados},
        {'nombre': 'Vicenzo', 'precio': 25900,
         'descripcion': 'Filete de salmon + Risotto cinco quesos + Ensalada Cesar.', 'categoria': combinados},
        {'nombre': 'Miguel Angel', 'precio': 24100,
         'descripcion': 'Filete de tilapia cubierto con camarones + Spaghetti al Pesto.', 'categoria': combinados},
        {'nombre': 'Obispo', 'precio': 24400, 'descripcion': 'Solomito + Spaghetti Carbonara + Ensalada de la casa.',
         'categoria': combinados},
        {'nombre': 'Sterling', 'precio': 25900,
         'descripcion': 'Solomito Sterling termino medio (250 g) + Papa al horno gratinada + Ensalaada de la casa.',
         'categoria': combinados},
        {'nombre': 'Positano', 'precio': 25900, 'descripcion': 'Solomito + Risotto funghi + Ensalada Cesar.',
         'categoria': combinados}
    )
    # {'nombre' : '', 'precio' : , 'descripcion' : '', 'categoria' : } ñ
    raviolis = Categoria.get(nombre='Raviolis')

    nombres_platos_raviolis = (
        {'nombre': 'Vegetariano', 'precio': 16900,
         'descripcion': 'Espinaca, Queso Ricotta y mozzarella. SALSA: Alfredo - Napolitana - Rosa - Champiñones - Mixta(quesos y napolitana).',
         'categoria': raviolis},
        {'nombre': 'Pollo', 'precio': 16900,
         'descripcion': 'SALSA: Alfredo - Napolitana - Rosa - Champiñones - Mixta(quesos y napolitana).',
         'categoria': raviolis},
        {'nombre': 'Carne', 'precio': 16900,
         'descripcion': 'SALSA: Alfredo - Napolitana - Rosa - Champiñones - Mixta(quesos y napolitana).',
         'categoria': raviolis},
        {'nombre': 'Salmon', 'precio': 18900,
         'descripcion': 'Salmon, queso crema y cebollin. SALSA: Alfredo - Napolitana - Rosa - Champiñones - Mixta(quesos y napolitana).',
         'categoria': raviolis},
    )

    risottos = Categoria.get(nombre='Risottos')

    nombres_platos_risottos = (
        {'nombre': 'Cinco quesos', 'precio': 18900,
         'descripcion': 'Salsa cinco quesos con mozzarella, parmesano, Ricotta y queso azul.', 'categoria': risottos},
        {'nombre': 'Vegetales', 'precio': 23900,
         'descripcion': 'Laminas de champiñon, tomate en cubos, alcachofas y esparragos frescos, en salsa alfredo y queso parmesano.',
         'categoria': risottos},
        {'nombre': 'Funghi', 'precio': 24900,
         'descripcion': 'Laminas de portobello y champiñones, tomates secos, jamon serrano, cilantro y ajo, en salsa de champiñones.',
         'categoria': risottos},
        {'nombre': 'Mariscos', 'precio': 29900,
         'descripcion': 'Langostinos, camarones, pulpo, calamares y pimentones asados, en salsa bisque con aceite de oliva, mantequilla de ajo y queso parmesano',
         'categoria': risottos},
    )

    sanduches = Categoria.get(nombre='Sanduches')

    nombres_platos_sanduches = (
        {'nombre': 'Saltimbocca', 'precio': 16500,
         'descripcion': 'Pan de masa de pizza, con jamon ahumado, queso mozzarella, bocconcini, rodajas de tomate, lechuga y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Veneciano', 'precio': 21900,
         'descripcion': 'Pechuga de pavo, lomo ahumando, jamon ahumando, queso mozzarella, queso holandes, rodajas de tomate verde, con mayonesa de pimentones ahumados y cebollin.',
         'categoria': sanduches},
        {'nombre': 'Maturo', 'precio': 21900,
         'descripcion': 'Lomo ahumado, jamon ahumado, pepperoni, salami, chorizo español, queso mozzarella, queso holandes y mayonesa balsamica.',
         'categoria': sanduches},
        {'nombre': 'Vegetariano', 'precio': 16900,
         'descripcion': 'Portobello, champiñones y brocoli, salteados en una reduccion de balsamico, queso mozzarella, queso parmesano, rodajas de tomate, pepino, rodajas de tomate, pepino, pimentones asados y variedad de lechugas, con queso crema de ajonjoli y pesto.',
         'categoria': sanduches},
        {'nombre': 'Pollo BBQ', 'precio': 17700,
         'descripcion': 'Cubos de pollo salteado en salsa BBQ, queso mozzarella, cebolla caramelizadaa y tocineta, con mayonesa y salsa BBQ.',
         'categoria': sanduches},
        {'nombre': 'Da Vinci', 'precio': 16900,
         'descripcion': 'Pollo desmechado, queso holandes, queso mozzaarella, champiñones salteados y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Cobb', 'precio': 19900,
         'descripcion': 'Julianas de pollo ala plancha, variedad de lechugas en vinagreta cobb, queso mozzarella, aguacate, tomates cherry, tocineta y huevo en casquitos, en salsa de queso azul y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Pollo Carbonara', 'precio': 17900,
         'descripcion': 'Julianas de pollo a la plancha, queso, mozzarella, laminas de portobello, tocineta y salsa carbonara.',
         'categoria': sanduches},
        {'nombre': 'Teriyaki', 'precio': 17500,
         'descripcion': 'Julianas de pollo a la plancha en salsa teriyaki y ajonjoli, variedad de lechugas en salsa cesa, queso mozzarella, aguacate y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Roast Beef', 'precio': 19900,
         'descripcion': 'Roast beed, queso holandes, cebolla caramelizada, pimentones asados y champiñones salteados, con queso, crema de ajo y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Manzo', 'precio': 19900,
         'descripcion': 'Roast beef, queso mozzarella, tocineta, cebolla caramelizada, chimichurri y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Rafael', 'precio': 19100,
         'descripcion': 'Lomo ahumado, salami, jamon ahumado, queso mozzarella, champiñones salteados y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Leonardo', 'precio': 16900,
         'descripcion': 'Lomo ahumado, queso mozzarella, tomates verdes, tocineta, cebolla caramelizada, mostazaaa y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Julio Cesar', 'precio': 17900,
         'descripcion': 'Lomo ahumado, queso mozzarella, laminas de portobello, salsa de queso azul y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Torino', 'precio': 16900,
         'descripcion': 'Jamon ahumado, tocineta, queso mozzarella, maiz tierno, champiñones salteados y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Brunelleschi', 'precio': 16800,
         'descripcion': 'Jamon ahumado, queso mozzarella, trozos de piña, rodajas de tomate, tocineta y salsa napolitana.',
         'categoria': sanduches},
        {'nombre': 'Arzobispal', 'precio': 21900,
         'descripcion': 'Julianas de solomito a la plancha, queso mozzarella, variedad de lechugas, rodajas de tomate, salsa napolitana y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Siena', 'precio': 21900,
         'descripcion': 'Filete de solomito a la plancha, queso mozzarella, variedad de lechugas, cebolla caramelizada, pimentones asados, tomates secos, aceite de oliva, reduccion de balsamico, pesto y mayonesa.',
         'categoria': sanduches},
        {'nombre': 'Salmon a la plancha', 'precio': 21900,
         'descripcion': 'Cubos de salmon a la plancha, queso mozzarella, laminas de portobello apanadas, rugula y queso crema de ajonjoli al pesto.',
         'categoria': sanduches},
        {'nombre': 'Antillano', 'precio': 19900,
         'descripcion': 'Camarones al ajillo en salsa rosada, aguacate y variedad de lechugas.', 'categoria': sanduches}
    )

    crear_items(nombres_platos_postres)
    crear_items(nombres_platos_cafes)
    crear_items(nombres_platos_gratinados)
    crear_items(nombres_platos_lasanas)
    crear_items(nombres_platos_canelones)
    crear_items(nombres_platos_combinados)
    crear_items(nombres_platos_raviolis)
    crear_items(nombres_platos_risottos)
    crear_items(nombres_platos_sanduches)

    cat1 = Categoria.get(nombre="Vinos Tintos")
    cat2 = Categoria.get(nombre="Vinos Blancos")
    cat3 = Categoria.get(nombre="Vinos Rosados")
    cat4 = Categoria.get(nombre="Sangrias")
    cat5 = Categoria.get(nombre="Cocteles")
    cat6 = Categoria.get(nombre="Cervezas")
    cat7 = Categoria.get(nombre="Licores")
    cat8 = Categoria.get(nombre="Sodas Saborizadas")
    cat9 = Categoria.get(nombre="Jugos Naturales")
    cat10 = Categoria.get(nombre="Otras Bebidas")
    cat11 = Categoria.get(nombre="Entradas")
    cat12 = Categoria.get(nombre="Sopas")
    cat13 = Categoria.get(nombre="Bruschettas")
    cat14 = Categoria.get(nombre="Ensaladas")
    cat15 = Categoria.get(nombre="Pizzas")
    cat16 = Categoria.get(nombre="Calzonis")

    # LLenado de la categoria: Vinoss Tintos
    item1, creado = ItemMenu.get_or_create(nombre="COPA DE VINO DE LA CASA", precio=9900.0, descripcion="",
                                           categoria=cat1)
    item2, creado = ItemMenu.get_or_create(nombre="COPA DE VINO CALIENTE", precio=12500.0, descripcion="",
                                           categoria=cat1)
    item3, creado = ItemMenu.get_or_create(nombre="COUSIÑO MACUL DON LUIS - MEDIA", precio=27000.0,
                                           descripcion="Cabernet Sauvignon", categoria=cat1)
    item4, creado = ItemMenu.get_or_create(nombre="COUSIÑO MACUL DON LUIS - BOTELLA", precio=48000.0,
                                           descripcion="Cabernet Sauvignon", categoria=cat1)
    item5, creado = ItemMenu.get_or_create(nombre="CASTILLO DE MOLINA RESERVA - MEDIA", precio=43000.0,
                                           descripcion="Cabernet Sauvignon", categoria=cat1)
    item6, creado = ItemMenu.get_or_create(nombre="CASTILLO DE MOLINA RESERVA - BOTELLA", precio=79000.0,
                                           descripcion="Cabernet Sauvignon", categoria=cat1)
    item7, creado = ItemMenu.get_or_create(nombre="SANTA RITA 120 - BOTELLA", precio=58000.0,
                                           descripcion="Cabernet Sauvignon", categoria=cat1)
    item8, creado = ItemMenu.get_or_create(nombre="LAS MORAS JOVEN - BOTELLA", precio=48000.0, descripcion="Malbec",
                                           categoria=cat1)
    item9, creado = ItemMenu.get_or_create(nombre="LAS MORAS RESERVA - BOTELLA", precio=67000.0, descripcion="Malbec",
                                           categoria=cat1)
    item10, creado = ItemMenu.get_or_create(nombre="LA CONSULTA RESERVA - MEDIA", precio=28000.0, descripcion="Malbec",
                                            categoria=cat1)
    item11, creado = ItemMenu.get_or_create(nombre="LA CONSULTA RESERVA - BOTELLA", precio=53000.0,
                                            descripcion="Malbec", categoria=cat1)
    item12, creado = ItemMenu.get_or_create(nombre="CATENA ZAPATA - BOTELLA", precio=103000.0, descripcion="Malbec",
                                            categoria=cat1)
    item13, creado = ItemMenu.get_or_create(nombre="COUSIÑO MACUL DON LUIS - BOTELLA", precio=48000.0,
                                            descripcion="Meriot", categoria=cat1)
    item14, creado = ItemMenu.get_or_create(nombre="J.P. CHENET - BOTELLA", precio=58000.0, descripcion="Meriot",
                                            categoria=cat1)
    item15, creado = ItemMenu.get_or_create(nombre="CASTILLO DE MOLINA RESERVA - BOTELLA", precio=79000.0,
                                            descripcion="Carmenere", categoria=cat1)
    item16, creado = ItemMenu.get_or_create(nombre="PICCINI - BOTELLA", precio=55000.0, descripcion="Lambrusco Tinto",
                                            categoria=cat1)

    # Llenado de las categorias: Vinos Blancos
    item17, creado = ItemMenu.get_or_create(nombre="COPA DE VINO DE LA CASA", precio=9900.0, descripcion="",
                                            categoria=cat2)
    item18, creado = ItemMenu.get_or_create(nombre="COPA DE VINO PROSECCO ESPUMOSO", precio=12900.0, descripcion="",
                                            categoria=cat2)
    item19, creado = ItemMenu.get_or_create(nombre="COUSINO MACUL DON LUIS - MEDIA", precio=27000.0,
                                            descripcion="Sauvignon Blanc", categoria=cat2)
    item20, creado = ItemMenu.get_or_create(nombre="COUSINO MACUL DON LUIS - BOTELLA", precio=48000.0,
                                            descripcion="Sauvignon Blanc", categoria=cat2)
    item21, creado = ItemMenu.get_or_create(nombre="COUSINO MACUL DON LUIS - BOTELLA", precio=48000.0,
                                            descripcion="Chardonnay", categoria=cat2)
    item22, creado = ItemMenu.get_or_create(nombre="CASTILLO DE MOLINA RESERVA - MEDIA", precio=43000.0,
                                            descripcion="Chardonnay", categoria=cat2)
    item23, creado = ItemMenu.get_or_create(nombre="CASTILLO DE MOLINA RESERVA - BOTELLA", precio=79900.0,
                                            descripcion="Chardonnay", categoria=cat2)
    item24, creado = ItemMenu.get_or_create(nombre="LA CONSULTA RESERVA - BOTELLA", precio=53000.0,
                                            descripcion="Torrontés", categoria=cat2)
    item25, creado = ItemMenu.get_or_create(nombre="J.P. CHENET ICE DEMI SEC - MEDIA", precio=24900.0,
                                            descripcion="Espumoso Francés", categoria=cat2)
    item26, creado = ItemMenu.get_or_create(nombre="J.P. CHENET ICE DEMI SEC - BOTELLA", precio=58000.0,
                                            descripcion="Espumoso Francés", categoria=cat2)

    # Llenado de las categorias: Vinos Rsados
    item27, creado = ItemMenu.get_or_create(nombre="COPA DE VINO DE LA CASA", precio=9900.0, descripcion="",
                                            categoria=cat3)
    item28, creado = ItemMenu.get_or_create(nombre="J.P CHENET - MEDIA", precio=24900.0,
                                            descripcion="Grenache/Cinsault", categoria=cat3)
    item29, creado = ItemMenu.get_or_create(nombre="J.P CHENET - BOTELLA", precio=58000.0,
                                            descripcion="Grenache/Cinsault", categoria=cat3)
    item30, creado = ItemMenu.get_or_create(nombre="J.P CHENET SPARKLING ROSÉ - MEDIA", precio=24900.0,
                                            descripcion="Espumoso Francés", categoria=cat3)
    item31, creado = ItemMenu.get_or_create(nombre="J.P CHENET SPARKLING ROSÉ - BOTELLA", precio=58000.0,
                                            descripcion="Espumoso Francés", categoria=cat3)
    item32, creado = ItemMenu.get_or_create(nombre="LAS MORAS - BOTELLA", precio=48000.0, descripcion="Syrah",
                                            categoria=cat3)
    item33, creado = ItemMenu.get_or_create(nombre="COUSIÑO MACUL GRIS - BOTELLA", precio=48000.0,
                                            descripcion="Cabernet Sauvignon", categoria=cat3)
    item34, creado = ItemMenu.get_or_create(nombre="SANTA RITA 120 - BOTELLA", precio=58000.0,
                                            descripcion="Cabernet Sauvignon", categoria=cat3)
    item35, creado = ItemMenu.get_or_create(nombre="CASTILLO DE MOLINA RESERVA - BOTELLA", precio=79000.0,
                                            descripcion="Cabernet Syrah", categoria=cat3)
    item36, creado = ItemMenu.get_or_create(nombre="PICCINI - BOTELLA", precio=55000.0, descripcion="Lambrusco Rosado",
                                            categoria=cat3)

    # Llenado de las categorias: Sangrias
    item37, creado = ItemMenu.get_or_create(nombre="VINO TINTO", precio=14300.0, descripcion="Copa", categoria=cat4)
    item38, creado = ItemMenu.get_or_create(nombre="VINO BLANCO", precio=14300.0, descripcion="Copa", categoria=cat4)
    item39, creado = ItemMenu.get_or_create(nombre="VINO ROSADO", precio=14300.0, descripcion="Copa", categoria=cat4)
    item40, creado = ItemMenu.get_or_create(nombre="VINO TINTO", precio=34900.0, descripcion="Media Jarra",
                                            categoria=cat4)
    item41, creado = ItemMenu.get_or_create(nombre="VINO BLANCO", precio=34900.0, descripcion="Media Jarra",
                                            categoria=cat4)
    item42, creado = ItemMenu.get_or_create(nombre="VINO ROSADO", precio=34900.0, descripcion="Media Jarra",
                                            categoria=cat4)
    item43, creado = ItemMenu.get_or_create(nombre="VINO TINTO", precio=59900.0, descripcion="Jarra", categoria=cat4)
    item44, creado = ItemMenu.get_or_create(nombre="VINO BLANCO", precio=59900.0, descripcion="Jarra", categoria=cat4)
    item45, creado = ItemMenu.get_or_create(nombre="VINO ROSADO", precio=59900.0, descripcion="Jarra", categoria=cat4)
    item46, creado = ItemMenu.get_or_create(nombre="VINO ROSADO ESPUMOSO", precio=65000.0, descripcion="Jarra",
                                            categoria=cat4)

    # Llenado de las categorias: Cocteles
    item47, creado = ItemMenu.get_or_create(nombre="APEROL SPRITZ", precio=17900.0, descripcion="Exclusivo antioquia",
                                            categoria=cat5)
    item48, creado = ItemMenu.get_or_create(nombre="MOJITO ZACAPA 23 AÑOS", precio=17900.0, descripcion=" ",
                                            categoria=cat5)
    item49, creado = ItemMenu.get_or_create(nombre="MOJITO VINO TINTO", precio=15900.0, descripcion=" ", categoria=cat5)
    item50, creado = ItemMenu.get_or_create(nombre="ZACAPA PUNCH", precio=17900.0, descripcion=" ", categoria=cat5)
    item51, creado = ItemMenu.get_or_create(nombre="MARGARITA JOSE CUERVO", precio=15900.0, descripcion="Limon o Fresa",
                                            categoria=cat5)
    item52, creado = ItemMenu.get_or_create(nombre="MARGARITA DON JULIO", precio=17900.0, descripcion="Limon o Fresa",
                                            categoria=cat5)
    item53, creado = ItemMenu.get_or_create(nombre="LYCHEE MARTINI", precio=17900.0, descripcion=" ", categoria=cat5)
    item54, creado = ItemMenu.get_or_create(nombre="GIN & TONIC TANQUERAY", precio=17900.0, descripcion=" ",
                                            categoria=cat5)
    item55, creado = ItemMenu.get_or_create(nombre="GIN & TONIC GORDON'S", precio=15900.0, descripcion=" ",
                                            categoria=cat5)
    item56, creado = ItemMenu.get_or_create(nombre="BLACKBERRY GIN HATSU", precio=17900.0, descripcion=" ",
                                            categoria=cat5)
    item57, creado = ItemMenu.get_or_create(nombre="CAIPIROSKA CIROC", precio=17900.0, descripcion=" ", categoria=cat5)
    item58, creado = ItemMenu.get_or_create(nombre="BLOODY MARY", precio=15900.0, descripcion=" ", categoria=cat5)

    # Llenado de las categorias: Cervezar
    item59, creado = ItemMenu.get_or_create(nombre="STELLA ARTOIS", precio=8500.0, descripcion=" ", categoria=cat6)
    item60, creado = ItemMenu.get_or_create(nombre="CORONA", precio=8500.0, descripcion=" ", categoria=cat6)
    item61, creado = ItemMenu.get_or_create(nombre="BOGOTÁ BEER COMPANY - BBC", precio=8500.0, descripcion=" ",
                                            categoria=cat6)
    item62, creado = ItemMenu.get_or_create(nombre="3 CORDILLERAS", precio=8500.0, descripcion=" ", categoria=cat6)
    item63, creado = ItemMenu.get_or_create(nombre="CLUB COLOMBIA", precio=6000.0, descripcion="Dorada, Roja o Negra",
                                            categoria=cat6)
    item64, creado = ItemMenu.get_or_create(nombre="ÁGUILA", precio=5000.0, descripcion=" ", categoria=cat6)
    item65, creado = ItemMenu.get_or_create(nombre="ÁGUILA LIGHT", precio=5000.0, descripcion=" ", categoria=cat6)
    item66, creado = ItemMenu.get_or_create(nombre="ÁGUILA CERO", precio=5000.0, descripcion=" ", categoria=cat6)
    item67, creado = ItemMenu.get_or_create(nombre="PILSEN", precio=5000.0, descripcion=" ", categoria=cat6)
    item68, creado = ItemMenu.get_or_create(nombre="JARRA DE REFAJO", precio=25000.0, descripcion=" ", categoria=cat6)

    # Llenado de las categorias: Licores
    item69, creado = ItemMenu.get_or_create(nombre="RON 3 AÑOS", precio=4900.0, descripcion="Trago", categoria=cat7)
    item70, creado = ItemMenu.get_or_create(nombre="RON 8 AÑOS", precio=6300.0, descripcion="Trago", categoria=cat7)
    item71, creado = ItemMenu.get_or_create(nombre="RON ZACAPA 23 AÑOS", precio=11600.0, descripcion="Trago",
                                            categoria=cat7)
    item72, creado = ItemMenu.get_or_create(nombre="WHISKY OLD PARR", precio=10900.0, descripcion="Trago",
                                            categoria=cat7)
    item73, creado = ItemMenu.get_or_create(nombre="WHISKY SELLO ROJO", precio=7100.0, descripcion="Trago",
                                            categoria=cat7)
    item74, creado = ItemMenu.get_or_create(nombre="WHISKY BUCHANAN'S", precio=10900.0, descripcion="Trago",
                                            categoria=cat7)
    item75, creado = ItemMenu.get_or_create(nombre="WHISKY BUCHANAN'S MASTER", precio=12400.0, descripcion="Trago",
                                            categoria=cat7)
    item76, creado = ItemMenu.get_or_create(nombre="AGUARDIENTE", precio=4900.0, descripcion="Trago", categoria=cat7)
    item77, creado = ItemMenu.get_or_create(nombre="TEQUILA DON JULIO REPOSADO", precio=14200.0, descripcion="Trago",
                                            categoria=cat7)
    item78, creado = ItemMenu.get_or_create(nombre="TEQUILA JOSE CUERVO", precio=7900.0, descripcion="Trago",
                                            categoria=cat7)
    item79, creado = ItemMenu.get_or_create(nombre="VODKA SMIRNOFF", precio=7900.0, descripcion="Trago", categoria=cat7)
    item80, creado = ItemMenu.get_or_create(nombre="VODKA CIROC", precio=10400.0, descripcion="Trago", categoria=cat7)
    item81, creado = ItemMenu.get_or_create(nombre="GINEBRA GORDON'S", precio=8100.0, descripcion="Trago",
                                            categoria=cat7)
    item82, creado = ItemMenu.get_or_create(nombre="GINEBRA TANQUERAY", precio=10900.0, descripcion="Trago",
                                            categoria=cat7)
    item83, creado = ItemMenu.get_or_create(nombre="AMARETTO", precio=4900.0, descripcion="Trago", categoria=cat7)
    item84, creado = ItemMenu.get_or_create(nombre="BRANDY DOMECQ", precio=5900.0, descripcion="Trago", categoria=cat7)
    item85, creado = ItemMenu.get_or_create(nombre="BAILEYS", precio=8800.0, descripcion="Trago", categoria=cat7)
    item86, creado = ItemMenu.get_or_create(nombre="DUBONNET", precio=8200.0, descripcion="Trago", categoria=cat7)

    # Llenado de las categorias: Sodas saborizadas
    item87, creado = ItemMenu.get_or_create(nombre="SANDÍA - LIMÓN", precio=5000.0, descripcion=" ", categoria=cat8)
    item88, creado = ItemMenu.get_or_create(nombre="PEPINO - LIMÓN", precio=5000.0, descripcion=" ", categoria=cat8)
    item89, creado = ItemMenu.get_or_create(nombre="FLOR DE JAMAICA", precio=5000.0, descripcion=" ", categoria=cat8)

    # Llenado de las categorias: Jugos naturales
    item90, creado = ItemMenu.get_or_create(nombre="MANDARINA", precio=4900.0, descripcion=" ", categoria=cat9)
    item91, creado = ItemMenu.get_or_create(nombre="BAHAMAS", precio=5500.0, descripcion="Mango, Maracuyá y Piña",
                                            categoria=cat9)
    item92, creado = ItemMenu.get_or_create(nombre="LIMONADA DE COCO", precio=5500.0, descripcion=" ", categoria=cat9)
    item93, creado = ItemMenu.get_or_create(nombre="LIMONADA", precio=4500.0, descripcion=" ", categoria=cat9)
    item94, creado = ItemMenu.get_or_create(nombre="MANDARINA - FRESA", precio=5500.0, descripcion=" ", categoria=cat9)
    item95, creado = ItemMenu.get_or_create(nombre="MANGO", precio=4500.0, descripcion=" ", categoria=cat9)
    item96, creado = ItemMenu.get_or_create(nombre="FRESA", precio=4500.0, descripcion=" ", categoria=cat9)
    item97, creado = ItemMenu.get_or_create(nombre="MORA", precio=4500.0, descripcion=" ", categoria=cat9)
    item98, creado = ItemMenu.get_or_create(nombre="PIÑA - HIERBABUENA", precio=5500.0, descripcion=" ", categoria=cat9)

    # Llenado de las categorias: Otros jugos
    item99, creado = ItemMenu.get_or_create(nombre="MALTEADAS", precio=6900.0, descripcion=" ", categoria=cat10)
    item100, creado = ItemMenu.get_or_create(nombre="TÉ HELADO", precio=3500.0, descripcion=" ", categoria=cat10)
    item101, creado = ItemMenu.get_or_create(nombre="TÉ HELADO LIGHT", precio=3500.0, descripcion=" ", categoria=cat10)
    item102, creado = ItemMenu.get_or_create(nombre="TÉ HATSU", precio=6000.0, descripcion=" ", categoria=cat10)
    item103, creado = ItemMenu.get_or_create(nombre="TÉ XENCHA", precio=6000.0, descripcion=" ", categoria=cat10)
    item104, creado = ItemMenu.get_or_create(nombre="GASEOSA", precio=3500.0, descripcion=" ", categoria=cat10)
    item105, creado = ItemMenu.get_or_create(nombre="AGUA EN BOTELLA", precio=3500.0, descripcion=" ", categoria=cat10)
    item106, creado = ItemMenu.get_or_create(nombre="AGUA AROMÁTICA", precio=2800.0, descripcion=" ", categoria=cat10)

    # Llenado de las categorias: Entradas
    item107, creado = ItemMenu.get_or_create(nombre="PANCITOS", precio=3500.0,
                                             descripcion="Recien salidos del horno, con mantequilla de hierbas y ajo",
                                             categoria=cat11)
    item108, creado = ItemMenu.get_or_create(nombre="ANTIPASTO IL FORNO", precio=31000.0,
                                             descripcion="Salami, jamón serrano, chorizo español, aceitunas verdes, pimentones asados, tomates secos, tomates cherry, queso holandés y cubos de queso mozzarella marinados en aceite de hierbas; acompañados con bruschettas de pan ciabatta o pan integral.",
                                             categoria=cat11)
    item109, creado = ItemMenu.get_or_create(nombre="PULPO AL PESTO SOBRE RATATOUILLE", precio=21000.0,
                                             descripcion="Tentáculos de pulpo servidos sobre rodajas de tomate, berenjena y zukini, cocinadas al horno sobre salsa napolitana",
                                             categoria=cat11)
    item110, creado = ItemMenu.get_or_create(nombre="TOMATES RELLENOS DE SOLOMITO", precio=12800.0,
                                             descripcion="Tomates rellenos con trozos de solomito, tocineta, queso mozzrella y albahaca, en salsa de pimienta; gratinados con queso mozzarella y parmesano.",
                                             categoria=cat11)
    item111, creado = ItemMenu.get_or_create(nombre="PATATA ROMANA", precio=12800.0,
                                             descripcion="Papa rellena con trozos de solomito, tocineta, tomate, albahaca y un toque de salsa napolitana; gratinada con queso mozzarella y parmesano.",
                                             categoria=cat11)
    item112, creado = ItemMenu.get_or_create(nombre="PATATA IMPERIAL", precio=10300.0,
                                             descripcion="Papa rellena con pollo desmechado, jamón ahumado, tocineta y un toque de salsa napolitana; gratinada con queso mozzarella y parmesano.",
                                             categoria=cat11)
    item113, creado = ItemMenu.get_or_create(nombre="PORTOBELLO FIORENTINO", precio=10900.0,
                                             descripcion="Champiñón portobello con jamón ahumado, rúgula fresca, queso mozzarella, parmesano y salsa de queso azul.",
                                             categoria=cat11)
    item114, creado = ItemMenu.get_or_create(nombre="PORTOBELLO GRIEGO", precio=10900.0,
                                             descripcion="Champiñón portobello con queso feta, tomates secos, rodajas de tomate marinadas en aceite de hierbas, albahaca, reducción de balsámico y pesto.",
                                             categoria=cat11)
    item115, creado = ItemMenu.get_or_create(nombre="CARPACCIO DE PULPO", precio=18600.0,
                                             descripcion="Finos cortes de pulpo con vinagreta de mostaza, ajonjolí y limón; acompañados con bruschettas de pan ciabatta o pan integral.",
                                             categoria=cat11)
    item116, creado = ItemMenu.get_or_create(nombre="CARPACCIO DE SALMÓN AHUMADO", precio=19900.0,
                                             descripcion="Finos cortes de salmón ahumado, cubos de pepino, manzana verde, alcaparras y semillas de chía, con aceite de oliva y limón; acompañados con bruschettas de pan ciabatta o pan integral, rociadas con aceite de oliva y ajonjolí.",
                                             categoria=cat11)
    item117, creado = ItemMenu.get_or_create(nombre="CARPACCIO DE RES", precio=15600.0,
                                             descripcion="Finos cortes crudos de res, queso parmesano y rúgula, rociados con aceite de oliva y limón; acompañados con bruschettas de pan ciabatta o pan integral. Opcional alcaparras.",
                                             categoria=cat11)
    item118, creado = ItemMenu.get_or_create(nombre="BURRATA DE BÚFALA", precio=19900.0,
                                             descripcion="Queso fresco tradicional italiano, cremoso en su interior, servido con tomates cherry confitados, rúgula marinada en vinagre balsámico y aceite de oliva, sobre pesto.",
                                             categoria=cat11)
    item119, creado = ItemMenu.get_or_create(nombre="BERENJENAS PARMESANAS", precio=9700.0,
                                             descripcion="Berenjenas a la parrilla con salsa napolitana, queso parmesano y albahaca; acompañadas con pan.",
                                             categoria=cat11)
    item120, creado = ItemMenu.get_or_create(nombre="QUESO CAMEMBERT AL HORNO", precio=19900.0,
                                             descripcion="Queso Camembert fundido dentro de su crocante piel, con un toque de dulce y nueces; acompañado de manzanas verdes y bruschettas de pan ciabatta o integral.",
                                             categoria=cat11)

    # Llenado de las categorias: Sopas

    item121, creado = ItemMenu.get_or_create(nombre="MEXICANA", precio=12700.0,
                                             descripcion="Sopa de tomates frescos y maduros con camarones o cubos de pollo, maíz tierno, aguacate, pimentones asados, queso parmesano, crema de leche y un poco de picante; acompañada con pan.",
                                             categoria=cat12)
    item122, creado = ItemMenu.get_or_create(nombre="TOMATE", precio=9000.0,
                                             descripcion="Sopa de tomates frescos y maduros con albahaca, crema de leche y queso parmesano; acompañada con pan.",
                                             categoria=cat12)
    item123, creado = ItemMenu.get_or_create(nombre="CREMA DE CHAMPIÑONES", precio=9800.0,
                                             descripcion="Crema elaborada con champiñones cocidos y salteados en vino blanco, crema de leche y queso parmesano; acompañada con pan.",
                                             categoria=cat12)
    item124, creado = ItemMenu.get_or_create(nombre="MINESTRONE", precio=9500.0,
                                             descripcion="Sopa clásica de verduras salteadas y cocidas (cebolla, tomate, pimentón, arveja, zanahoria y champiñones); acompañada con pan.",
                                             categoria=cat12)
    item125, creado = ItemMenu.get_or_create(nombre="CEBOLLA", precio=9500.0,
                                             descripcion="Tradicional sopa de cebolla con queso mozzarella derretido; acompañada con bruschettas de pan ciabatta.",
                                             categoria=cat12)

    # Llenado de las categorias: Bruschettas

    item126, creado = ItemMenu.get_or_create(nombre="IL FORNO", precio=8900.0,
                                             descripcion="Rebanadas de pan tostado cubiertas con salsa napolitana, rodajas de tomate marinadas con aceite de hierbas, queso mozzarella gratinado, pesto y albahaca.",
                                             categoria=cat13)
    item127, creado = ItemMenu.get_or_create(nombre="ROMANAS", precio=10900.0,
                                             descripcion="Rebanadas de pan tostado cubiertas con salsa napolitana, rodajas de tomate marinadas con aceite de hierbas, queso mozzarella gratinado, jamón serrano, pesto y albahaca.",
                                             categoria=cat13)
    item128, creado = ItemMenu.get_or_create(nombre="CON CAMARONES", precio=12500.0,
                                             descripcion="Rebanadas de pan tostado cubiertas con camarones salteados al ajillo, pesto, queso parmesano y salsa de hierbas.",
                                             categoria=cat13)

    # Llenado de las categorias: Ensaladas

    item129, creado = ItemMenu.get_or_create(nombre="RIMINI", precio=22000.0,
                                             descripcion="Variedad de lechugas, atún fresco encostrado en ajonjolí, marañones, almendras confitadas, alcaparras fritas, fosforitos de cebolla y queso feta con vinagreta ranch. (Término sugerido del atún: medio).",
                                             categoria=cat14)
    item130, creado = ItemMenu.get_or_create(nombre="GRECA", precio=19500.0,
                                             descripcion="Variedad de lechugas con hojas de espinaca, pechuga de pavo, queso feta, tomates cherry, quinua, nueces de pecan, almendras confitadas, arándanos y semillas de chía, con vinagreta griega.",
                                             categoria=cat14)
    item131, creado = ItemMenu.get_or_create(nombre="TERIYAKI", precio=17900.0,
                                             descripcion="Variedad de lechugas marinadas con salsa césar, julianas de pollo en salsa teriyaki, trozos de aguacate, palitos al horno y ajonjolí.",
                                             categoria=cat14)
    item132, creado = ItemMenu.get_or_create(nombre="PRIMAVERA", precio=17900.0,
                                             descripcion="Variedad de lechugas, julianas de pollo a la plancha, láminas de portobello apanadas, pimentones asados y fosforitos de cebolla, con salsa de queso azul.",
                                             categoria=cat14)
    item133, creado = ItemMenu.get_or_create(nombre="IL FORNO", precio=21000.0,
                                             descripcion="Variedad de lechugas, cubos de pollo salteados en ajonjolí, tomates cherry, trozos de aguacate, queso feta, champiñones salteados y tocineta, con vinagreta campiña.",
                                             categoria=cat14)
    item134, creado = ItemMenu.get_or_create(nombre="COBB", precio=17900.0,
                                             descripcion="Variedad de lechugas, julianas de pollo a la plancha, tomates cherry, trozos de aguacate, casquitos de huevo y tocineta, con salsa de queso azul y vinagreta cobb.",
                                             categoria=cat14)
    item135, creado = ItemMenu.get_or_create(nombre="VIOLETTA", precio=17900.0,
                                             descripcion="Variedad de lechugas, pollo a la plancha, almendras confitadas, trozos de queso azul, arándanos y ajonjolí, con salsa de frutos rojos y vinagreta de miel mostaza.",
                                             categoria=cat14)
    item136, creado = ItemMenu.get_or_create(nombre="DEL HUERTO", precio=17900.0,
                                             descripcion="Rebanadas de pan tostado cubiertas con salsa napolitana, rodajas de tomate marinadas con aceite de hierbas, queso mozzarella gratinado, jamón serrano, pesto y albahaca.",
                                             categoria=cat14)
    item137, creado = ItemMenu.get_or_create(nombre="SIMONA", precio=18100.0,
                                             descripcion="Variedad de lechugas, pollo a la plancha, jamón ahumado, queso holandés, maíz tierno, tomate y aguacate, con mostaza y vinagreta campiña.",
                                             categoria=cat14)
    item138, creado = ItemMenu.get_or_create(nombre="CÉSAR DE POLLO", precio=16500.0,
                                             descripcion="Variead de lechugas, cubos de pollo, tomates cherry, crotones y queso parmesano, con salsa césar. ",
                                             categoria=cat14)
    item139, creado = ItemMenu.get_or_create(nombre="CÉSAR DE SALMÓN", precio=21000.0,
                                             descripcion="Variead de lechugas, cubos de salmon, tomates cherry, crotones y queso parmesano, con salsa césar. ",
                                             categoria=cat14)
    item140, creado = ItemMenu.get_or_create(nombre="TATAKI", precio=22000.0,
                                             descripcion="Variedad de lechugas, solomito, láminas de portobello apanadas, queso bocconcini y tomates cherry, con vinagreta de miel mostaza.",
                                             categoria=cat14)
    item141, creado = ItemMenu.get_or_create(nombre="MEDITERRÁNEA", precio=21000.0,
                                             descripcion="Variedad de lechugas, camarones, calamares, tomates cherry, palitroques de cangrejo salteados en mantequilla de la casa, aceitunas verdes y fosforitos de cebolla, con vinagreta mediterránea.",
                                             categoria=cat14)

    # Llenado de las categorias: Pizzas

    item142, creado = ItemMenu.get_or_create(nombre="PAVO Y MANZANA", precio=19900.0,
                                             descripcion="Base tomate, queso mozzarella, pechuga de pavo, manzana caramelizada, arándanos y tocineta.",
                                             categoria=cat15)
    item143, creado = ItemMenu.get_or_create(nombre="CÉSAR", precio=19900.0,
                                             descripcion="Base blanca, queso mozzarella, queso parmesano, pollo en cubos, tomates cherry y variedad de lechugas, con salsa césar.",
                                             categoria=cat15)
    item144, creado = ItemMenu.get_or_create(nombre="IL FORNO", precio=19900.0,
                                             descripcion="Base tomate, queso mozzarella, rodajas de berenjena asadas, tomates secos, champiñones salteados y alcachofas, con reducción de balsámico y aceite de oliva.",
                                             categoria=cat15)
    item145, creado = ItemMenu.get_or_create(nombre="NAPOLI", precio=19900.0,
                                             descripcion="Base blanca, queso mozzarella, queso parmesano, jamón serrano, tomates cherry, rúgula y queso bocconcini.",
                                             categoria=cat15)
    item146, creado = ItemMenu.get_or_create(nombre="BURRATA DE BÚFALA", precio=21900.0,
                                             descripcion="Base tomate o pesto, tomates cherry, albahaca y burrata de búfala con aceite de oliva y queso parmesano.",
                                             categoria=cat15)
    item147, creado = ItemMenu.get_or_create(nombre="CINCO QUESOS", precio=18900.0,
                                             descripcion="Base blanca con cinco quesos: mozzarella, parmesano, ricotta, azul y holandés.",
                                             categoria=cat15)
    item148, creado = ItemMenu.get_or_create(nombre="HAWAIANA", precio=12300.0,
                                             descripcion="Base tomate, queso mozzarella, jamón ahumado y trozos de piña.",
                                             categoria=cat15)
    item149, creado = ItemMenu.get_or_create(nombre="TOSCANA", precio=15300.0,
                                             descripcion="Base tomate, queso mozzarella, jamón ahumado, pimentones asados y champiñones salteados.",
                                             categoria=cat15)
    item150, creado = ItemMenu.get_or_create(nombre="POLLO BBQ", precio=17700.0,
                                             descripcion="Base BBQ, queso mozzarella, pollo a la plancha, cebolla caramelizada y tocineta.",
                                             categoria=cat15)
    item151, creado = ItemMenu.get_or_create(nombre="POLLO MIEL MOSTAZA", precio=17700.0,
                                             descripcion="Base miel mostaza, queso mozzarella, pollo a la plancha, pimentones asados y tocineta.",
                                             categoria=cat15)
    item152, creado = ItemMenu.get_or_create(nombre="NICOLO", precio=22900.0,
                                             descripcion="Base tomate, queso mozzarella, tocineta, salami, jamón ahumado, lomo ahumado y pepperoni.",
                                             categoria=cat15)
    item153, creado = ItemMenu.get_or_create(nombre="FIORENTINA", precio=18800.0,
                                             descripcion="Base tomate, queso mozzarella, jamón serrano, champiñones salteados, rodajas de tomate marinadas en aceite de hierbas y cebolla caramelizada.",
                                             categoria=cat15)
    item154, creado = ItemMenu.get_or_create(nombre="CARPACCIO DE RES", precio=18900.0,
                                             descripcion="Base tomate, queso mozzarella, queso parmesano, finos cortes crudos de res, rúgula y aceite de oliva",
                                             categoria=cat15)
    item155, creado = ItemMenu.get_or_create(nombre="CARBONARA", precio=17500.0,
                                             descripcion="Base carbonara, queso mozzarella, queso parmesano, láminas de portobello y tocineta.",
                                             categoria=cat15)
    item156, creado = ItemMenu.get_or_create(nombre="CAPRESSE", precio=12100.0,
                                             descripcion="Base tomate, queso mozzarella, rodajas de tomate y albahaca.",
                                             categoria=cat15)
    item157, creado = ItemMenu.get_or_create(nombre="SALMÓN AHUMADO", precio=21900.0,
                                             descripcion="Base tomate, queso mozzarella, salmón ahumado, queso crema, cebollín y alcaparras.",
                                             categoria=cat15)
    item158, creado = ItemMenu.get_or_create(nombre="VARENNA", precio=19900.0,
                                             descripcion="Base pesto, camarones al ajillo, aguacate, tomates cherry y rúgula.",
                                             categoria=cat15)

    # Llenado de las categorias: Calzonis

    item159, creado = ItemMenu.get_or_create(nombre="VERNAZZA", precio=18100.0,
                                             descripcion="Salsa de queso azul, jamón serrano, tomates cherry, cebolla caramelizada y rúgula.",
                                             categoria=cat16)
    item160, creado = ItemMenu.get_or_create(nombre="FIESOLE", precio=13900.0,
                                             descripcion="Salsa napolitana, queso mozzarella, jamón ahumado y champiñones.",
                                             categoria=cat16)
    item161, creado = ItemMenu.get_or_create(nombre="CALABRIA", precio=16800.0,
                                             descripcion="Salsa napolitana, queso mozzarella, jamón ahumado, tocineta, piña y maíz tierno con un toque de mostaza.",
                                             categoria=cat16)
    item162, creado = ItemMenu.get_or_create(nombre="PARMA", precio=18900.0,
                                             descripcion="Salsa napolitana, queso mozzarella, salami, jamón ahumado, peperoni, pimentones asados, tomate y cebolla.",
                                             categoria=cat16)
    item163, creado = ItemMenu.get_or_create(nombre="MILÁN", precio=14900.0,
                                             descripcion="Salsa napolitana, queso mozzarella, pollo y champiñones.",
                                             categoria=cat16)

    # Llenado de las categorias: Ensaladas (otra vez)

    item164, creado = ItemMenu.get_or_create(nombre="QUINUA", precio=500.0,
                                             descripcion="Lechuga, tomates cherry, cascos de mandarina, quinua y semillas de chía con vinagreta griega. 1/2 porción.",
                                             categoria=cat15)
    item165, creado = ItemMenu.get_or_create(nombre="ENSALADA CÉSAR", precio=5500.0,
                                             descripcion="Lechuga en vinagreta césar con crotones y queso parmesano. 1/2 porción.",
                                             categoria=cat15)
    item166, creado = ItemMenu.get_or_create(nombre="ENSALADA DE LA CASA", precio=4500.0,
                                             descripcion="Lechuga y tomate en cubos con vinagreta de cilantro. 1/2 porción.",
                                             categoria=cat15)


def crear_items(lista_nombres):
    for item in lista_nombres:
        item_menu = ItemMenu()
        item_menu.nombre = item['nombre']
        item_menu.precio = item['precio']
        item_menu.descripcion = item['descripcion']
        item_menu.categoria = item['categoria']
        item_menu.crear_item_menu()


def crear_sucursal_base():
    sucursal = Sucursal(nombre='IlForno',direccion='Avenida Roosevelt #39A-20',telefono='554 65 94')
    sucursal.save()

    mesa1 = Mesa(sucursal=sucursal,numero_interno=1)
    mesa1.save()

    mesa1 = Mesa(sucursal=sucursal, numero_interno=2)
    mesa1.save()

    mesa1 = Mesa(sucursal=sucursal, numero_interno=3)
    mesa1.save()

    empleado1 = Empleado(nombres="Jose Alejandro", sucursal=sucursal,
                        numero_documento_identificacion="1234",
                        tipo_documento_identificacion='PASAPORTE', cargo="MESERO")
    empleado1.asignar_contrasena("123")
    empleado1.save()

    empleado2 = Empleado(nombres="Juan Jose" , sucursal=sucursal,
                        numero_documento_identificacion="01234",
                        tipo_documento_identificacion='CÉDULA DE CIUDADANÍA', cargo="MESERO")
    empleado2.asignar_contrasena("123")
    empleado2.save()

    empleado3 = Empleado(nombres="Ivan" , sucursal=sucursal,
                        numero_documento_identificacion="12345" ,
                        tipo_documento_identificacion='CÉDULA DE CIUDADANÍA', cargo="GERENTE")
    empleado3.asignar_contrasena("123")
    empleado3.save()

    empleado4 = Empleado(nombres="Juan David" , sucursal=sucursal,
                        numero_documento_identificacion="123" ,
                        tipo_documento_identificacion='PASAPORTE', cargo="CAJERO")
    empleado4.asignar_contrasena("123")
    empleado4.save()

    empleados = Empleado.select().where((Empleado.numero_documento_identificacion == '123')
                                        | (Empleado.numero_documento_identificacion == '1234')
                                        | (Empleado.numero_documento_identificacion == '01234')
                                        | (Empleado.numero_documento_identificacion == '12345'))

    for empleado in empleados:

        turno = Turno.get_or_create(empleado=empleado,dia='LUNES',hora_inicio='08:00:00.00',hora_fin='20:00:00.00')

        turno = Turno.get_or_create(empleado=empleado, dia='MARTES', hora_inicio='08:00:00.00', hora_fin='20:00:00.00')

        turno = Turno.get_or_create(empleado=empleado, dia='MIERCOLES', hora_inicio='08:00:00.00', hora_fin='20:00:00.00')

        turno = Turno.get_or_create(empleado=empleado, dia='JUEVES', hora_inicio='08:00:00.00', hora_fin='20:00:00.00')

        turno = Turno.get_or_create(empleado=empleado, dia='VIERNES', hora_inicio='08:00:00.00', hora_fin='20:00:00.00')

        turno = Turno.get_or_create(empleado=empleado, dia='SABADO', hora_inicio='08:00:00.00', hora_fin='20:00:00.00')

        turno = Turno.get_or_create(empleado=empleado, dia='DOMINGO', hora_inicio='08:00:00.00', hora_fin='20:00:00.00')





crear_categorias()
crear_lista_items()
crear_sucursal_base()
ventana_confirmacion = QMessageBox()
ventana_confirmacion.setText("Platos , categorias y sucursal base cargados correctamente")
ventana_confirmacion.setWindowTitle("Mensaje")
ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
sys.exit(ventana_confirmacion.exec_())