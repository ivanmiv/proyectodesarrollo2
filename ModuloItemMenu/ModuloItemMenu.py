
from PyQt5 import uic, QtWidgets
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QGraphicsPixmapItem
from PyQt5.QtWidgets import QGraphicsScene
from PyQt5.QtWidgets import QMessageBox
from peewee import IntegrityError, DoesNotExist

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter
from ModuloCategoria.modelo import Categoria
from ModuloItemMenu.modelo import ItemMenu


# from ModuloEmpleado.modelo import Empleado, Turno

class ModuloItemMenu(QtWidgets.QMainWindow):
    file = None

    def __init__(self, empleado, ui_padre):
        super(ModuloItemMenu, self).__init__()
        uic.loadUi('ModuloItemMenu/ModuloItemMenu.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.b_seleccionar_foto_crear.clicked.connect(self.on_click_b_seleccionar_fotografia_crear)
        self.b_seleccionar_foto_modificar.clicked.connect(self.on_click_b_seleccionar_fotografia_modificar)
        self.bAgregarPlato.clicked.connect(self.registrar_item_menu)
        self.b_obtener_informacion_consulta_item.clicked.connect(self.on_click_b_obtener_informacion_consulta_item)
        self.b_obtener_informacion_modificar_item.clicked.connect(self.on_click_b_obtener_informacion_modificar_item)
        self.b_modificar.clicked.connect(self.modificar_item_menu)
        self.b_eliminar_item.clicked.connect(self.eliminar_item_menu)



        self.cb_categoria_consulta_item.currentIndexChanged.connect(self.llenar_cb_plato_1)
        self.cb_categoria_seleccionar_modificar_item.currentIndexChanged.connect(self.llenar_cb_plato_2)
        self.cb_categoria_eliminar_item.currentIndexChanged.connect(self.llenar_cb_plato_3)

        self.ventana_confirmacion = QMessageBox()

        self.ruta_archivo = None
        self.ruta_archivo_modificar = None
        self.item_modificar = None

        if(self.usuario.cargo == 'MESERO'):
            self.tab_item_menu.removeTab(3)
            self.tab_item_menu.removeTab(2)
            self.tab_item_menu.removeTab(0)

        self.llenar_cb_categoria()
        self.llenar_cb_plato_1()
        self.llenar_cb_plato_2()
        self.llenar_cb_plato_3()

        self.show()


    def actualizar_texto_ventana_confirmacion(self, texto):
        self.ventana_confirmacion.setText(texto)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        # respuesta = ventana_confirmacion.exec_()

    def registrar_item_menu(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:

                nombre_categoria = self.cb_categoria_registro_item.currentText().strip()
                if len(nombre_categoria) == 0:
                    raise Exception("El nombre de la categoria no puede ser vacio")

                categoria = None
                try:
                    categoria = Categoria().buscar_categoria(nombre_categoria)
                except DoesNotExist as e:
                    raise Exception("No existe la categoría")

                nombre = self.lied_Nombre.text().strip()
                if len(nombre) == 0:
                    raise Exception("El nombre del item no puede ser vacio")

                try:
                    item = ItemMenu.buscar_item_menu(nombre)
                    if not (item == None):
                        raise  Exception("Ya existe un item con el nombre ingresado")
                except DoesNotExist as e:
                    pass

                precio = self.lied_precio.text()
                if len(precio) == 0:
                    raise Exception("El precio del item no puede ser vacio")

                try:
                    precio = float(precio)
                    if(precio<=0):
                        raise Exception()
                except Exception as e:
                    raise Exception("El precio debe ser un valor númerico positivo")

                descripcion = self.lied_Descripcion.toPlainText().strip()
                if len(descripcion) > 255:
                    raise Exception("El tamaño de la descripcion no puede exceder 255 caracteres")


                item = ItemMenu()
                item.nombre = nombre
                item.precio = precio
                item.descripcion = descripcion
                item.categoria = categoria
                item.estado = 'ACTIVO'

                lista = self.ruta_archivo.split('/')
                nombre_archivo = lista[len(lista)-1]
                extension_archivo = nombre_archivo.split('.')[1]

                extensiones_validas =['png','jpeg','jpg','bmp']

                if not (extension_archivo in extensiones_validas):
                    raise  Exception("Debe seleccionar un archivo en un formato aceptado para la foto (jpg, jpeg, png, bmp)")

                if (not self.ruta_archivo == '') and (not self.ruta_archivo is None):
                    item.set_fotografia(self.ruta_archivo)

                item.crear_item_menu()

                self.actualizar_texto_ventana_confirmacion("Item creado exitosamente")
                self.ventana_confirmacion.exec_()


            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def on_click_b_obtener_informacion_consulta_item(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                nombre_item_modificar = self.cb_plato_consulta_item.currentText()
                item = ItemMenu.get(ItemMenu.nombre == nombre_item_modificar,ItemMenu.estado == 'ACTIVO')

                self.lied_Nombre_2.setText(item.nombre)
                self.lied_precio_2.setText(str(item.precio))
                self.lied_Descripcion_2.setText(item.descripcion)
                self.cb_categoria_item_consultado.setCurrentText(item.categoria.nombre)


                item.get_fotografia()
                pixmap_imagen = QPixmap.fromImage(QImage('imgN.png'))
                escena = QGraphicsScene()
                self.gw_foto_item_consultar.setScene(escena)
                tamaño_escena = self.gw_foto_item_consultar.sceneRect()
                pixmap_imagen.scaledToWidth(tamaño_escena.width())
                imagen = QGraphicsPixmapItem(pixmap_imagen)

                escena.addItem(imagen)
                self.gw_foto_item_consultar.fitInView(imagen)
                imagen.setPos(0, 0)
                self.gw_foto_item_consultar.show()



            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()


    def on_click_b_obtener_informacion_modificar_item(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                nombre_item_modificar = self.cb_plato_seleccionar_modificar_item.currentText()
                self.item_modificar = ItemMenu.get(ItemMenu.nombre == nombre_item_modificar,ItemMenu.estado == 'ACTIVO')

                self.lied_Nombre_modificar_item.setText(self.item_modificar.nombre)
                self.lied_precio_modificar_item.setText(str(self.item_modificar.precio))
                self.lied_descripcion_modificar_item.setText(self.item_modificar.descripcion)
                self.cb_categoria_nueva_modificar_item.setCurrentText(self.item_modificar.categoria.nombre)
                self.item_modificar.get_fotografia()
                pixmap_imagen = QPixmap.fromImage(QImage('imgN.png'))
                escena = QGraphicsScene()
                self.gw_foto_item_modificar.setScene(escena)
                tamaño_escena = self.gw_foto_item_modificar.sceneRect()
                pixmap_imagen.scaledToWidth(tamaño_escena.width())
                imagen = QGraphicsPixmapItem(pixmap_imagen)

                escena.addItem(imagen)
                self.gw_foto_item_modificar.fitInView(imagen)
                imagen.setPos(0, 0)
                self.gw_foto_item_modificar.show()



            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()



    def modificar_item_menu(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:

                self.item_modificar.nombre = self.lied_Nombre_modificar_item.text()
                self.item_modificar.precio = self.lied_precio_modificar_item.text()
                self.item_modificar.descripcion = self.lied_descripcion_modificar_item.toPlainText()
                if(self.ruta_archivo_modificar != None):
                    self.item_modificar.set_fotografia(self.ruta_archivo_modificar)

                self.item_modificar.categoria = Categoria().buscar_categoria(self.cb_categoria_nueva_modificar_item.currentText())

                self.item_modificar.modificar_item_menu()
                self.actualizar_texto_ventana_confirmacion("Item modificado exitosamente")
                self.ventana_confirmacion.exec_()

            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def eliminar_item_menu(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                nombre_item_modificar = self.cb_plato_eliminar_item.currentText()
                item = ItemMenu.get(ItemMenu.nombre == nombre_item_modificar)

                item.eliminar_item_menu()
                self.actualizar_texto_ventana_confirmacion("Item eliminado exitosamente")
                self.ventana_confirmacion.exec_()

            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def on_click_b_seleccionar_fotografia_crear(self):
        opciones = QFileDialog.Options()
        opciones |= QFileDialog.DontUseNativeDialog
        self.ruta_archivo, _ = QFileDialog.getOpenFileName(self, "Seleccione una imagen", "",
                                                           "Imagenes (*.png *.jpeg *.bmp *.jpg)", options=opciones)
        if self.ruta_archivo:
            pixmap_imagen = QPixmap.fromImage(QImage(self.ruta_archivo))
            escena = QGraphicsScene()
            self.gw_foto_item_crear.setScene(escena)
            tamaño_escena = self.gw_foto_item_crear.sceneRect()
            pixmap_imagen.scaledToWidth(tamaño_escena.width())
            imagen = QGraphicsPixmapItem(pixmap_imagen)

            escena.addItem(imagen)
            self.gw_foto_item_crear.fitInView(imagen)
            imagen.setPos(0, 0)
            self.gw_foto_item_crear.show()


    def on_click_b_seleccionar_fotografia_modificar(self):
        opciones = QFileDialog.Options()
        opciones |= QFileDialog.DontUseNativeDialog
        self.ruta_archivo_modificar, _ = QFileDialog.getOpenFileName(self, "Seleccione una imagen", "",
                                                           "Imagenes (*.png *.jpeg *.bmp *.jpg)", options=opciones)
        if self.ruta_archivo_modificar:
            pixmap_imagen = QPixmap.fromImage(QImage(self.ruta_archivo_modificar))
            escena = QGraphicsScene()
            self.gw_foto_item_modificar.setScene(escena)
            tamaño_escena = self.gw_foto_item_modificar.sceneRect()
            pixmap_imagen.scaledToWidth(tamaño_escena.width())
            imagen = QGraphicsPixmapItem(pixmap_imagen)

            escena.addItem(imagen)
            self.gw_foto_item_modificar.fitInView(imagen)
            imagen.setPos(0, 0)
            self.gw_foto_item_modificar.show()

    def llenar_cb_categoria(self):
        self.cb_categoria_registro_item.clear()
        self.cb_categoria_consulta_item.clear()
        self.cb_categoria_item_consultado.clear()
        self.cb_categoria_seleccionar_modificar_item.clear()
        self.cb_categoria_nueva_modificar_item.clear()
        self.cb_categoria_eliminar_item.clear()

        for categoria in Categoria.select().where(Categoria.estado == 'ACTIVO'):
            self.cb_categoria_registro_item.addItem(categoria.nombre)
            self.cb_categoria_consulta_item.addItem(categoria.nombre)
            self.cb_categoria_item_consultado.addItem(categoria.nombre)
            self.cb_categoria_seleccionar_modificar_item.addItem(categoria.nombre)
            self.cb_categoria_nueva_modificar_item.addItem(categoria.nombre)
            self.cb_categoria_eliminar_item.addItem(categoria.nombre)

    def llenar_cb_plato_1(self):

        self.cb_plato_consulta_item.clear()
        for plato in ItemMenu.select().where(
                        ItemMenu.categoria == Categoria.get(Categoria.nombre == self.cb_categoria_consulta_item.currentText())
                ,ItemMenu.estado =='ACTIVO'):
            self.cb_plato_consulta_item.addItem(plato.nombre)

    def llenar_cb_plato_2(self):
        self.cb_plato_seleccionar_modificar_item.clear()

        for plato in ItemMenu.select().where(
                        ItemMenu.categoria == Categoria.get(Categoria.nombre == self.cb_categoria_seleccionar_modificar_item.currentText())
                ,ItemMenu.estado =='ACTIVO'):
            self.cb_plato_seleccionar_modificar_item.addItem(plato.nombre)

    def llenar_cb_plato_3(self):
        self.cb_plato_eliminar_item.clear()

        for plato in ItemMenu.select().where(
                        ItemMenu.categoria == Categoria.get(Categoria.nombre == self.cb_categoria_eliminar_item.currentText())
                ,ItemMenu.estado =='ACTIVO'):
            self.cb_plato_eliminar_item.addItem(plato.nombre)

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()
