from ModuloCategoria.modelo import *


def fotografia_por_defecto():
    with open('Imagenes/item_por_defecto.png', "rb+") as imageFile:
        return bytearray(imageFile.read())


class ItemMenu(BaseModel):
    nombre = CharField()
    fotografia = BlobField(null=True, default=fotografia_por_defecto())
    precio = FloatField()
    descripcion = CharField()
    categoria = ForeignKeyField(Categoria)

    def crear_item_menu(self):
        self.save()
        print("Plato creado")

    def modificar_item_menu(self):
        self.save()
        print("plato modificado")

    def eliminar_item_menu(self):
        self.update(estado='INACTIVO').where(ItemMenu.id == self.id).execute()
        print("plato eliminado")

    def buscar_item_menu(nombre_item):
        print(ItemMenu.get(nombre=nombre_item))
        return ItemMenu.get(nombre = nombre_item)

    def get_fotografia(self):
        import io
        from PIL import Image
        image = Image.open(io.BytesIO(self.fotografia))
        image.save('imgN.png')

    def set_fotografia(self, ruta_imagen):
        with open(ruta_imagen, "rb+") as imageFile:
            self.fotografia = bytearray(imageFile.read())
