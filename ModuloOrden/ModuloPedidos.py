from abc import ABCMeta, abstractmethod
from datetime import datetime

from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QTableWidgetItem

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter
from ModuloCategoria.modelo import Categoria
from ModuloEmpleado.modelo import Empleado
from ModuloItemMenu.modelo import ItemMenu
from ModuloMesa.modelo import Mesa
from ModuloOrden.modelo import Orden, ItemOrden


######################################
# IMPLEMENTACION DEL PATRON OBSERVER #
######################################


class Observer(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def update(self, *args, **kwargs):
        pass

    @abstractmethod
    def update(self):
        pass


# Clase observable
class Observable(object):
    def __init__(self):
        self.observers = []
        pass

    def register(self, observer):
        if not observer in self.observers:
            self.observers.append(observer)

    def unregister(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)

    def unregister_all(self):
        if self.observers:
            del self.observers[:]

    def update_observers(self, args):
        for observer in self.observers:
            observer.update(args)


# OBSERVADORES
class PedidosMesa(Observer):
    def update(self, args):
        if args.rbMesa.isChecked():
            row_position = args.tMesasMesero.model().rowCount()
            # args.tMesasMesero.model().insertRow(row_position)
            # args.tMesasMesero.model().setItem(row_position, 0, QtGui.QStandardItem("text1"))
            # args.tMesasMesero.model().setItem(row_position, 1, QtGui.QStandardItem("text2"))
            # args.tMesasMesero.model().setItem(row_position, 2, QtGui.QStandardItem("text3"))
            # args.tMesasMesero.model().setItem(row_position, 3, QtGui.QStandardItem("text4"))
            print("Se actualiza la tabla de pedidos en mesa")


# OBSERVADORES
class PedidosLlevar(Observer):
    def update(self, args):
        if args.rbParaLlevar.isChecked():
            row_position = args.tPedidosMesero.model().rowCount()
            # args.tPedidosMesero.model().insertRow(row_position)
            # args.tPedidosMesero.model().setItem(row_position, 0, QtGui.QStandardItem("text5"))
            # args.tPedidosMesero.model().setItem(row_position, 1, QtGui.QStandardItem("text6"))
            # args.tPedidosMesero.model().setItem(row_position, 2, QtGui.QStandardItem("text7"))
            # args.tPedidosMesero.model().setItem(row_position, 3, QtGui.QStandardItem("text8"))
            print("Se actualiza la tabla de pedidos para llevar")


class ModuloPedidos(QtWidgets.QMainWindow):
    observable = Observable()
    items_en_orden_mesa = 0
    items_atendidos_mesa = 0
    items_en_orden_llevar = 0
    items_atendidos_llevar = 0
    numero_orden = 0
    numero_orden_modificar = 0

    def __init__(self, empleado, ui_padre):
        super(ModuloPedidos, self).__init__()
        uic.loadUi('ModuloOrden/ModuloPedidos.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.tMesasMesero.horizontalHeader().setStretchLastSection(True)
        self.tMesasMesero.setHorizontalHeaderLabels(
            ["Mesa", "Inicio del pedido", "Elementos pedidos", "Elementos entregados"])
        self.tMesasMesero.setColumnCount(4)
        self.tMesasMesero.setColumnWidth(0, 40)
        self.tMesasMesero.setColumnWidth(1, 100)
        self.tMesasMesero.setColumnWidth(2, 120)
        self.tMesasMesero.setColumnWidth(3, 130)

        self.tPedidosMesero.horizontalHeader().setStretchLastSection(True)
        self.tPedidosMesero.setHorizontalHeaderLabels(
            ["Mesa", "Inicio del pedido", "Elementos pedidos", "Elementos entregados"])
        self.tPedidosMesero.setColumnCount(4)
        self.tPedidosMesero.setColumnWidth(0, 40)
        self.tPedidosMesero.setColumnWidth(1, 100)
        self.tPedidosMesero.setColumnWidth(2, 120)
        self.tPedidosMesero.setColumnWidth(3, 130)

        # Creación y Registro de observadores
        pedidomesa = PedidosMesa()
        pedidollevar = PedidosLlevar()

        self.observable.register(pedidomesa)
        self.observable.register(pedidollevar)
        self.bRegistrarPedido.clicked.connect(self.on_b_registrar_pedido_clicked)
        self.bRegistrarPedido.clicked.connect(self.notificar)
        self.bAgregar.clicked.connect(self.on_b_agregar_clicked)
        self.bQuitar.clicked.connect(self.on_b_quitar_clicked)
        # self.bAgregar.clicked.connect(self.agregarItem)
        self.bActualizarMesa.clicked.connect(self.actualizar_t_mesas_mesero)
        self.bActualizarLlevar.clicked.connect(self.actualizar_t_pedidos_mesero)

        self.bConsultarMesa.clicked.connect(self.mostrar_items_mesa)
        self.bConsultar.clicked.connect(self.mostrar_items_llevar)

        self.bRegistrarEntregaMesa.clicked.connect(self.registrar_entrega_mesa)
        self.bRegistrarEntrega.clicked.connect(self.registrar_entrega_llevar)

        self.b_consultar_pedido_modificar.clicked.connect(self.on_b_consultar_pedido_modificar_clicked)
        self.bAgregar_Modificar.clicked.connect(self.on_b_agregar_modificar_clicked)
        self.bQuitar_Modificar.clicked.connect(self.on_b_quitar_modificar_clicked)
        self.bModificarPedido.clicked.connect(self.on_b_modificar_pedido_clicked)

        self.b_obtener_informacion__historico_pedido.clicked.connect(self.on_b_obtener_informacion_historico_clicked)
        self.b_eliminar_pedido.clicked.connect(self.on_b_eliminar_pedido_clicked)

        self.rbMesa.clicked.connect(self.habilitar_cb_mesas)
        self.rbMesa_Modificar.clicked.connect(self.habilitar_cb_mesas_modificar)
        self.rbParaLlevar.clicked.connect(self.deshabilitar_cb_mesas)

        self.actualizar_t_pedidos_mesero()
        self.actualizar_t_mesas_mesero()

        self.iniciar_tablas()

        self.llenar_cb_categoria()
        self.llenar_cb_mesa()

        self.le_cedula.setText(str(self.usuario.numero_documento_identificacion))

        self.cbCategorias.currentIndexChanged.connect(self.on_cb_categorias_index_changed)
        self.cbCategorias_Modificar.currentIndexChanged.connect(self.on_cb_categorias_modificar_index_changed)

        self.show()

        self.ventana_confirmacion = QMessageBox()

    def habilitar_cb_mesas(self):
        self.labelMesa.setEnabled(True)
        self.cbMesasDisponibles.setEnabled(True)

    def habilitar_cb_mesas_modificar(self):
        self.labelMesa_2.setEnabled(True)
        self.cbMesasDisponibles_Modificar.setEnabled(True)

    def deshabilitar_cb_mesas(self):
        self.labelMesa.setEnabled(False)
        self.cbMesasDisponibles.setEnabled(False)

    # Llamado a notificar a los observadores
    def notificar(self, a):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                self.observable.update_observers(self)
                itemOrden = ItemOrden()
                orden = Orden()
                orden.estado = 'ACTIVO'
                if (self.rbMesa.isChecked() and (not self.rbParaLlevar.isChecked())):
                    orden.tipo = 'MESA'
                elif (self.rbParaLlevar.isChecked() and (not self.rbMesa.isChecked())):
                    orden.tipo = 'LLEVAR'
                else:
                    print("Error ingresando el tipo de pedido")
                orden.hora_inicio = datetime.now()
                orden.atentido_por = Empleado.get(Empleado.numero_documento_identificacion == self.le_cedula.text())

                self.actualizar_t_mesas_mesero()
                self.actualizar_t_pedidos_mesero()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error :" + str(e))
                self.ventana_confirmacion.exec_()

    def on_cb_categorias_index_changed(self, indice_nuevo):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                if self.cbCategorias.currentText() == "Seleccione":
                    return

                categoria = Categoria.get(Categoria.nombre == self.cbCategorias.currentText())
                items_categoria = ItemMenu.select().where(ItemMenu.categoria == categoria)

                self.t_items_crear.clearContents()
                self.t_items_crear.setRowCount(0)
                for item in items_categoria:
                    rowPosition = self.t_items_crear.rowCount()
                    self.t_items_crear.insertRow(rowPosition)
                    self.t_items_crear.setItem(rowPosition, 0, QTableWidgetItem(str(item.id)))
                    self.t_items_crear.setItem(rowPosition, 1, QTableWidgetItem(item.nombre))
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error :" + str(e))
                self.ventana_confirmacion.exec_()

    def on_cb_categorias_modificar_index_changed(self, indice_nuevo):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                if self.cbCategorias_Modificar.currentText() == "Seleccione":
                    return

                categoria = Categoria.get(Categoria.nombre == self.cbCategorias_Modificar.currentText())
                items_categoria = ItemMenu.select().where(ItemMenu.categoria == categoria)

                self.t_items_Modificar.clearContents()
                self.t_items_Modificar.setRowCount(0)
                for item in items_categoria:
                    rowPosition = self.t_items_Modificar.rowCount()
                    self.t_items_Modificar.insertRow(rowPosition)
                    self.t_items_Modificar.setItem(rowPosition, 0, QTableWidgetItem(str(item.id)))
                    self.t_items_Modificar.setItem(rowPosition, 1, QTableWidgetItem(item.nombre))
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error :" + str(e))
                self.ventana_confirmacion.exec_()

    def actualizar_t_mesas_mesero(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                self.tMesasMesero.setRowCount(0)
                self.tMesasMesero.setRowCount(len(Orden.select().where(Orden.tipo == 'MESA', Orden.estado == 'ACTIVO', Orden.hora_entrega_ultimo_item == None)))
                #print(len(Orden.select().where(Orden.tipo == 'MESA', Orden.estado == 'ACTIVO', Orden.hora_entrega_ultimo_item == None)))
                self.tMesasMesero.setColumnCount(4)
                self.tMesasMesero.setHorizontalHeaderLabels(
                    ['id', 'mesero', 'Hora inicio', 'mesero id'])

                i = 0
                for orden in Orden.select().where(Orden.tipo == 'MESA', Orden.estado == 'ACTIVO', Orden.hora_entrega_ultimo_item == None):
                    orden_actual = Orden.get(Orden.id == orden.id)
                    mesero = Empleado.get(
                        Empleado.numero_documento_identificacion == orden.atentido_por.numero_documento_identificacion)
                    hora_inicio = orden.hora_inicio


                    self.tMesasMesero.setItem(i, 0, QTableWidgetItem(str(orden_actual.id)))
                    self.tMesasMesero.setItem(i, 1, QTableWidgetItem(str(mesero)))
                    self.tMesasMesero.setItem(i, 2, QTableWidgetItem(str(hora_inicio)))
                    self.tMesasMesero.setItem(i, 3, QTableWidgetItem(str(mesero.numero_documento_identificacion)))
                    i += 1
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error :" + str(e))
                self.ventana_confirmacion.exec_()

    def actualizar_t_pedidos_mesero(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                self.tPedidosMesero.setRowCount(0)
                self.tPedidosMesero.setRowCount(
                    len(Orden.select().where(Orden.tipo == 'LLEVAR', Orden.estado == 'ACTIVO', Orden.hora_entrega_ultimo_item == None)))
                self.tPedidosMesero.setColumnCount(4)
                self.tPedidosMesero.setHorizontalHeaderLabels(
                    ['id', 'mesero', 'Hora inicio', 'mesero id'])

                i = 0
                for orden in Orden.select().where(Orden.tipo == 'LLEVAR', Orden.estado == 'ACTIVO', Orden.hora_entrega_ultimo_item == None):
                    orden_actual = Orden.get(Orden.id == orden.id)
                    mesero = Empleado.get(
                        Empleado.numero_documento_identificacion == orden.atentido_por.numero_documento_identificacion)
                    hora_inicio = orden.hora_inicio
                    #print("--" + str(orden.hora_entrega_ultimo_item))
                    self.tPedidosMesero.setItem(i, 0, QTableWidgetItem(str(orden_actual.id)))
                    self.tPedidosMesero.setItem(i, 1, QTableWidgetItem(str(mesero)))
                    self.tPedidosMesero.setItem(i, 2, QTableWidgetItem(str(hora_inicio)))
                    self.tPedidosMesero.setItem(i, 3, QTableWidgetItem(str(mesero.numero_documento_identificacion)))
                    i += 1
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error :" + str(e))
                self.ventana_confirmacion.exec_()

    def mostrar_items_mesa(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                self.t_items_mesa.setRowCount(0)
                rowPosition = self.t_items_mesa.rowCount()
                self.t_items_mesa.insertRow(rowPosition)
                self.numero_orden = self.tMesasMesero.selectedItems()[0].text()
                print(self.numero_orden)
                orden = Orden.get(Orden.id == self.numero_orden)
                self.items_orden = ItemOrden.select().where(ItemOrden.orden == orden, ItemOrden.estado == 'ACTIVO')

                self.empleado_consultar_mesas.setText(self.tMesasMesero.selectedItems()[3].text())

                self.t_items_mesa.setRowCount(len(self.items_orden))
                self.t_items_mesa.setColumnCount(4)
                self.t_items_mesa.setHorizontalHeaderLabels(
                    ['Nombre plato', 'Cantidad', 'Valor unidad', 'valor total'])
                print(self.tMesasMesero.selectedItems()[3].text())
                i = 0
                for item in self.items_orden:
                    item_actual = ItemMenu.get(ItemMenu.id == item.plato)
                    cantidad = item.cantidad_plato
                    precio = item_actual.precio
                    precio_total_items = cantidad * precio

                    self.t_items_mesa.setItem(i, 0, QTableWidgetItem(item_actual.nombre))
                    self.t_items_mesa.setItem(i, 1, QTableWidgetItem(str(cantidad)))
                    self.t_items_mesa.setItem(i, 2, QTableWidgetItem(str(precio)))
                    self.t_items_mesa.setItem(i, 3, QTableWidgetItem(str(precio_total_items)))
                    i += 1

                self.items_en_orden_mesa = self.t_items_mesa.rowCount()
                self.items_atendidos_mesa = 0
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error : Debe seleccionar una orden " + str(e))
                self.ventana_confirmacion.exec_()

    def mostrar_items_llevar(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                self.t_items_llevar.setRowCount(0)
                rowPosition = self.t_items_mesa.rowCount()
                self.t_items_llevar.insertRow(rowPosition)
                self.numero_orden = self.tPedidosMesero.selectedItems()[0].text()

                orden = Orden.get(Orden.id == self.numero_orden)
                self.items_orden = ItemOrden.select().where(ItemOrden.orden == orden, ItemOrden.estado == 'ACTIVO')

                self.empleado_consultar_llevar.setText(self.tPedidosMesero.selectedItems()[3].text())

                self.t_items_llevar.setRowCount(len(self.items_orden))
                self.t_items_llevar.setColumnCount(4)
                self.t_items_llevar.setHorizontalHeaderLabels(
                    ['Nombre plato', 'Cantidad', 'Valor unidad', 'valor total'])

                i = 0
                for item in self.items_orden:
                    item_actual = ItemMenu.get(ItemMenu.id == item.plato)
                    cantidad = item.cantidad_plato
                    precio = item_actual.precio
                    precio_total_items = cantidad * precio

                    self.t_items_llevar.setItem(i, 0, QTableWidgetItem(item_actual.nombre))
                    self.t_items_llevar.setItem(i, 1, QTableWidgetItem(str(cantidad)))
                    self.t_items_llevar.setItem(i, 2, QTableWidgetItem(str(precio)))
                    self.t_items_llevar.setItem(i, 3, QTableWidgetItem(str(precio_total_items)))
                    i += 1

                self.items_en_orden_llevar = self.t_items_llevar.rowCount()
                self.items_atendidos_llevar = 0
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error : Debe seleccionar una orden " + str(e))
                self.ventana_confirmacion.exec_()

    def registrar_entrega_mesa(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                if (self.usuario == Empleado.get(
                            Empleado.numero_documento_identificacion == self.empleado_consultar_mesas.text())):
                    if (self.t_items_mesa.rowCount() > 0):
                        nombre_item = self.t_items_mesa.selectedItems()[0].text()

                        id_item = ItemMenu.buscar_item_menu(nombre_item).id

                        ItemOrden.buscar_item_orden(self.numero_orden, id_item).entregar_item_orden()

                        self.items_atendidos_mesa = self.items_atendidos_mesa + 1

                        self.t_items_mesa.removeRow(self.t_items_mesa.currentRow())

                        if (self.items_atendidos_mesa == self.items_en_orden_mesa):
                            orden = Orden().buscar_orden(self.numero_orden)
                            orden.finalizar_orden()
                            self.actualizar_t_mesas_mesero()
                    else:
                        pass
                        # self.actualizar_t_mesas_mesero()
                else:
                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText("No estas autorizado para modificar esta orden")
                    ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error registrando entrega en mesa: " + str(e))
                self.ventana_confirmacion.exec_()

    def registrar_entrega_llevar(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                if (self.usuario == Empleado.get(
                            Empleado.numero_documento_identificacion == self.empleado_consultar_llevar.text())):
                    if (self.t_items_llevar.rowCount() > 0):
                        nombre_item = self.t_items_llevar.selectedItems()[0].text()

                        id_item = ItemMenu.buscar_item_menu(nombre_item).id

                        ItemOrden.buscar_item_orden(self.numero_orden, id_item).entregar_item_orden()

                        self.items_atendidos_llevar = self.items_atendidos_llevar + 1

                        self.t_items_llevar.removeRow(self.t_items_llevar.currentRow())

                        if (self.items_atendidos_llevar == self.items_en_orden_llevar):
                            orden = Orden().buscar_orden(self.numero_orden)
                            orden.finalizar_orden()
                            self.actualizar_t_pedidos_mesero()
                    else:
                        pass
                        # self.actualizar_t_pedidos_mesero()
                else:
                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText("No estas autorizado para modificar esta orden")
                    ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error registrando entrega para llevar: " + str(e))
                self.ventana_confirmacion.exec_()

    def on_b_consultar_pedido_modificar_clicked(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                orden = Orden()
                self.numero_orden_modificar = self.le_numero_pedido_modificar.text()
                orden = Orden.get(Orden.id == self.numero_orden_modificar)
                self.items_orden = ItemOrden.select().where(ItemOrden.orden == orden)

                self.t_items_Modificar_2.setRowCount(0)
                rowPosition = self.t_items_mesa.rowCount()
                self.t_items_Modificar_2.insertRow(rowPosition)

                self.items_orden = ItemOrden.select().where(ItemOrden.orden == orden, ItemOrden.estado == 'ACTIVO')

                self.t_items_Modificar_2.setRowCount(len(self.items_orden))
                self.t_items_Modificar_2.setColumnCount(5)
                self.t_items_Modificar_2.setHorizontalHeaderLabels(
                    ['id plato', 'Nombre plato', 'Cantidad', 'Valor unidad', 'valor total'])

                i = 0
                for item in self.items_orden:
                    item_actual = ItemMenu.get(ItemMenu.id == item.plato)
                    cantidad = item.cantidad_plato
                    precio = item_actual.precio
                    precio_total_items = cantidad * precio

                    self.t_items_Modificar_2.setItem(i, 0, QTableWidgetItem(str(item_actual.id)))
                    self.t_items_Modificar_2.setItem(i, 1, QTableWidgetItem(item_actual.nombre))
                    self.t_items_Modificar_2.setItem(i, 2, QTableWidgetItem(str(cantidad)))
                    self.t_items_Modificar_2.setItem(i, 3, QTableWidgetItem(str(precio)))
                    self.t_items_Modificar_2.setItem(i, 4, QTableWidgetItem(str(precio_total_items)))
                    i += 1

                if (orden.tipo == 'LLEVAR'):
                    self.rbMesa_Modificar.setChecked(False)
                    self.rbParaLlevar_Modificar.setChecked(True)
                else:
                    self.rbMesa_Modificar.setChecked(True)
                    self.rbParaLlevar_Modificar.setChecked(False)

                empleado = Empleado.get(
                    Empleado.numero_documento_identificacion == orden.atentido_por.numero_documento_identificacion)
                self.le_cedula_Modificar.setText(str(empleado.numero_documento_identificacion))
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error : " + str(e))
                self.ventana_confirmacion.exec_()

    def on_b_agregar_modificar_clicked(self):
        try:
            cantidad_agregar = int(self.le_cantidad_Modificar.text())
            rowPosition = self.t_items_Modificar_2.rowCount()
            id_item_agregar = self.t_items_Modificar.selectedItems()[0].text()
            nombre_item_agregar = self.t_items_Modificar.selectedItems()[1].text()
            repetido = False

            for i in range(self.t_items_Modificar_2.rowCount()):
                id_item = self.t_items_Modificar_2.item(i, 0).text()
                cantidad = int(self.t_items_Modificar_2.item(i, 2).text())
                if(id_item == id_item_agregar):
                    cantidad_agregar = cantidad_agregar + cantidad
                    self.t_items_Modificar_2.setItem(i, 2, QTableWidgetItem(str(cantidad_agregar)))
                    repetido = True
                    break

            if(not repetido):
                self.t_items_Modificar_2.insertRow(rowPosition)
                self.t_items_Modificar_2.setItem(rowPosition, 0,
                                                 QTableWidgetItem(id_item_agregar))
                self.t_items_Modificar_2.setItem(rowPosition, 1,
                                                 QTableWidgetItem(nombre_item_agregar))
                self.t_items_Modificar_2.setItem(rowPosition, 2, QTableWidgetItem(str(cantidad_agregar)))

        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()

    def on_b_quitar_modificar_clicked(self):
        try:
            # itemOrden = ItemOrden.buscar_item_orden(self.numero_orden_modificar,self.t_items_Modificar_2.selectedItems()[0].text())
            # itemOrden.eliminar_item_orden()
            self.t_items_Modificar_2.removeRow(self.t_items_Modificar_2.currentRow())
        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()

    def on_b_modificar_pedido_clicked(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                if (self.usuario == Empleado.get(
                            Empleado.numero_documento_identificacion == self.le_cedula_Modificar.text())):
                    orden = Orden().buscar_orden(self.numero_orden_modificar)
                    if not self.rbParaLlevar_Modificar.isChecked():
                        orden.mesa = Mesa.get(Mesa.id == int(self.cbMesasDisponibles_Modificar.currentText()))
                        orden.tipo = 'MESA'
                    else:
                        orden.tipo = 'LLEVAR'

                    orden.atentido_por = Empleado.get(
                        Empleado.numero_documento_identificacion == self.le_cedula_Modificar.text())
                    orden.crear_orden()
                    #####
                    ItemOrden.delete().where(ItemOrden.orden_id == orden, ItemOrden.estado == 'ACTIVO').execute()

                    ######
                    for i in range(self.t_items_Modificar_2.rowCount()):
                        id_item = self.t_items_Modificar_2.item(i, 0).text()
                        cantidad = self.t_items_Modificar_2.item(i, 2).text()
                        ItemOrden.get_or_create(orden=orden, plato=ItemMenu.get(ItemMenu.id == id_item), cantidad_plato=cantidad, )
                        #print("creado")


                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText("Orden modificada correctamente")
                    ventana_confirmacion.exec_()

                    self.actualizar_t_mesas_mesero()
                    self.actualizar_t_pedidos_mesero()
                    self.t_items_Modificar_2.setRowCount(0)
                else:
                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText("No estas autorizado para modificar esta orden")
                    ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error modificando orden: " + str(e))
                self.ventana_confirmacion.exec_()

    def on_b_agregar_clicked(self):
        try:
            cantidad_agregar = int(self.le_cantidad.text())
            rowPosition = self.t_items_pedido.rowCount()
            id_item_agregar = self.t_items_crear.selectedItems()[0].text()
            nombre_item_agregar = self.t_items_crear.selectedItems()[1].text()
            repetido = False

            for i in range(self.t_items_pedido.rowCount()):
                id_item = self.t_items_pedido.item(i, 0).text()
                cantidad = int(self.t_items_pedido.item(i, 2).text())
                if(id_item == id_item_agregar):
                    cantidad_agregar = cantidad_agregar + cantidad
                    self.t_items_pedido.setItem(i, 2, QTableWidgetItem(str(cantidad_agregar)))
                    repetido = True
                    break

            if(not repetido):
                self.t_items_pedido.insertRow(rowPosition)
                self.t_items_pedido.setItem(rowPosition, 0,
                                                 QTableWidgetItem(id_item_agregar))
                self.t_items_pedido.setItem(rowPosition, 1,
                                                 QTableWidgetItem(nombre_item_agregar))
                self.t_items_pedido.setItem(rowPosition, 2, QTableWidgetItem(str(cantidad_agregar)))

        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()


    def on_b_quitar_clicked(self):
        try:
            self.t_items_pedido.removeRow(self.t_items_pedido.currentRow())

        except Exception as e:
            self.actualizar_texto_ventana_confirmacion(str(e))
            self.ventana_confirmacion.exec_()

    # def agregarItem(self, id_orden, nombre_item, cantidad):
    #     itemOrden = ItemOrden()
    #
    #     itemOrden.orden = Orden.buscar_orden(id_orden)
    #     itemOrden.plato = ItemMenu.get(nombre_item)
    #     itemOrden.cantidad_plato = cantidad
    #
    #     categoria = Categoria.get(Categoria.nombre == self.cbCategorias.currentText())
    #     items_categoria = ItemMenu.select().where(ItemMenu.categoria == categoria)
    #
    #     self.t_items_crear.clearContents()
    #     self.t_items_crear.setRowCount(0)
    #     for item in items_categoria:
    #         rowPosition = self.t_items_crear.rowCount()
    #         self.t_items_crear.insertRow(rowPosition)
    #         self.t_items_crear.setItem(rowPosition, 0, QTableWidgetItem(str(item.id)))
    #         self.t_items_crear.setItem(rowPosition, 1, QTableWidgetItem(item.nombre))

    def llenar_cb_categoria(self):
        self.cbCategorias.clear()
        self.cbCategorias_Modificar.clear()
        self.cbCategorias.addItem("Seleccione")
        self.cbCategorias_Modificar.addItem("Seleccione")
        for categoria in Categoria.select().where(Categoria.estado == 'ACTIVO'):
            self.cbCategorias.addItem(categoria.nombre)
            self.cbCategorias_Modificar.addItem(categoria.nombre)

    def llenar_cb_mesa(self):
        self.cbMesasDisponibles.clear()
        self.cbMesasDisponibles_Modificar.clear()
        for mesa in Mesa.select().where(Mesa.estado == 'ACTIVO', Mesa.ocupado == False):
            self.cbMesasDisponibles.addItem(str(mesa.id))
            self.cbMesasDisponibles_Modificar.addItem(str(mesa.id))

    def iniciar_tablas(self):
        self.t_items_crear.setColumnCount(2)
        self.t_items_crear.setHorizontalHeaderLabels(['Id', 'Nombre plato'])
        self.t_items_pedido.setColumnCount(3)
        self.t_items_pedido.setHorizontalHeaderLabels(['Id', 'Nombre plato', 'Cantidad'])
        self.t_items_Modificar.setColumnCount(2)
        self.t_items_Modificar.setHorizontalHeaderLabels(['Id', 'Nombre plato'])
        self.t_items_Modificar_2.setColumnCount(3)
        self.t_items_Modificar_2.setHorizontalHeaderLabels(['Id', 'Nombre plato', 'Cantidad'])


    def on_b_registrar_pedido_clicked(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                orden = Orden()
                if not self.rbParaLlevar.isChecked():
                    orden.mesa = Mesa.get(Mesa.id == int(self.cbMesasDisponibles.currentText()))
                    orden.tipo = 'MESA'
                else:
                    orden.tipo = 'LLEVAR'

                orden.atentido_por = self.usuario
                orden.crear_orden()

                for i in range(self.t_items_pedido.rowCount()):
                    id_item = self.t_items_pedido.item(i, 0).text()
                    cantidad = self.t_items_pedido.item(i, 2).text()
                    ItemOrden.get_or_create(orden=orden, plato=ItemMenu.get(ItemMenu.id == id_item),
                                            cantidad_plato=cantidad, )
                    print("creado")

                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText("Orden registrada correctamente")
                ventana_confirmacion.exec_()

                self.actualizar_t_mesas_mesero()
                self.actualizar_t_pedidos_mesero()
                self.t_items_pedido.setRowCount(0)
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error registrando orden: " + str(e))
                self.ventana_confirmacion.exec_()

    def on_b_obtener_informacion_historico_clicked(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                numero_orden = self.le_num_pedido.text()
                orden = Orden.get(Orden.id == numero_orden)
                self.items_orden = ItemOrden.select().where(ItemOrden.orden == orden)

                items_text = "\n"

                for item in self.items_orden:
                    item_actual = ItemMenu.get(ItemMenu.id == item.plato)
                    nombre_item = item_actual.nombre
                    items_text += nombre_item + " \n"

                tex_tipo_orden = "TIPO DE ORDEN: " + orden.tipo
                mesa_actual = Mesa().get(Mesa.id == orden.mesa)
                tex_numero_mesa = "NUMERO MESA: " + str(mesa_actual.id)
                tex_hora_inicio = "HORA DE INICIO: " + str(orden.hora_inicio)
                tex_hora_ultimo = "HORA ULTIMO: " + str(orden.hora_entrega_ultimo_item)
                tex_mesero = "ATENDIDO POR: " + str(orden.atentido_por)
                tex_platos = "PLATOS INCLUIDOS: " + items_text

                self.pte_detalle_pedido.clear()
                self.pte_detalle_pedido.appendPlainText(tex_tipo_orden)
                self.pte_detalle_pedido.appendPlainText(tex_numero_mesa)
                self.pte_detalle_pedido.appendPlainText(tex_hora_inicio)
                self.pte_detalle_pedido.appendPlainText(tex_hora_ultimo)
                self.pte_detalle_pedido.appendPlainText(tex_mesero)
                self.pte_detalle_pedido.appendPlainText(tex_platos)
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error obteniendo informacion: " + str(e))
                self.ventana_confirmacion.exec_()

    def on_b_eliminar_pedido_clicked(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                if (self.usuario == Empleado.get(
                            Empleado.numero_documento_identificacion == self.le_num_pedido.text())):
                    numero_orden = self.le_num_pedido.text()
                    orden = Orden.get(Orden.id == numero_orden)
                    orden.eliminar_orden()
                else:
                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText("No estas autorizado para eliminar esta orden")
                    ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str(e))
                self.ventana_confirmacion.exec_()

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()

    def actualizar_texto_ventana_confirmacion(self, texto):
        self.ventana_confirmacion.setText(texto)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        # respuesta = ventana_confirmacion.exec_()
