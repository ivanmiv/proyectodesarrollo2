from datetime import datetime

from peewee import *

from ConfiguracionORM.BaseModel import BaseModel
from ModuloEmpleado.modelo import Empleado
from ModuloItemMenu.modelo import ItemMenu
from ModuloMesa.modelo import Mesa


class Orden(BaseModel):
    TIPO_CHOICES = (('MESA', 'En mesa'), ('LLEVAR', 'Para llevar'))

    tipo = CharField(max_length=20, constraints=[Check('tipo = \'MESA\' or tipo = \'LLEVAR\'')])
    hora_inicio = DateTimeField(default=datetime.now())
    hora_fin = DateTimeField(null=True)
    hora_entrega_ultimo_item = DateTimeField(null=True)
    mesa = ForeignKeyField(Mesa, null=True)
    atentido_por = ForeignKeyField(Empleado)

    def crear_orden(self):
        self.save()
        print("orden creada")

    def modificar_orden(self):
        self.save()
        print("orden modificada")

    def eliminar_orden(self):
        self.update(estado='INACTIVO').where(Orden.id == self.id)
        print("orden eliminada")

    def buscar_orden(self, numero_orden):
        return Orden.get(Orden.id == numero_orden,Orden.estado == 'ACTIVO')

    def finalizar_orden(self):
        self.hora_entrega_ultimo_item = datetime.now()
        #self.estado = 'INACTIVO'
        self.save()
        print("Ultimo item entregado")


class ItemOrden(BaseModel):
    orden = ForeignKeyField(Orden)
    plato = ForeignKeyField(ItemMenu)
    cantidad_plato = IntegerField()
    hora_entrega_plato = DateTimeField(null=True)
    fecha_orden = DateTimeField(default=datetime.now())

    class Meta:
        primary_key = CompositeKey('orden', 'plato', 'fecha_orden')

    def crear_item_orden(self):
        self.save()
        print("item orden creada")

    def modificar_item_orden(self):
        self.save()
        print("item orden modificada")

    def entregar_item_orden(self):
        self.hora_entrega_plato = datetime.now()
        self.estado = 'INACTIVO'
        self.save()
        print("item de la orden entregado")

    def eliminar_item_orden(self):
        self.estado = 'INACTIVO'
        self.save()
        print("orden eliminada")

    def buscar_item_orden(numero_orden, id_plato):
        return ItemOrden.get(orden=numero_orden, plato=id_plato)
