
from abc import ABCMeta, abstractmethod

from PyQt5 import uic, QtWidgets
from PyQt5.QtWidgets import QMessageBox

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter
from ModuloCategoria.modelo import Categoria


# Interfaz a implementar
class Iterator:
    __metaclass__ = ABCMeta

    @abstractmethod
    def has_next(self):
        raise Exception("Metodo no implementado")

    @abstractmethod
    def next(self):
        raise Exception("Metodo no implementado")

    @abstractmethod
    def current(self):
        raise Exception("Metodo no implementado")

    @abstractmethod
    def first(self):
        raise Exception("Metodo no implementado")


class Coleccion:
    __metaclass__ = ABCMeta

    @abstractmethod
    def iterator(self):
        raise Exception("Metodo no implementado")


class ListaCategoria(Coleccion):
    categorias = []

    def agregar_categoria(self, categoria):
        self.categorias.append(categoria)

    def iterator(self):
        return ListaCategoria.ListaCategoriaIterator(self.categorias)

    class ListaCategoriaIterator(Iterator):

        index = 0

        def __init__(self, categorias):
            self.categorias = categorias

        def has_next(self):
            if self.index < len(self.categorias):
                return True
            return False

        def next(self):
            if self.has_next():
                self.index += 1
            return self.categorias[self.index - 1]

        def current(self):
            return self.categorias[self.index]

        def first(self):
            return self.categorias[0]


class ModuloCategoria(QtWidgets.QMainWindow):
    def __init__(self, empleado, ui_padre):
        super(ModuloCategoria, self).__init__()

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.ui_padre.hide()

        uic.loadUi('ModuloCategoria/ModuloCategoria.ui', self)
        self.bRegistrar.clicked.connect(self.registrar_categoria)
        self.bModificar.clicked.connect(self.modificar_categoria)
        self.llenar_combo_box()
        self.cbCategoriasExistentes.currentIndexChanged.connect(self.obtenerDatos)
        self.obtenerDatos()
        self.bEliminar.clicked.connect(self.eliminar_categoria)
        self.llenar_combo_box()

        self.ventana_confirmacion = QMessageBox()

        self.show()

    def actualizar_texto_ventana_confirmacion(self, texto):
        self.ventana_confirmacion.setText(texto)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        # respuesta = ventana_confirmacion.exec_()

    def registrar_categoria(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                categoria = Categoria()
                categoria.nombre = self.leNombre.text()
                categoria.estado = 'ACTIVO'

                categoria.crear_categoria()
                self.llenar_combo_box()
                self.leNombre.clear()

                self.actualizar_texto_ventana_confirmacion("Categoria registrada correctamente")
                self.ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion(str("Error registrando categoria: "+e))
                self.ventana_confirmacion.exec_()

    def modificar_categoria(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                nombre_categoria_modificar = self.cbCategoriasExistentes.currentText()
                categoria = Categoria.get(Categoria.nombre == nombre_categoria_modificar, Categoria.estado == 'ACTIVO')
                categoria.nombre = self.leNuevoNombre.text()

                categoria.modificar_categoria()

                self.leNuevoNombre.clear()
                self.llenar_combo_box()

                self.actualizar_texto_ventana_confirmacion("Categoria modificada correctamente")
                self.ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error modificando categoria: "+str(e))
                self.ventana_confirmacion.exec_()

    def obtenerDatos(self):
        self.leNuevoNombre.setText(self.cbCategoriasExistentes.currentText())

    def eliminar_categoria(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                nombre_categoria_modificar = self.cbCategoriasExistentes.currentText()
                categoria = Categoria.get(Categoria.nombre == nombre_categoria_modificar, Categoria.estado == 'ACTIVO')

                categoria.eliminar_categoria()

                self.llenar_combo_box()

                self.actualizar_texto_ventana_confirmacion("Categoria eliminada correctamente")
                self.ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error eliminando categoria"+str(e))
                self.ventana_confirmacion.exec_()

    def llenar_combo_box(self):
        self.cbCategoriasExistentes.clear()
        self.comboBox.clear()
        self.cbCategoriasExistentes_2.clear()
        for categoria in Categoria.select().where(Categoria.estado == 'ACTIVO'):
            self.cbCategoriasExistentes.addItem(categoria.nombre)
            self.comboBox.addItem(categoria.nombre)
            self.cbCategoriasExistentes_2.addItem(categoria.nombre)

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()
