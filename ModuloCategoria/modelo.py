from peewee import *

from ConfiguracionORM.BaseModel import BaseModel


class Categoria(BaseModel):
    nombre = CharField(max_length=200)

    def crear_categoria(self):
        self.save()
        print("categoria creada")

    def modificar_categoria(self):
        self.save()
        print("categoria modificada")

    def eliminar_categoria(self):
        self.update(estado='INACTIVO').where(Categoria.id == self.id).execute()
        print("categoria eliminada")

    def buscar_categoria(self, nombre_categoria):
        return Categoria.get(Categoria.nombre==nombre_categoria, Categoria.estado == 'ACTIVO')
