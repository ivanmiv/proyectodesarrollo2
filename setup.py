from cx_Freeze import setup, Executable
import os
os.environ['TCL_LIBRARY'] = 'd:/Python/Python35-32/tcl/tcl8.6'
os.environ['TK_LIBRARY'] = 'd:/Python/Python35-32/tcl/tk8.6'


# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages=[
    'matplotlib',
    'matplotlib.backends.backend_tkagg',
    'passlib',
    'tkinter',
    'reportlab',
    'numpy',
    'ConfiguracionORM',
    'ModuloCategoria',
    'ModuloEmpleado',
    'ModuloFactura',
    'ModuloItemMenu',
    'ModuloMesa',
    'ModuloOrden',
    'ModuloSucursal',
    'Principal',
],
    excludes=[],
    include_files=['Imagenes',
                   'd:/Python/Python35-32/DLLs/tcl86t.dll',
                   'd:/Python/Python35-32/DLLs/tk86t.dll',], )

import sys

base = 'Win32GUI' if sys.platform == 'win32' else None

executables = [
    Executable('main.py', base=base)
]

setup(name='main',
      version='1.0',
      description='',
      options=dict(build_exe=buildOptions),
      executables=executables)
