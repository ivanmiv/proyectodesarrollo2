#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt5 import uic, QtWidgets
from PyQt5.QtCore import pyqtSlot, QTime
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QFileDialog, QTableWidgetItem, QGraphicsPixmapItem, QGraphicsScene
from PyQt5.QtWidgets import QMessageBox
from peewee import IntegrityError, DoesNotExist

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter
from ModuloEmpleado.modelo import Empleado, Turno
from ModuloSucursal.modelo import Sucursal


class ModuloEmpleados(QtWidgets.QMainWindow):
    def __init__(self, empleado, ui_padre):
        super(ModuloEmpleados, self).__init__()
        uic.loadUi('ModuloEmpleado/ModuloEmpleado.ui', self)

        self.usuario = empleado
        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.b_registrar_empleado.clicked.connect(self.on_click_b_registrar_empleado)
        self.b_eliminar_empleado.clicked.connect(self.on_click_b_eliminar_empleado)
        self.b_seleccionar_foto_crear.clicked.connect(self.on_click_b_seleccionar_fotografia_crear)
        self.b_obtener_informacion_empleado.clicked.connect(self.on_click_b_obtener_informacion_empleado)

        self.b_registrar_modificacion_empleado.clicked.connect(self.on_click_b_registrar_modificacion_empleado)
        self.b_seleccionar_foto_modificar.clicked.connect(self.on_click_b_seleccionar_fotografia_modificar)
        self.b_obtener_informacion_modificar_empleado.clicked.connect(
            self.on_click_b_obtener_informacion_modificar_empleado)
        self.b_reiniciar_intentos_inicio_sesion.clicked.connect(self.on_click_b_reiniciar_intentos_inicio_sesion)

        self.actualizar_t_empleados_eliminar()

        self.horarios_modificar = []

        self.horarios_modificar.append([self.te_lunes_inicio_modificar, self.te_lunes_fin_modificar, 'LUNES'])
        self.horarios_modificar.append([self.te_martes_inicio_modificar, self.te_martes_fin_modificar, 'MARTES'])
        self.horarios_modificar.append(
            [self.te_miercoles_inicio_modificar, self.te_miercoles_fin_modificar, 'MIERCOLES'])
        self.horarios_modificar.append([self.te_jueves_inicio_modificar, self.te_jueves_fin_modificar, 'JUEVES'])
        self.horarios_modificar.append([self.te_viernes_inicio_modificar, self.te_viernes_fin_modificar, 'VIERNES'])
        self.horarios_modificar.append([self.te_sabado_inicio_modificar, self.te_sabado_fin_modificar, 'SABADO'])
        self.horarios_modificar.append([self.te_domingo_inicio_modificar, self.te_domingo_fin_modificar, 'DOMINGO'])
        self.empleado = None

        if (self.usuario.cargo != 'GERENTE'):
            # Remueve pestaña de eliminacion de empleado
            self.tab_modulo_empleado.removeTab(3)
            # Remueve pestaña de eliminacion de empleado
            self.tab_modulo_empleado.removeTab(0)

            # Dejar fijo el numero de documento para consultar o modificar
            self.le_identificacion_obtener.setText(str(self.usuario.numero_documento_identificacion))
            self.le_identificacion_obtener.setReadOnly(True)

            self.le_Identificacion_modificar.setText(str(self.usuario.numero_documento_identificacion))
            self.le_Identificacion_modificar.setReadOnly(True)

            self.b_reiniciar_intentos_inicio_sesion.setVisible(False)

            self.cb_cargo_consultar.setEnabled(False)
            self.cb_cargo_modificar.setEnabled(False)
            self.cb_tipo_documento_consultar.setEnabled(False)
            self.cb_tipo_documento_modificar.setEnabled(False)
            self.cb_sucursal_modificar_empleado.setEnabled(False)
            self.cb_sucursal_consultar_empleado.setEnabled(False)

        elif (self.usuario.cargo == 'GERENTE'):
            self.cb_cargo_modificar.setEnabled(True)
            self.cb_tipo_documento_modificar.setEnabled(True)
            self.le_nombre_modificar.setReadOnly(False)
            self.le_apellido_modificar.setReadOnly(False)
            self.le_identificacion_modificar.setReadOnly(False)
            self.le_nombre_modificar.setReadOnly(False)

        self.ventana_confirmacion = QMessageBox()

        self.actualizar_cb_sucursales()

        self.show()

    def actualizar_texto_ventana_confirmacion(self, texto):
        self.ventana_confirmacion.setText(texto)
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")
        # respuesta = ventana_confirmacion.exec_()

    def actualizar_cb_sucursales(self):
        self.cb_sucursal_crear_empleado.clear()
        self.cb_sucursal_consultar_empleado.clear()
        self.cb_sucursal_modificar_empleado.clear()
        self.cb_sucursal_crear_empleado.addItem('Seleccione una sucursal')
        self.cb_sucursal_consultar_empleado.addItem('Seleccione una sucursal')
        self.cb_sucursal_modificar_empleado.addItem('Seleccione una sucursal')

        for sucursal in Sucursal.select().where(Sucursal.estado == 'ACTIVO'):
            self.cb_sucursal_crear_empleado.addItem(sucursal.nombre)
            self.cb_sucursal_consultar_empleado.addItem(sucursal.nombre)
            self.cb_sucursal_modificar_empleado.addItem(sucursal.nombre)

    @pyqtSlot()
    def on_click_b_registrar_empleado(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:

                empleado = Empleado()
                empleado.nombres = self.le_nombre_crear.text().strip()
                empleado.apellidos = self.le_apellido_crear.text().strip()
                empleado.tipo_documento_identificacion = self.cb_tipo_documento_crear.currentText()
                empleado.numero_documento_identificacion = self.le_identificacion_crear.text().strip()
                empleado.cargo = self.cb_cargo_crear.currentText()
                empleado.set_fotografia(self.l_archivo_seleccionado_foto_crear.text())
                empleado.direccion = self.le_direccion_crear.text().strip()
                empleado.telefono_fijo = self.le_telefono_fijo_crear.text().strip()
                empleado.telefono_celular = self.le_telefono_celular_crear.text().strip()
                empleado.correo_electronico = self.le_coreo_electronico_crear.text().strip()
                empleado.asignar_contrasena(self.le_password_crear.text())
                empleado.estado = 'ACTIVO'
                empleado.sucursal = Sucursal().consultar_sucursal(self.cb_sucursal_crear_empleado.currentText())

                empleado.crear_empleado()
                horarios_crear = []

                horarios_crear.append([self.te_lunes_inicio_crear.time(), self.te_lunes_fin_crear.time(), 'LUNES'])
                horarios_crear.append([self.te_martes_inicio_crear.time(), self.te_martes_fin_crear.time(), 'MARTES'])
                horarios_crear.append(
                    [self.te_miercoles_inicio_crear.time(), self.te_miercoles_fin_crear.time(), 'MIERCOLES'])
                horarios_crear.append([self.te_jueves_inicio_crear.time(), self.te_jueves_fin_crear.time(), 'JUEVES'])
                horarios_crear.append(
                    [self.te_viernes_inicio_crear.time(), self.te_viernes_fin_crear.time(), 'VIERNES'])
                horarios_crear.append([self.te_sabado_inicio_crear.time(), self.te_sabado_fin_crear.time(), 'SABADO'])
                horarios_crear.append(
                    [self.te_domingo_inicio_crear.time(), self.te_domingo_fin_crear.time(), 'DOMINGO'])

                for horas in horarios_crear:
                    horas[0] = horas[0].toString("HH:mm:ss")
                    horas[1] = horas[1].toString("HH:mm:ss")
                    turno, creado = Turno.get_or_create(empleado=empleado, dia=horas[2],
                                                        defaults={'hora_inicio': horas[0], 'hora_fin': horas[1]})
                    if not creado:
                        turno.hora_inicio_crear = horas[0]
                        turno.hora_fin_crear = horas[1]
                        turno.save()

                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText("Empleado registrado correctamente")
                ventana_confirmacion.exec_()

            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error registrando empleado: "+str(e))
                self.ventana_confirmacion.exec_()

    def on_click_b_seleccionar_fotografia_crear(self):

        opciones = QFileDialog.Options()
        opciones |= QFileDialog.DontUseNativeDialog
        ruta_archivo, _ = QFileDialog.getOpenFileName(self, "Seleccione una imagen", "",
                                                      "Imagenes (*.png *.jpeg *.bmp *.jpg)", options=opciones)
        if ruta_archivo:
            self.l_archivo_seleccionado_foto_crear.setText(ruta_archivo)
            pixmap_imagen = QPixmap.fromImage(QImage(ruta_archivo))
            escena = QGraphicsScene()
            self.gw_fotografia_crear.setScene(escena)

            tamaño_escena = self.gw_fotografia_crear.sceneRect()
            pixmap_imagen.scaledToWidth(tamaño_escena.width())
            imagen = QGraphicsPixmapItem(pixmap_imagen)

            escena.addItem(imagen)
            self.gw_fotografia_crear.fitInView(imagen)
            imagen.setPos(0, 0)
            self.gw_fotografia_crear.show()




    def actualizar_t_empleados_eliminar(self):

        empleados = Empleado.select().where(Empleado.estado == 'ACTIVO')
        self.t_empleados_eliminar.setColumnCount(3)
        self.t_empleados_eliminar.setHorizontalHeaderLabels(
            ['Documento', 'Nombre', 'Estado'])
        self.t_empleados_eliminar.clearContents()
        self.t_empleados_eliminar.setRowCount(0)
        for empleado in empleados:
            rowPosition = self.t_empleados_eliminar.rowCount()
            self.t_empleados_eliminar.insertRow(rowPosition)
            self.t_empleados_eliminar.setItem(rowPosition, 0,
                                              QTableWidgetItem(str(empleado.numero_documento_identificacion)))
            self.t_empleados_eliminar.setItem(rowPosition, 1,
                                              QTableWidgetItem("%s %s" % (empleado.nombres, empleado.apellidos)))
            self.t_empleados_eliminar.setItem(rowPosition, 2, QTableWidgetItem(empleado.estado))

    @pyqtSlot()
    def on_click_b_eliminar_empleado(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                numero_documento = self.t_empleados_eliminar.selectedItems()[0].text()

                empleado = Empleado.get(Empleado.numero_documento_identificacion == numero_documento,
                                                 Empleado.estado == 'ACTIVO')
                empleado.eliminar_empleado()

                self.actualizar_t_empleados_eliminar()
                self.actualizar_texto_ventana_confirmacion("Empleado eliminado correctamente")
                self.ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error eliminando empleado: "+str(e))
                self.ventana_confirmacion.exec_()

    @pyqtSlot()
    def on_click_b_obtener_informacion_empleado(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                identificacion_empleado = self.le_identificacion_obtener.text().strip()

                try:
                    self.empleado = Empleado.get(Empleado.numero_documento_identificacion == identificacion_empleado,
                                                 Empleado.estado == 'ACTIVO')

                    horarios_consultar = []

                    horarios_consultar.append([self.te_lunes_inicio_consultar, self.te_lunes_fin_consultar, 'LUNES'])
                    horarios_consultar.append([self.te_martes_inicio_consultar, self.te_martes_fin_consultar, 'MARTES'])
                    horarios_consultar.append(
                        [self.te_miercoles_inicio_consultar, self.te_miercoles_fin_consultar, 'MIERCOLES'])
                    horarios_consultar.append([self.te_jueves_inicio_consultar, self.te_jueves_fin_consultar, 'JUEVES'])
                    horarios_consultar.append([self.te_viernes_inicio_consultar, self.te_viernes_fin_consultar, 'VIERNES'])
                    horarios_consultar.append([self.te_sabado_inicio_consultar, self.te_sabado_fin_consultar, 'SABADO'])
                    horarios_consultar.append([self.te_domingo_inicio_consultar, self.te_domingo_fin_consultar, 'DOMINGO'])

                except DoesNotExist as e:
                    print(e)
                    texto = "No existe el empleado con numero de identificacion %s" % identificacion_empleado
                    ventana_confirmacion = QMessageBox()
                    ventana_confirmacion.setWindowTitle("Mensaje")
                    ventana_confirmacion.setText(texto)
                    ventana_confirmacion.exec_()
                else:
                    # Asignar informacion del empleado a los campos
                    self.le_nombre_consultar.setText(self.empleado.nombres)
                    self.le_apellido_consultar.setText(self.empleado.apellidos)
                    self.cb_tipo_documento_consultar.setCurrentText(self.empleado.tipo_documento_identificacion)
                    self.le_identificacion_consultar.setText(self.empleado.numero_documento_identificacion)
                    self.cb_cargo_consultar.setCurrentText(self.empleado.cargo)
                    self.le_direccion_consultar.setText(self.empleado.direccion)
                    self.le_telefono_fijo_consultar.setText(self.empleado.telefono_fijo)
                    self.le_telefono_celular_consultar.setText(self.empleado.telefono_celular)
                    self.le_coreo_electronico_consultar.setText(self.empleado.correo_electronico)
                    self.cb_sucursal_consultar_empleado.setCurrentText(self.empleado.sucursal.nombre)
                    for horas in horarios_consultar:
                        turno = Turno.get(Turno.empleado == self.empleado, Turno.dia == horas[2])
                        horas[0].setTime(QTime(turno.hora_inicio.hour, turno.hora_inicio.minute))
                        horas[1].setTime(QTime(turno.hora_fin.hour, turno.hora_fin.minute))

                    self.empleado.get_fotografia()
                    pixmap_imagen = QPixmap.fromImage(QImage('img.png'))
                    escena = QGraphicsScene()
                    self.gw_fotografia_consultar.setScene(escena)

                    tamaño_escena = self.gw_fotografia_consultar.sceneRect()
                    pixmap_imagen.scaledToWidth(tamaño_escena.width())
                    imagen = QGraphicsPixmapItem(pixmap_imagen)

                    escena.addItem(imagen)
                    self.gw_fotografia_consultar.fitInView(imagen)
                    imagen.setPos(0, 0)
                    self.gw_fotografia_consultar.show()


            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error consultando empleado: "+str(e))
                self.ventana_confirmacion.exec_()

    @pyqtSlot()
    def on_click_b_obtener_informacion_modificar_empleado(self):

        with BaseDatosAdapter.base_datos.atomic():
            try:
                identificacion_empleado = self.le_Identificacion_modificar.text().strip()
                self.empleado = Empleado.get(Empleado.numero_documento_identificacion == identificacion_empleado,
                                                 Empleado.estado == 'ACTIVO')
            except DoesNotExist as e:
                texto = "No existe el empleado con número de identificación %s" % identificacion_empleado
                ventana_confirmacion = QMessageBox()
                ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText(texto)
                ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error obteniendo la informacion del empleado: "+str(e))
                self.ventana_confirmacion.exec_()
            else:
                try:
                    self.le_nombre_modificar.setText(self.empleado.nombres)
                    self.le_apellido_modificar.setText(self.empleado.apellidos)
                    self.cb_tipo_documento_modificar.setCurrentText(self.empleado.tipo_documento_identificacion)
                    self.le_identificacion_modificar.setText(self.empleado.numero_documento_identificacion)
                    self.cb_cargo_modificar.setCurrentText(self.empleado.cargo)
                    self.le_direccion_modificar.setText(self.empleado.direccion)
                    self.le_telefono_fijo_modificar.setText(self.empleado.telefono_fijo)
                    self.le_telefono_celular_modificar.setText(self.empleado.telefono_celular)
                    self.le_coreo_electronico_modificar.setText(self.empleado.correo_electronico)
                    self.cb_sucursal_modificar_empleado.setCurrentText(self.empleado.sucursal.nombre)

                    for horas in self.horarios_modificar:
                        turno = Turno.get(Turno.empleado == self.empleado, Turno.dia == horas[2])
                        horas[0].setTime(QTime(turno.hora_inicio.hour, turno.hora_inicio.minute))
                        horas[1].setTime(QTime(turno.hora_fin.hour, turno.hora_fin.minute))


                    self.empleado.get_fotografia()
                    pixmap_imagen = QPixmap.fromImage(QImage('img.png'))
                    escena = QGraphicsScene()
                    self.gw_fotografia_modificar.setScene(escena)

                    tamaño_escena = self.gw_fotografia_modificar.sceneRect()
                    pixmap_imagen.scaledToWidth(tamaño_escena.width())
                    imagen = QGraphicsPixmapItem(pixmap_imagen)

                    escena.addItem(imagen)
                    self.gw_fotografia_modificar.fitInView(imagen)
                    imagen.setPos(0, 0)
                    self.gw_fotografia_modificar.show()




                except Exception as e:
                    print(e)

    @pyqtSlot()
    def on_click_b_registrar_modificacion_empleado(self):
        with BaseDatosAdapter.base_datos.atomic():
            try:
                """
                self.le_nombre_modificar.setText(self.empleado.nombres)
                self.le_apellido_modificar.setText(self.empleado.apellidos)
                self.cb_tipo_documento_modificar.setCurrentText(self.empleado.tipo_documento_identificacion)
                self.le_identificacion_modificar.setText(self.empleado.numero_documento_identificacion)
                self.cb_cargo_modificar.setCurrentText(self.empleado.cargo)
                self.le_direccion_modificar.setText(self.empleado.direccion)
                self.le_telefono_fijo_modificar.setText(self.empleado.telefono_fijo)
                self.le_telefono_celular_modificar.setText(self.empleado.telefono_celular)
                self.le_coreo_electronico_modificar.setText(self.empleado.correo_electronico)
                """
                self.empleado.nombres = self.le_nombre_modificar.text().strip()
                self.empleado.apellidos = self.le_apellido_modificar.text().strip()
                self.empleado.tipo_documento_identificacion = self.cb_tipo_documento_modificar.currentText()
                self.empleado.numero_documento_identificacion = self.le_identificacion_modificar.text().strip()
                self.empleado.cargo = self.cb_cargo_modificar.currentText()

                if not self.l_archivo_seleccionado_foto_modificar.text().upper() == 'NINGUNO':
                    self.empleado.set_fotografia(self.l_archivo_seleccionado_foto_modificar.text())

                self.empleado.direccion = self.le_direccion_modificar.text().strip()
                self.empleado.telefono_fijo = self.le_telefono_fijo_modificar.text().strip()
                self.empleado.telefono_celular = self.le_telefono_celular_modificar.text().strip()
                self.empleado.correo_electronico = self.le_coreo_electronico_modificar.text().strip()
                self.empleado.sucursal = Sucursal().consultar_sucursal(self.cb_sucursal_modificar_empleado.currentText())

                if not self.le_password_modificar.text().strip() == "":
                    self.empleado.asignar_contrasena(self.le_password_modificar.text())
                self.empleado.actualizar_empleado()

                for horas in self.horarios_modificar:
                    hora_inicio = horas[0].time().toString("HH:mm:ss")
                    hora_fin = horas[1].time().toString("HH:mm:ss")
                    turno, creado = Turno.get_or_create(empleado=self.empleado, dia=horas[2],
                                                        defaults={'hora_inicio': hora_inicio, 'hora_fin': hora_fin})
                    if not creado:
                        turno.hora_inicio = hora_inicio
                        turno.hora_fin = hora_fin
                        turno.save()


                self.actualizar_texto_ventana_confirmacion("Empleado modificado correctamente")
                self.ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error modificando empleado: "+str(e))
                self.ventana_confirmacion.exec_()

    @pyqtSlot()
    def on_click_b_seleccionar_fotografia_modificar(self):

        opciones = QFileDialog.Options()
        opciones |= QFileDialog.DontUseNativeDialog
        ruta_archivo, _ = QFileDialog.getOpenFileName(self, "Seleccione una imagen", "",
                                                      "Imagenes (*.png *.jpeg *.bmp *.jpg)", options=opciones)
        if ruta_archivo:
            self.l_archivo_seleccionado_foto_modificar.setText(ruta_archivo)
            pixmap_imagen = QPixmap.fromImage(QImage(ruta_archivo))
            escena = QGraphicsScene()
            self.gw_fotografia_modificar.setScene(escena)

            tamaño_escena = self.gw_fotografia_modificar.sceneRect()
            pixmap_imagen.scaledToWidth(tamaño_escena.width())
            imagen = QGraphicsPixmapItem(pixmap_imagen)

            escena.addItem(imagen)
            self.gw_fotografia_modificar.fitInView(imagen)
            imagen.setPos(0, 0)
            self.gw_fotografia_modificar.show()


    @pyqtSlot()
    def on_click_b_reiniciar_intentos_inicio_sesion(self):
        with BaseDatosAdapter.base_datos.atomic():
            identificacion_empleado = self.le_Identificacion_modificar.text().strip()
            try:
                self.empleado = Empleado.get(Empleado.numero_documento_identificacion == identificacion_empleado,
                                                 Empleado.estado == 'ACTIVO')
                self.empleado.intentos_inicio_sesion = 0
                self.empleado.save()
                texto = "Intentos de inicio de sesión reiniciados"
                ventana_confirmacion = QMessageBox()
                self.ventana_confirmacion.setWindowTitle("Mensaje")
                ventana_confirmacion.setText(texto)
                ventana_confirmacion.exec_()
            except DoesNotExist as e:
                texto = "No existe el empleado con número de identificación %s" % identificacion_empleado
                self.actualizar_texto_ventana_confirmacion(texto)
                self.ventana_confirmacion.exec_()
            except Exception as e:
                BaseDatosAdapter.base_datos.rollback()
                self.actualizar_texto_ventana_confirmacion("Error modificando empleado: "+str(e))
                self.ventana_confirmacion.exec_()

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()
