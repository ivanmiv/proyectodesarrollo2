from passlib.hash import pbkdf2_sha256
from peewee import *

from ConfiguracionORM.BaseModel import BaseModel
from ModuloSucursal.modelo import Sucursal


def fotografia_por_defecto():
    with open('Imagenes/usuario_por_defecto.png', "rb+") as imageFile:
        return bytearray(imageFile.read())

class Empleado(BaseModel):
    nombres = CharField()
    apellidos = CharField(default="")
    tipo_documento_identificacion = CharField(max_length=20,
                                              constraints=[
                                                  Check('tipo_documento_identificacion = \'CÉDULA DE CIUDADANÍA\' '
                                                        'or tipo_documento_identificacion = \'TARJETA DE IDENTIDAD\' '
                                                        'or tipo_documento_identificacion = \'PASAPORTE\' ')])
    numero_documento_identificacion = CharField(max_length=20, unique=True)
    cargo = CharField(max_length=20, constraints=[Check('cargo = \'GERENTE\' '
                                                        'or cargo = \'CAJERO\' '
                                                        'or cargo = \'MESERO\'')])
    fotografia = BlobField(default=fotografia_por_defecto())
    direccion = CharField(default="")
    telefono_fijo = CharField(default="")
    telefono_celular = CharField(default="")
    correo_electronico = CharField(default="")
    sucursal = ForeignKeyField(Sucursal)
    intentos_inicio_sesion = IntegerField(default=0)
    __contrasena = CharField()

    def asignar_contrasena(self, contrasena):
        self.__contrasena = pbkdf2_sha256.hash(contrasena)

    def verificar_contrasena(self, contrasena):
        try:
            return pbkdf2_sha256.verify(contrasena,
                                        self.__contrasena)
        except Exception as e:
            print(e)

        return False

    def __str__(self):
        return self.nombres

    def get_fotografia(self):
        import io
        from PIL import Image
        image = Image.open(io.BytesIO(self.fotografia))
        image.save('img.png')
        return image

    def set_fotografia(self, ruta_imagen):
        with open(ruta_imagen, "rb+") as imageFile:
            self.fotografia = bytearray(imageFile.read())

    def crear_empleado(self):
        self.save()

    def actualizar_empleado(self):
        self.save()

    def eliminar_empleado(self):
        self.estado = 'INACTIVO'
        self.save()


class Turno(BaseModel):
    empleado = ForeignKeyField(Empleado)
    hora_inicio = TimeField()
    hora_fin = TimeField()
    dia = CharField(max_length=20, constraints=[Check('dia = \'LUNES\' or dia = \'MARTES\' or dia = \'MIERCOLES\' or '
                                                      'dia = \'JUEVES\' or dia = \'VIERNES\' or dia = \'SABADO\' or '
                                                      'dia = \'DOMINGO\' ')]
                    )
