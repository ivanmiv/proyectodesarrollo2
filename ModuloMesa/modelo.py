from peewee import BooleanField, IntegerField, ForeignKeyField
import peewee
from ConfiguracionORM.BaseModel import BaseModel
from ModuloSucursal.modelo import Sucursal


class Mesa(BaseModel):
    ocupado = BooleanField(default=False)
    numero_interno = IntegerField(null=True)
    sucursal = ForeignKeyField(Sucursal)

    def crear_mesa(self):
        self.save()

    def modificar_mesa(self):
        self.save()

    def ocupar_mesa(self, numero_interno_mesa):
        query = self.update(ocupado=True).where(Mesa.numero_interno == numero_interno_mesa)
        query.execute()

    def eliminar_mesa(self):
        query = self.update(estado='INACTIVO').where(Mesa.id == self.id)

    def obtener_cantidad_mesas(self, sucursal_buscada):
        return Mesa.select().where(Mesa.sucursal_id == sucursal_buscada.id, Mesa.estado == 'ACTIVO').count()

    def obtener_mesa(self, numero_interno_mesa, sucursal_buscada):
        return Mesa.get(Mesa.numero_interno == numero_interno_mesa, Mesa.sucursal_id == sucursal_buscada.id)
    def obtener_ultimo_numero_interno(self, sucursal_buscada):
        return Mesa.select(peewee.fn.max(Mesa.numero_interno).alias('numero_interno_max')).where(Mesa.estado == 'ACTIVO').group_by(sucursal_buscada.id).scalar()
