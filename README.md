## Proyecto desarrollo 2 ##

## Integrantes ##
* José Alejandro Libreros
* Iván Miguel Viveros
* Juan David Suaza
* Juan José Salazar

## Enlace al repositorio ##

https://bitbucket.org/ivanmiv/proyectodesarrollo2

## Instalación del proyecto ##

# Requisitos #
*python 3.5.2
*Postgres


1- Disponer una base de datos postgres vacia

2- Instalar los paquetes requeridos por el proyecto ejecutando desde la carpeta del proyecto el comando ( estando preferiblemente en un ambiente virtual):

	pip3 install -r requirements.txt

3- Instalar tkinter siguiendo la guia de instalación propia de tkinter:

	http://www.tkdocs.com/tutorial/install.html
	

	

## Ejecutar el proyecto ##

1-Mediante la consola ubicarse en al carpeta del proyecto (en caso de tener ambiente virtual activarlo)

2-Modificar el archivo conf.bd con los datos correspondientes a la base de datos postgres utilizada (la versión por defecto del archivo conf.bd se encuentra en la sección de descargas del repositorio)

## Nota:  

## En caso de no encontrar el archivo conf.bd el programa utiliza la configuración por defecto:

nombre_base_datos=desarrollo2

usuario=postgres

password=postgres

host=127.0.0.1

## 

3-Ejecutar el comando :

	python3 main.py


## Notas:

## 1. Para cargar los datos iniciales del menu y una sucursal base con un empleado de cada tipo ejecutar el archivo cargarDatos.py:## 

    python3 cargarDatos.py

## 2. Generar el ejecutable del proyecto : ## 
i) Tener instalado el paquete cx-freeze (se encuentra en requirements.txt)

ii) configurar las rutas a los archivos tcl8.6, tk8.6

En caso de estar en windows tambien las rutas a los archivos: tcl86t.dll, tk86t.dll

Ejemplo:

os.environ['TCL_LIBRARY'] = 'd:/Python/Python35-32/tcl/tcl8.6'

os.environ['TK_LIBRARY'] = 'd:/Python/Python35-32/tcl/tk8.6'

include_files=['Imagenes','d:/Python/Python35-32/DLLs/tcl86t.dll','d:/Python/Python35-32/DLLs/tk86t.dll',], )

iii) Ejecutar el comando:

	python3 setup.py build
	
##


