from abc import ABCMeta, abstractmethod
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import numpy as np
import peewee

from ModuloEmpleado.modelo import Empleado
from ModuloFactura.GeneradorFactura import Generador
from ModuloFactura.modelo import Factura, ItemFactura
from ModuloOrden.modelo import Orden


class ReportesInterface:
    __metaclass__ = ABCMeta

    @abstractmethod
    def mostrar_reporte(self): raise NotImplementedError


class ReportePlatosMasVendidos(ReportesInterface):
    def __init__(self):
        self.nombre_platos = []
        self.cantidad_vendida = []

    def mostrar_reporte(self):
        # SQL: select nombre, sum(cantidad) from itemfactura group by (nombre) order by sum DESC;
        fecha_actual = datetime.now()
        fecha_inicio_mes = fecha_actual - timedelta(days=fecha_actual.day - 1)
        try:
            i = 0
            for item in ItemFactura.select(
                    ItemFactura.nombre, peewee.fn.SUM(ItemFactura.cantidad).alias('sum')).group_by(
                ItemFactura.nombre).join(Factura).join(Orden).where(
                Orden.hora_inicio.between(fecha_inicio_mes, fecha_actual)).order_by(
                peewee.fn.SUM(ItemFactura.cantidad).desc()):
                if (i < 10):
                    self.nombre_platos.append(item.nombre)
                    self.cantidad_vendida.append(item.sum)
                    i = i + 1
                else:
                    break
        except Exception as e:
            print(str(e))

        # separacion entre barras
        pos_y = np.arange(len(self.nombre_platos))

        # Matpltlib
        
        plt.bar(pos_y, self.cantidad_vendida, align='center', color='red')
        plt.xticks(pos_y, self.nombre_platos, verticalalignment='baseline', horizontalalignment='center', rotation=-90)
        plt.ylabel('Cantidad Vendida')
        plt.xlabel('Item Menú')
        
        __mes = fecha_actual.month
        __year = fecha_actual.year

        _mes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
               'Noviembre', 'Diciembre']
        

        plt.title('10 Items del menú más vendidos en: ' + _mes[__mes-1] + ' del ' + str(__year))
        plt.tight_layout()
        plt.show()
        #return 0


class ReportePlatosMenosVendidos(ReportesInterface):
    def __init__(self):
        self.nombre_platos = []
        self.cantidad_vendida = []

    def mostrar_reporte(self):
        # obtencion de datos
        # SQL: select nombre, sum(cantidad) from itemfactura group by (nombre) order by sum;
        fecha_actual = datetime.now()
        fecha_inicio_mes = fecha_actual - timedelta(days=30 * 6)
        try:
            i = 0
            for item in ItemFactura.select(ItemFactura.nombre,
                                           peewee.fn.SUM(ItemFactura.cantidad).alias('sum')).group_by(
                    ItemFactura.nombre).join(Factura).join(Orden).where(
                    Orden.hora_inicio.between(fecha_inicio_mes, fecha_actual)).order_by(
                    peewee.fn.SUM(ItemFactura.cantidad)):
                if (i < 10):
                    self.nombre_platos.append(item.nombre)
                    self.cantidad_vendida.append(item.sum)
                    i = i + 1
                else:
                    break
        except Exception as e:
            print(str(e))
        
        # separacion entre barras
        pos_y = np.arange(len(self.nombre_platos))

        # Matpltlib
        plt.bar(pos_y, self.cantidad_vendida, align='center', color='red')
        plt.xticks(pos_y, self.nombre_platos, verticalalignment='baseline', horizontalalignment='center', rotation=-90)
        plt.ylabel('Cantidad Vendida')
        plt.xlabel('Item Menú')
        
        __mes = fecha_actual.month
        __year = fecha_actual.year

        _mes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
               'Noviembre', 'Diciembre']

        plt.title('10 Items del menú menos vendidos en: ' + _mes[__mes-1] + ' del ' + str(__year))
        plt.tight_layout()
        plt.show()


class ReporteTiempoPromedio(ReportesInterface):
    def __init__(self):
        self.mes = [0, 0, 0, 0, 0, 0]
        self.tiempo = [0, 0, 0, 0, 0, 0]

    def mostrar_reporte(self):
        # obtencion de datos
        hora_actual = datetime.now()
        lista_meses = []
        years = []
        lista_frecuencias = [0, 0, 0, 0, 0, 0]
        mes_actual = hora_actual.month
        current_year = hora_actual.year
        iterador = 0
        while iterador < 6:
            lista_meses.append(mes_actual)
            years.append(current_year)
            mes_actual = mes_actual - 1
            if (mes_actual == 0):
                mes_actual = 12
                current_year = current_year - 1
            iterador = iterador + 1

        # Se obtienen los registros que estan entre los 6 meses anteriores al actual
        # Se obtiene el tiempo que demoro la orden (entrega-inicio) en segundos
        # Se acumulan los valores en self.tiempo y se incrementan las frecuencias

        for orden in Orden.select(Orden.hora_inicio, Orden.hora_entrega_ultimo_item).order_by(Orden.hora_inicio):
            if (orden.hora_entrega_ultimo_item is not None):
                timedelta = hora_actual - orden.hora_inicio
                if (timedelta.days <= 30 * 6):
                    if (orden.hora_inicio.month == lista_meses[0]):
                        lista_frecuencias[0] = lista_frecuencias[0] + 1
                        delta_tiempo = orden.hora_entrega_ultimo_item - orden.hora_inicio
                        self.tiempo[0] = self.tiempo[0] + delta_tiempo.seconds / 60 + delta_tiempo.days * 1440

                    elif (orden.hora_inicio.month == lista_meses[1]):
                        lista_frecuencias[1] = lista_frecuencias[1] + 1
                        delta_tiempo = orden.hora_entrega_ultimo_item - orden.hora_inicio
                        self.tiempo[1] = self.tiempo[1] + delta_tiempo.seconds / 60 + delta_tiempo.days * 1440

                    elif (orden.hora_inicio.month == lista_meses[2]):
                        lista_frecuencias[2] = lista_frecuencias[2] + 1
                        delta_tiempo = orden.hora_entrega_ultimo_item - orden.hora_inicio
                        self.tiempo[2] = self.tiempo[2] + delta_tiempo.seconds / 60 + delta_tiempo.days * 1440

                    elif (orden.hora_inicio.month == lista_meses[3]):
                        lista_frecuencias[3] = lista_frecuencias[3] + 1
                        delta_tiempo = orden.hora_entrega_ultimo_item - orden.hora_inicio
                        self.tiempo[3] = self.tiempo[3] + delta_tiempo.seconds / 60 + delta_tiempo.days * 1440

                    elif (orden.hora_inicio.month == lista_meses[4]):
                        lista_frecuencias[4] = lista_frecuencias[4] + 1
                        delta_tiempo = orden.hora_entrega_ultimo_item - orden.hora_inicio
                        self.tiempo[4] = self.tiempo[4] + delta_tiempo.seconds / 60 + delta_tiempo.days * 1440

                    elif (orden.hora_inicio.month == lista_meses[5]):
                        lista_frecuencias[5] = lista_frecuencias[5] + 1
                        delta_tiempo = orden.hora_entrega_ultimo_item - orden.hora_inicio
                        self.tiempo[5] = self.tiempo[5] + delta_tiempo.seconds / 60 + delta_tiempo.days * 1440
                        # Se obtiene el promedio de tiempo por mes, tiempo acumulado/cantidad de ocurrencias => tiempo[i]/lista_frecuencias[i]
        no_ventas = False
        for freq in lista_frecuencias:
            if (freq != 0):
                no_ventas = 1
                break

        if (not no_ventas):
            print('No hay ventas')

        for i in range(0, 5):
            try:
                self.tiempo[i] = self.tiempo[i] / lista_frecuencias[i]
            except ZeroDivisionError:
                self.tiempo[i] = 0

        iter_mes = 0
        for m in lista_meses:
            if (m == 1):
                self.mes[iter_mes] = 'Enero \n' + str(years[iter_mes])
            elif (m == 2):
                self.mes[iter_mes] = 'Febrero \n' + str(years[iter_mes])
            elif (m == 3):
                self.mes[iter_mes] = 'Marzo \n' + str(years[iter_mes])
            elif (m == 4):
                self.mes[iter_mes] = 'Abril \n' + str(years[iter_mes])
            elif (m == 5):
                self.mes[iter_mes] = 'Mayo \n' + str(years[iter_mes])
            elif (m == 6):
                self.mes[iter_mes] = 'Junio \n' + str(years[iter_mes])
            elif (m == 7):
                self.mes[iter_mes] = 'Julio \n' + str(years[iter_mes])
            elif (m == 8):
                self.mes[iter_mes] = 'Agosto \n' + str(years[iter_mes])
            elif (m == 9):
                self.mes[iter_mes] = 'Septiembre \n' + str(years[iter_mes])
            elif (m == 10):
                self.mes[iter_mes] = 'Octubre \n' + str(years[iter_mes])
            elif (m == 11):
                self.mes[iter_mes] = 'Noviembre \n' + str(years[iter_mes])
            elif (m == 12):
                self.mes[iter_mes] = 'Diciembre \n' + str(years[iter_mes])
            iter_mes = iter_mes + 1
            if (iter_mes > 5):
                break

        # separacion entre barras
        pos_y = np.arange(len(self.mes))

        # Matpltlib
        plt.bar(pos_y, self.tiempo, align='center', color='red')
        plt.xticks(pos_y, self.mes)
        plt.ylabel('Tiempo promedio (Minutos)')
        plt.xlabel('Mes')


        _mes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
               'Noviembre', 'Diciembre']

        plt.title('Tiempo promedio de atención de pedidos \nen el semestre: ' + _mes[lista_meses[5]-1] + ' del ' + str(years[5]-1) + ' a ' + _mes[lista_meses[0]-1] + ' del ' + str(years[0]-1))
        plt.tight_layout()
        plt.show()


class ReporteIngresosDiariosSemanal(ReportesInterface):
    def __init__(self, fecha_reporte):
        self.dias = ('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado')
        self.ingresos = 7 * [0]
        self.fecha = fecha_reporte
        print(self.fecha)

    def mostrar_reporte(self):
        # obtencion de datos
        fecha_seleccionada = self.fecha
        dia_semana = fecha_seleccionada.isoweekday()
        inicio_semana_dia = fecha_seleccionada.day - dia_semana
        mes = fecha_seleccionada.month
        year = fecha_seleccionada.year

        inicio_semana_fecha = self.calcular_fecha_inicio(year, mes, inicio_semana_dia)
        print(inicio_semana_fecha)

        final_semana_dia = fecha_seleccionada.day + 6 - dia_semana
        final_semana_fecha = self.calcular_fecha_final(year, mes, final_semana_dia)
        print(final_semana_fecha)

        query = ItemFactura.select().join(Factura).join(Orden).where(
            Orden.hora_inicio.between(inicio_semana_fecha, final_semana_fecha))

        dias_intermedios = [inicio_semana_fecha,
                            inicio_semana_fecha + timedelta(days=1), inicio_semana_fecha + timedelta(days=2),
                            inicio_semana_fecha + timedelta(days=3), inicio_semana_fecha + timedelta(days=4),
                            inicio_semana_fecha + timedelta(days=5), final_semana_fecha]

        for orden in query:
            fecha_orden = orden.factura.orden.hora_inicio
            for dia in range(0, 7):
                if (fecha_orden.day == dias_intermedios[dia].day):
                    self.ingresos[dia] = self.ingresos[dia] + orden.precio * orden.cantidad

 
        # separacion entre barras
        pos_y = np.arange(len(self.dias))

        # Matpltlib
        plt.bar(pos_y, self.ingresos, align='center', color='red')
        plt.xticks(pos_y, self.dias)
        plt.ylabel('Ingresos (Pesos)')
        plt.xlabel('Dia de la semana')
        
        _inicio = str(inicio_semana_fecha.day) + '-' + str(inicio_semana_fecha.month) + '-' + str(inicio_semana_fecha.year)
        _fin = str(final_semana_fecha.day) + '-' + str(final_semana_fecha.month) + '-' + str(final_semana_fecha.year)
        plt.title('Ingresos en la semana: ' + str(_inicio) + ' a ' + str(_fin))
        plt.tight_layout()
        plt.show()

    def calcular_fecha_inicio(self, year, mes, dia):
        if (dia <= 0):
            if (mes == 1 or mes == 2 or mes == 4 or mes == 6 or mes == 8 or mes == 9 or mes == 11):
                dia = 31 + dia
            elif (mes == 5 or mes == 7 or mes == 10 or mes == 12):
                dias = 30 + dia
            elif (mes == 3):
                dias = 28 + dia
            if (mes == 1):
                mes = 12
                year = year - 1
            else:
                mes = mes - 1
        return datetime(year, mes, dia)

    def calcular_fecha_final(self, year, mes, dia):
        if (mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12):
            if (dia > 31):
                dia = dia - 31
                mes = 1
                year = year + 1
                print("xD")
        elif (mes == 4 or mes == 6 or mes == 9 or mes == 11):
            if (dia > 30):
                dia = dia - 30
                mes = mes + 1
        elif (mes == 2):
            if (dia > 28):
                dia = dia - 28
                mes = mes + 1
        return datetime(year, mes, dia, hour=23, minute=59, second=59)


class ReporteMeserosDelMes(ReportesInterface):
    def __init__(self):
        self.mesero = []
        self.mes = ['Enero:', 'Febrero:', 'Marzo:', 'Abril:', 'Mayo:', 'Junio:', 'Julio:', 'Agosto:', 'Septiembre:', 'Octubre:',
                    'Noviembre:', 'Diciembre:']
        self.fechas_meses = [
            {'MesI': datetime(2017, 1, 1), 'MesF': datetime(2017, 1, 31)},
            {'MesI': datetime(2017, 2, 1), 'MesF': datetime(2017, 2, 28)},
            {'MesI': datetime(2017, 3, 1), 'MesF': datetime(2017, 3, 31)},
            {'MesI': datetime(2017, 4, 1), 'MesF': datetime(2017, 4, 30)},
            {'MesI': datetime(2017, 5, 1), 'MesF': datetime(2017, 5, 31)},
            {'MesI': datetime(2017, 6, 1), 'MesF': datetime(2017, 6, 30)},
            {'MesI': datetime(2017, 7, 1), 'MesF': datetime(2017, 7, 31)},
            {'MesI': datetime(2017, 8, 1), 'MesF': datetime(2017, 8, 31)},
            {'MesI': datetime(2017, 8, 1), 'MesF': datetime(2017, 9, 30)},
            {'MesI': datetime(2017, 10, 1), 'MesF': datetime(2017, 10, 31)},
            {'MesI': datetime(2017, 11, 1), 'MesF': datetime(2017, 11, 30)},
            {'MesI': datetime(2017, 12, 1), 'MesF': datetime(2017, 12, 31)}, ]

    def mostrar_reporte(self):
        # obtencion de datos
        i = 0
        for month in self.fechas_meses:

            empleados = []
            query_empleados = Orden.select(Empleado.nombres, peewee.fn.count(Orden.id)).join(Empleado).where(
                Orden.hora_inicio.between(month['MesI'], month['MesF'])).group_by(Empleado.nombres)
            for orden in query_empleados:
                empleados.append(orden.atentido_por.nombres)

            subtotal_empleado = len(empleados) * [0]
            query_ventas = ItemFactura.select().join(Factura).join(Orden).join(Empleado).where(
                Orden.hora_inicio.between(month['MesI'], month['MesF']))
            for venta in query_ventas:
                nombre_empleado_venta = venta.factura.orden.atentido_por.nombres
                for pos in range(0, len(empleados)):
                    if (nombre_empleado_venta == empleados[pos]):
                        subtotal_empleado[pos] = subtotal_empleado[pos] + venta.cantidad * venta.precio
            posMax = -1
            valorMax = -1
            for pos in range(0, len(subtotal_empleado)):
                if (subtotal_empleado[pos] > valorMax):
                    posMax = pos
                    valorMax = subtotal_empleado[pos]
            if (posMax != -1):
                self.mesero.append(empleados[posMax])
            else:
                self.mesero.append('No se ha elegido el mesero')
        print(self.mesero)
        reporte = Generador()
        reporte.generar_reporte(datos=self.mesero, labels=self.mes, mes=None, tipo=1)
        # separacion entre barras


class ReporteIngresosPorDiaMensual(ReportesInterface):
    def __init__(self, fecha_reporte):
        self.dias = []
        self.ingresos = None
        self.fecha = fecha_reporte

    def mostrar_reporte(self):
        # obtencion de datos
        fecha_seleccionada = self.fecha
        mes = fecha_seleccionada.month
        dia = fecha_seleccionada.day;
        if (mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12):
            dia = 31
        elif (mes == 4 or mes == 6 or mes == 9 or mes == 11):
            dia = 30
        elif (mes == 2):
            dia = 28

        print(dia)
        inicio_mes = datetime(fecha_seleccionada.year, mes, 1)
        fin_mes = datetime(fecha_seleccionada.year, mes, dia)

        for i in range(1, dia + 1):
            self.dias.append(str(i))
        self.ingresos = dia * [0]

        query = ItemFactura.select().join(Factura).join(Orden).where(Orden.hora_inicio.between(inicio_mes, fin_mes))
        for orden in query:
            fecha_orden = orden.factura.orden.hora_inicio
            for d in range(0, dia):
                if (fecha_orden.day - 1 == d):
                    self.ingresos[d] = self.ingresos[d] + orden.precio * orden.cantidad

        mes = ['Enero:', 'Febrero:', 'Marzo:', 'Abril:', 'Mayo:', 'Junio:', 'Julio:', 'Agosto:', 'Septiembre:', 'Octubre:',
               'Noviembre:', 'Diciembre:']
        fecha_reporte = mes[fecha_seleccionada.month - 1] + ' ' + str(fecha_seleccionada.year)
        reporte = Generador()
        
        for i in range(len(self.ingresos)):
            self.ingresos[i] = '$' + str(self.ingresos[i])
        reporte.generar_reporte(datos=self.ingresos, labels=self.dias, mes=fecha_reporte, tipo=2)
        for fila in range(0, dia):
            print(self.dias[fila], self.ingresos[fila])
            # separacion entre barras


class ReportesFactory:
    class __ReportesFactory:
        def __init__(self, reporte_tipo, fecha_reporte=datetime.now()):
            self.reporte = None
            self.tipo = reporte_tipo
            self.fecha = fecha_reporte
            print('Fecha factory: ', self.fecha)

        def mostrar_reporte(self):
            if self.tipo == 1:
                self.reporte = ReportePlatosMasVendidos()
                self.reporte.mostrar_reporte()
            elif self.tipo == 2:
                # reporte platos menos vendidos
                self.reporte = ReportePlatosMenosVendidos()
                self.reporte.mostrar_reporte()
            elif self.tipo == 3:
                # rtiempo promedio
                self.reporte = ReporteTiempoPromedio()
                self.reporte.mostrar_reporte()
            elif self.tipo == 4:
                # ingresos diarios
                self.reporte = ReporteIngresosDiariosSemanal(self.fecha)
                self.reporte.mostrar_reporte()
            elif self.tipo == 5:
                # mesero del mes
                self.reporte = ReporteMeserosDelMes()
                self.reporte.mostrar_reporte()
            elif self.tipo == 6:
                # ingresos/ dia
                self.reporte = ReporteIngresosPorDiaMensual(self.fecha)
                self.reporte.mostrar_reporte()

    Instance = None

    def __init__(self):
        self.Instance = None

    def get_instance(self, reporte_tipo, _fecha=None):
        if not ReportesFactory.Instance:
            print('Fecha ge intance: ', _fecha)
            ReportesFactory.Instance = ReportesFactory.__ReportesFactory(reporte_tipo, fecha_reporte=_fecha)
        else:
            ReportesFactory.Instance.fecha = _fecha
            ReportesFactory.Instance.tipo = reporte_tipo

        return ReportesFactory.Instance
