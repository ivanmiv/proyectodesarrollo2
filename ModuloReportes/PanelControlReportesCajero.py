from PyQt5 import uic

from ModuloReportes.CalendarioReporte import UiCalendarioReporte
from ModuloReportes.PanelControlModuloReportes import UiPanelControlReportes
from ModuloReportes.ReportesMain import ReportesFactory

class PanelControlReportesCajero(UiPanelControlReportes):
    def __init__(self,ui_padre):
        super(UiPanelControlReportes, self).__init__()
        uic.loadUi('ModuloReportes/PanelControlReportes.ui', self)

        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.b_ingresos_cada_dia_semana_cajero.clicked.connect(self.reportarIngresosDiaSemana)
        self.b_ingresos_caja_diarios_en_mes_cajero.clicked.connect(self.tablaIngresoCaja)
        self.b_items_mas_vendidos_mes_cajero.clicked.connect(self.reportarMasVendidos)
        self.b_items_menos_vendidos_semestre_cajero.clicked.connect(self.reportarMenosVendidos)
        self.wGerente.hide()
        self.wCajero.show()
        self.wMesero.hide()
        self.show()
        self.singleton = None

        ########################################################################


        ########################################################################
        # consultar reportes y graficos en la tabla:



        # cnsultar items mas vendidos del mes

    def reportarMasVendidos(self):
        print("Reportando mas vendidos del mes")

        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(1)
        self.singleton.mostrar_reporte()
        # consultar items menos vendidos del semestre

    def reportarMenosVendidos(self):
        print("Reportando menos vendidos del Semestre")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(2)
        self.singleton.mostrar_reporte()
        # ingress del dia de la semana

    def reportarIngresosDiaSemana(self):
        print("Reportando ingresos por cada dia de la semana")
        self.ventana_calendario = UiCalendarioReporte(4)

        # ingress en la caja cada dia del mes (tabla 2)

    def tablaIngresoCaja(self):
        print("Reportando los ingressos de la caja cada dia del mes")
        self.ventana_calendario = UiCalendarioReporte(6)


    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()