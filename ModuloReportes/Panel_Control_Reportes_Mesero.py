from PyQt5 import uic

from  ModuloReportes.PanelControlModuloReportes import UiPanelControlReportes
from ModuloReportes.ReportesMain import ReportesFactory


class Panel_Control_Reportes_Mesero(UiPanelControlReportes):
    def __init__(self,ui_padre):
        super(UiPanelControlReportes, self).__init__()
        uic.loadUi('ModuloReportes/PanelControlReportes.ui', self)

        self.ui_padre = ui_padre
        self.ui_padre.hide()

        self.b_items_mas_vendidos_mes_mesero.clicked.connect(self.reportarMasVendidos)
        self.b_items_menos_vendidos_semestre_mesero.clicked.connect(self.reportarMenosVendidos)
        self.b_meseros_del_mes_mesero.clicked.connect(self.tablaMeseroDelMes)
        self.b_tiempo_atencion_pedidos_mesero.clicked.connect(self.reportarPromedioTiempoAtencion)
        self.wGerente.hide()
        self.wCajero.hide()
        self.wMesero.show()
        self.show()

        ########################################################################


        ########################################################################
        # consultar reportes y graficos en la tabla:



        # cnsultar items mas vendidos del mes

    def reportarMasVendidos(self):
        print("Reportando mas vendidos del mes")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(1)
        self.singleton.mostrar_reporte()
    

        # consultar items menos vendidos del semestre

    def reportarMenosVendidos(self):
        print("Reportando menos vendidos del Semestre")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(2)
        self.singleton.mostrar_reporte()
        

        # tiempo promedio de atencion de pedidos

    def reportarPromedioTiempoAtencion(self):
        print("Reportando Promedio de tiempo de atencion de los pedidos")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(3)
        self.singleton.mostrar_reporte()
        # consultar meseros del mes de cada año (tabla 1)

    def tablaMeseroDelMes(self):
        print("Reportando tabla del mesero del mes")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(5)
        self.singleton.mostrar_reporte()

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()