from datetime import datetime

from PyQt5 import uic, QtWidgets

from ModuloReportes.ReportesMain import ReportesFactory

class UiCalendarioReporte(QtWidgets.QMainWindow):
    def __init__(self, _tipo):
        super(UiCalendarioReporte, self).__init__()
        uic.loadUi('ModuloReportes/calendarioreportesemana.ui', self)
        self.b_obtener_reporte.clicked.connect(self.hacerReporte)
        self.tipo = _tipo
        self.singleton = None
        self.show()

    def hacerReporte(self):
        reporte = ReportesFactory()
        fecha = self.c_semana.selectedDate()
        print(self.c_semana.selectedDate())
        fecha_selecccionada = datetime(fecha.year(), fecha.month(), fecha.day())
        print('Fecha seleccionada:', fecha_selecccionada)
        if (self.tipo == 4):
            print('4')
            self.singleton = reporte.get_instance(4, fecha_selecccionada)
            self.singleton.mostrar_reporte()
        
        elif (self.tipo == 6):
            print('6')
            self.singleton = reporte.get_instance(6, fecha_selecccionada)
            self.singleton.mostrar_reporte()
        self.hide()