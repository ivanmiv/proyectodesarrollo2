from PyQt5 import uic

from ModuloReportes.CalendarioReporte import UiCalendarioReporte
from ModuloReportes.PanelControlModuloReportes import UiPanelControlReportes
from ModuloReportes.ReportesMain import ReportesFactory

class PanelControlReportesGerente(UiPanelControlReportes):
    def __init__(self,ui_padre):
        super(UiPanelControlReportes, self).__init__()
        uic.loadUi('ModuloReportes/PanelControlReportes.ui', self)


        self.ui_padre = ui_padre
        self.ui_padre.hide()
        try:
            self.b_ingresos_cada_dia_semana_gerente.clicked.connect(self.reportarIngresosDiaSemana)
            self.b_ingresos_caja_diarios_en_mes_gerente.clicked.connect(self.tablaIngresoCaja)
            self.b_items_mas_vendidos_mes_gerente.clicked.connect(self.reportarMasVendidos)
            self.b_items_menos_vendidos_semestre_gerente.clicked.connect(self.reportarMenosVendidos)
            self.b_meseros_del_mes_gerente.clicked.connect(self.tablaMeseroDelMes)
            self.b_tiempo_atencion_pedidos_gerente.clicked.connect(self.reportarPromedioTiempoAtencion)
            self.wGerente.show()
            self.wCajero.hide()
            self.wMesero.hide()
            self.show()
        except Exception as e:
            print(e)
        ########################################################################


        ########################################################################
        # consultar reportes y graficos en la tabla:



        # cnsultar items mas vendidos del mes

    def reportarMasVendidos(self):
        print("Reportando mas vendidos del mes")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(1)
        self.singleton.mostrar_reporte()

    # consultar items menos vendidos del semestre
    def reportarMenosVendidos(self):
        print("Reportando menos vendidos del Semestre")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(2)
        self.singleton.mostrar_reporte()

    # tiempo promedio de atencion de pedidos
    def reportarPromedioTiempoAtencion(self):
        print("Reportando Promedio de tiempo de atencion de los pedidos")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(3)
        self.singleton.mostrar_reporte()

    # ingresos del dia de la semana
    def reportarIngresosDiaSemana(self):
        print("Reportando ingresos por cada dia de la semana")
        self.ventana_calendario = UiCalendarioReporte(4)
        # print(ventana_calendario)

    # consultar meseros del mes de cada año (tabla 1)
    def tablaMeseroDelMes(self):
        print("Reportando tabla del mesero del mes")
        reporte = ReportesFactory()
        self.singleton = reporte.get_instance(5)
        self.singleton.mostrar_reporte()

    # ingresos en la caja cada dia del mes (tabla 2)
    def tablaIngresoCaja(self):
        print("Reportando los ingresos de la caja cada dia del mes")
        self.ventana_calendario = UiCalendarioReporte(6)

    def closeEvent(self, e):
        self.ui_padre.show()
        e.accept()