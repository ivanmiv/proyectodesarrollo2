from abc import ABCMeta, abstractmethod

from ModuloReportes.PanelControlReportesCajero import PanelControlReportesCajero
from ModuloReportes.PanelControlReportesGerente import PanelControlReportesGerente
from ModuloReportes.Panel_Control_Reportes_Mesero import Panel_Control_Reportes_Mesero


# --------------------Adapter--------------------------

# Interfaz a implementar
class FabricaPanelEmpleadosReportesIF:
    __metaclass__ = ABCMeta

    @abstractmethod
    def mostrar_panel_empleado(self,ui_padre, tipo):
        raise Exception("Metodo no implementado")


class FabricaPanelEmpleadosReportes(FabricaPanelEmpleadosReportesIF):
    # codigo para la fabrica
    tiposDeEmpleados = {"mesero": Panel_Control_Reportes_Mesero,
                        "cajero": PanelControlReportesCajero,
                        "gerente": PanelControlReportesGerente}
    panel_empleado = None

    def mostrar_panel_empleado(self,ui_padre, tipo):
        self.panel_empleado = self.tiposDeEmpleados[tipo](ui_padre)
