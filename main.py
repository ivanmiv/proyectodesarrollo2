import sys
from PyQt5 import QtWidgets

from PyQt5.QtCore import QLibraryInfo
from PyQt5.QtCore import QTranslator

from ConfiguracionORM.FachadaBD import FachadaBD

from ModuloCategoria.ModuloCategoria import *
from ModuloEmpleado.modelo import Empleado
from Principal.login import UiLoginSingleton

def main():
    app = QtWidgets.QApplication(sys.argv)

    fachada_bd = FachadaBD()
    fachada_bd.crear_tablas_bd()


    #
    #    orden,creado  = Orden.get_or_create(tipo="MESA",mesa=Mesa.get(Mesa.id==i),atentido_por=Empleado.get(Empleado.numero_documento_identificacion=="123"+str(i)))
    #    itemorden1,creado  = ItemOrden.get_or_create(orden=orden,plato=item1,cantidad_plato=1+i,)
    #    itemorden2,creado  = ItemOrden.get_or_create(orden=orden, plato=item2, cantidad_plato=2*i, )
    #    itemorden3,creado  = ItemOrden.get_or_create(orden=orden, plato=item3, cantidad_plato=3+2*i, )
    #   itemorden4,creado  = ItemOrden.get_or_create(orden=orden, plato=item4, cantidad_plato=4*(i**2), )


    qtTranslator = QTranslator()
    if qtTranslator.load("qt_es", QLibraryInfo.location(QLibraryInfo.TranslationsPath)):
        app.installTranslator(qtTranslator)

    # login = UiLoginSingleton()
    # reporte = ReportesFactory()
    # singleton = reporte.get_instance(2)
    # singleton.mostrar_reporte()
    login = UiLoginSingleton()
    # item = ModuloItemMenu()
    # empleado = ModuloEmpleados()
    # factura = ModuloFacturacion()
    #pedido = ModuloPedidos()
    # sucursal = ModuloSucursales()

    sys.exit(app.exec_())

main()