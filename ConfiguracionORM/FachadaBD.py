# --------------------Facade--------------------------



class FachadaBD:
    def __init__(self):
        from ConfiguracionORM.Configuracion_BD import ConfiguracionBaseDatos
        from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter

        self.configuracion_base_datos = ConfiguracionBaseDatos()
        self.base_datos = BaseDatosAdapter(self.configuracion_base_datos)
        self.base_datos.iniciar_base_datos()

    def get_base_datos(self):
        return self.base_datos

    def crear_tablas_bd(self):
        from ModuloItemMenu.modelo import ItemMenu
        from ModuloCategoria.modelo import Categoria
        from ModuloEmpleado.modelo import Empleado, Turno
        from ModuloMesa.modelo import Mesa
        from ModuloOrden.modelo import Orden, ItemOrden
        from ModuloSucursal.modelo import Sucursal
        from ModuloFactura.modelo import Factura, Pago
        from ModuloFactura.modelo import ItemFactura
        self.base_datos.crear_tablas(
            [Categoria, Empleado, Turno, Mesa, Orden, ItemOrden, Sucursal, Factura, Pago, ItemMenu, ItemFactura
             ])

        # -----------------------------------------------------
