from peewee import Model, CharField, Check

from ConfiguracionORM.BaseDatosAdapter import BaseDatosAdapter


# Modelo base para las clases
class BaseModel(Model):
    # estado que solo acepta ACTIVO o INACTIVO como valor
    estado = CharField(max_length=20, default='ACTIVO',
                       constraints=[Check('estado = \'ACTIVO\' or estado = \'INACTIVO\''), ])

    class Meta:
        database = BaseDatosAdapter.base_datos
