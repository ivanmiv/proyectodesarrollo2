from PyQt5.QtWidgets import QMessageBox


class ConfiguracionBaseDatos:
    def __init__(self):

        self.ventana_confirmacion = QMessageBox()
        self.ventana_confirmacion.setWindowTitle("Mensaje")
        self.ventana_confirmacion.setStandardButtons(QMessageBox.Ok)
        self.ventana_confirmacion.setDefaultButton(QMessageBox.Ok)
        self.ventana_confirmacion.button(QMessageBox.Ok).setText("Aceptar")

        self.archivo = None
        try:
            self.archivo = open("conf.bd", "r")
        except Exception as e:

            self.nombre_base_datos = "desarrollo2"
            self.usuario = "postgres"
            self.password = "postgres"
            self.host = "127.0.0.1"
            self.ventana_confirmacion.setText("Problema cargando configuración de base de datos error : %s. \n"
                                              "Cargada configuración por defecto" % e)
            self.ventana_confirmacion.exec()
        else:
            try:

                self.nombre_base_datos = self.archivo.readline().split("=")[1].split("\n")[0]
                self.usuario = self.archivo.readline().split("=")[1].split("\n")[0]
                self.password = self.archivo.readline().split("=")[1].split("\n")[0]
                self.host = self.archivo.readline().split("=")[1].split("\n")[0]


            except Exception as e:

                self.nombre_base_datos = "desarrollo2"
                self.usuario = "postgres"
                self.password = "postgres"
                self.host = "127.0.0.1"
                self.ventana_confirmacion.setText("Problema leyendo configuración de base de datos error : %s. \n"
                                                  "Cargada configuración por defecto" % e)
                self.ventana_confirmacion.exec()
            else:
                self.archivo.close()

                self.ventana_confirmacion.setText("Configuración de la base de datos cargada correctamente")
                self.ventana_confirmacion.exec()
