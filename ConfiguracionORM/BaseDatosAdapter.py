from abc import ABCMeta, abstractmethod

from peewee import PostgresqlDatabase


class BaseDatos:
    __metaclass__ = ABCMeta

    @abstractmethod
    def crear_tablas(self, lista_clases):
        raise Exception("Metodo no implementado")

    @abstractmethod
    def conectar_base_datos(self):
        raise Exception("Metodo no implementado")

    @abstractmethod
    def desconectar_base_datos(self):
        raise Exception("Metodo no implementado")

    @abstractmethod
    def iniciar_base_datos(self):
        raise Exception("Metodo no implementado")


# Adaptador


class BaseDatosAdapter(BaseDatos):
    # PostrgresqlDatabase existente que queremos adaptar
    base_datos = PostgresqlDatabase(None)

    def __init__(self, configuracion_base_datos):
        self.configuracion_base_datos = configuracion_base_datos

    def get_base_datos(self):
        return self.base_datos

    def iniciar_base_datos(self):
        self.base_datos.init(self.configuracion_base_datos.nombre_base_datos,
                             user=self.configuracion_base_datos.usuario,
                             password=self.configuracion_base_datos.password,
                             host=self.configuracion_base_datos.host)

    def crear_tablas(self, lista_clases):
        self.base_datos.connect()
        self.base_datos.create_tables(lista_clases, safe=True)
        self.base_datos.close()

    def conectar_base_datos(self):
        self.base_datos.connect()

    def desconectar_base_datos(self):
        self.base_datos.close()

        # -----------------------------------------------------
